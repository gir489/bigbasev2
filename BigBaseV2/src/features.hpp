#pragma once
#include "common.hpp"
#include "gta\natives.hpp"

namespace big::features
{
	void run_tick();
	void resolve_weapon_hotkey(Hash weapon);
	void script_func();
}
