#pragma once
#include <cstdint>

#pragma pack(push, 1)
class RockstarIdentifier
{
public:
	uint64_t m_rockstar_id; //0x0000
	uint32_t m_type; //0x0008
	uint32_t m_sub_type; //0x000C
}; //Size: 0x0010

class QueryGamerResponse
{
public:
	char pad_0000[104]; //0x0000
	uint64_t m_identifier_count; //0x0068
	RockstarIdentifier m_rockstar_identifiers[32]; //0x0070
	char pad_0270[8272]; //0x0270
	char* m_response_data[32]; //0x22C0
	char pad_23C0[4]; //0x23C0
	uint32_t m_response_size; //0x23C4
}; //Size: 0x23C8

class GsSession
{
public:
	char pad_0000[8]; //0x0000
	uint64_t m_peer_token; //0x0008
	char pad_0010[56]; //0x0010
	uint64_t m_rockstar_id; //0x0048
}; //Size: 0x0050

class NetMsgIdentifier
{
public:
	uint64_t m_rockstar_id; //0x0000
	uint8_t m_type; //0x0008
	char pad_0009[8]; //0x0009
}; //Size: 0x0011

class JoinSessionRequest
{
public:
	char pad_0000[8]; //0x0000
	GsSession m_session_info; //0x0008
	NetMsgIdentifier m_identifier; //0x0058
}; //Size: 0x0069

#pragma pack(pop)
