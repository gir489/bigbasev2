#pragma once
#include <cstdint>
#include "fwddec.hpp"
#include "extensible.hpp"
#include "vector.hpp"
#include "gta\natives.hpp"

#pragma pack(push, 1)
namespace rage
{
#	pragma warning(push)
#	pragma warning(disable : 4201) // nonstandard extension used: nameless struct/union
	union netAddress
	{
		std::uint32_t m_raw;
		struct
		{
			std::uint8_t m_field4;
			std::uint8_t m_field3;
			std::uint8_t m_field2;
			std::uint8_t m_field1;
		};
	};
#	pragma warning(pop)

	class netPlayerData
	{
	public:
		char pad_0000[8]; //0x0000
		uint64_t m_rockstar_id_2; //0x0008
		char pad_0010[32]; //0x0010
		uint32_t m_sec_key_time; //0x0030
		netAddress m_relay_ip; //0x0034
		uint16_t m_relay_port; //0x0038
		char pad_003A[18]; //0x003A
		netAddress m_online_ip; //0x004C
		uint16_t m_online_port; //0x0050
		char pad_0052[2]; //0x0052
		netAddress m_local_ip; //0x0054
		uint16_t m_local_port; //0x0058
		char pad_005A[6]; //0x005A
		int32_t m_host_token; //0x0060
		char pad_0064[12]; //0x0064
		uint64_t m_rockstar_id; //0x0070
		char pad_0078[12]; //0x0078
		char m_name[20]; //0x0084
	}; //Size: 0x0098
	static_assert(sizeof(netPlayerData) == 0x98);

	class nonPhysicalPlayerDataBase
	{
	public:
		virtual ~nonPhysicalPlayerDataBase() = default;    // 0 (0x00)
		virtual void unk_0x08() = 0;                       // 1 (0x08)
		virtual void unk_0x10() = 0;                       // 2 (0x10)
		virtual void unk_0x18() = 0;                       // 3 (0x18)
		virtual void log(netLoggingInterface* logger) = 0; // 4 (0x20)
	};

	class netPlayer
	{
	public:
		virtual ~netPlayer() = default;            // 0 (0x00)
		virtual void reset() = 0;                  // 1 (0x08)
		virtual bool is_valid() const = 0;         // 2 (0x10)
		virtual const char *get_name() const = 0;  // 3 (0x18)
		virtual void _0x20() = 0;                  // 4 (0x20)
		virtual bool is_host() = 0;                // 5 (0x28)
		virtual netPlayerData *get_net_data() = 0; // 6 (0x30)
		virtual void _0x38() = 0;                  // 7 (0x38)
	};

	class netPlayerMgrBase
	{
	public:
		virtual ~netPlayerMgrBase() = default; // 0 (0x00)
	};
}

namespace gta
{
	inline constexpr auto num_players = 32;
}

class CNonPhysicalPlayerData : public rage::nonPhysicalPlayerDataBase
{
public:
	std::int32_t  m_bubble_id; // 0x08
	std::int32_t  m_player_id; // 0x0C
	rage::vector3 m_position;  // 0x10
};

class CNetGamePlayer : public rage::netPlayer
{
public:
	char pad_0008[152]; //0x0008
	rage::CPlayerInfo* player_info; //0x00A0
	char pad_00A8[20]; //0x00A8
	int32_t bubble_id; //0x00BC
	Player player_id; //0x00C0
};

static_assert(sizeof(CNonPhysicalPlayerData) == 0x1C);
#pragma pack(pop)
