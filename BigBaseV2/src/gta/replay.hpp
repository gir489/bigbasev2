#pragma once
#pragma pack(push, 4)

#include "common.hpp"
#include "fwddec.hpp"
#include "array.hpp"
#include "gta\netObject.h"
#include "extensible.hpp"

namespace rage
{
	class fwEntity
	{
	public:
		char pad_0000[32]; //0x0000
		class CBaseModelInfo* m_model_info; //0x0020
		int8_t m_invisible; //0x0028
		char pad_0029[7]; //0x0029
		class CNavigation* m_navigation; //0x0030
		char pad_0038[1]; //0x0038
		int8_t m_entity_type; //0x0039
		char pad_003A[150]; //0x003A
		rage::netObject* m_net_object; //0x00D0
		char pad_00D8[176]; //0x00D8
		uint32_t m_damage_bits; //0x0188

		bool is_invincible() { return(m_damage_bits & (1 << 8)); }
		void enable_invincible() { m_damage_bits |= (1 << 8); }
		void disable_invincible() { m_damage_bits &= ~(1 << 8); }
	}; //Size: 0x018C
	static_assert(sizeof(fwEntity) == 0x18C, "fwEntity is not properly sized");

	class CVehicle : public fwEntity
	{
	public:
		char pad_018C[396]; //0x018C
		uint32_t m_boost_bits; //0x0318
		char pad_031C[4]; //0x031C
		float m_rocket_boost; //0x0320
		char pad_0324[1536]; //0x0324
		float m_boost_max; //0x0924
		float m_boost; //0x0928
	}; //Size: 0x092C
	static_assert(sizeof(CVehicle) == 0x92C);

	class CNavigation
	{
	public:
		char pad_0000[32]; //0x0000
		float m_heading; //0x0020
		float m_heading2; //0x0024
		char pad_0028[8]; //0x0028
		vector3 m_rotation; //0x0030
		char pad_003C[20]; //0x003C
		vector3 m_position; //0x0050
	}; //Size: 0x005C

	class CPed : public fwEntity
	{
	public:
		char pad_018C[2980]; //0x018C
		class CVehicle* m_last_vehicle; //0x0D30
		char pad_0D38[900]; //0x0D38
		int8_t m_decal_index; //0x10BC
		int8_t m_decal_info; //0x10BD
		char pad_10BE[10]; //0x10BE
		class CPlayerInfo* m_player_info; //0x10C8
		class CPedInventory* m_ped_inventory; //0x10D0
		class CPedWeaponManager* m_ped_weapon_manager; //0x10D8
		char pad_10E0[824]; //0x10E0
		uint64_t m_ped_state; //0x1418
		char pad_1420[256]; //0x1420
		class CVehicle* m_last_vehicle2; //0x1520
	}; //Size: 0x1528
	static_assert(sizeof(CPed) == 0x1528);

	// Created with ReClass.NET 1.2 by KN4CK3R

	class CPlayerInfo
	{
	public:
		char pad_0000[32]; //0x0000
		uint32_t m_internal_ip; //0x0020
		uint16_t m_internal_port; //0x0024
		char pad_0026[2]; //0x0026
		uint32_t m_relay_ip; //0x0028
		uint16_t m_relay_port; //0x002C
		char pad_002E[70]; //0x002E
		uint32_t m_external_ip; //0x0074
		uint16_t m_external_port; //0x0078
		char pad_007A[20]; //0x007A
		int8_t N00001AC7; //0x008E
		char pad_008F[1]; //0x008F
		uint64_t m_rockstar_id; //0x0090
		char pad_0098[12]; //0x0098
		char m_name[20]; //0x00A4
		char pad_00B8[184]; //0x00B8
		float m_swim_speed; //0x0170
		char pad_0174[77]; //0x0174
		bool m_is_rockstar_dev; //0x01C1
		char pad_01C2[1]; //0x01C2
		bool m_is_cheater; //0x01C3
		char pad_01C4[11]; //0x01C4
		bool m_is_online; //0x01CF
		char pad_01D0[24]; //0x01D0
		class CPed* m_ped; //0x01E8
		char pad_01F0[40]; //0x01F0
		uint32_t m_frame_flags; //0x0218
		char pad_021C[28]; //0x021C
		uint32_t m_player_controls; //0x0238
		char pad_023C[1604]; //0x023C
		bool m_is_wanted; //0x0880
		char pad_0881[3]; //0x0881
		int32_t m_wanted_level_display; //0x0884
		int32_t m_wanted_level; //0x0888
		char pad_088C[1124]; //0x088C
		float m_run_speed; //0x0CF0
		float m_stamina; //0x0CF4
		float m_stamina_regen; //0x0CF8
	}; //Size: 0x0CFC
	static_assert(sizeof(CPlayerInfo) == 0xCFC);

	class CObject : public fwEntity
	{
	public:
	}; //Size: 0x018C

	class CBaseModelInfo
	{
	private:
		char pad_0000[24]; //0x0000
	public:
		Hash m_model; //0x0018
	private:
		char pad_001C[148]; //0x001C
	public:
		class CVehicleModelInfoLayout* m_vehicle_layout; //0x00B0
	}; //Size: 0x00B8

	class CPedFactory
	{
	public:
		virtual ~CPedFactory() = default;
		class CPed* m_local_ped; //0x0008
	}; //Size: 0x0010

	class CObjectHandle
	{
	public:
		class CObject* m_object; //0x0000
		int32_t m_handle; //0x0008
		char pad_000C[4]; //0x000C
	}; //Size: 0x0010
	static_assert(sizeof(CObjectHandle) == 0x10, "CObjectHandle is not properly sized");

	class CObjectList
	{
	public:
		class CObjectHandle m_objects[2300]; //0x0000
	}; //Size: 0x8FC0

	class CObjectInterface
	{
	public:
		char pad_0000[344]; //0x0000
		class CObjectList* m_object_list; //0x0158
		int32_t m_max_objects; //0x0160
		char pad_0164[4]; //0x0164
		int32_t m_cur_objects; //0x0168

		rage::CObject* get_object(const int& index)
		{
			if (index < m_max_objects)
				return m_object_list->m_objects[index].m_object;
			return nullptr;
		}
	}; //Size: 0x016C

	class CPedHandle
	{
	public:
		class CPed* m_ped; //0x0000
		int32_t m_handle; //0x0008
		char pad_000C[4]; //0x000C
	}; //Size: 0x0010
	static_assert(sizeof(CPedHandle) == 0x10, "CPedHandle is not properly sized");

	class CPedList
	{
	public:
		class CPedHandle m_peds[256]; //0x0000
	}; //Size: 0x1000

	class CPedInterface
	{
	public:
		char pad_0000[256]; //0x0000
		class CPedList* m_ped_list; //0x0100
		int32_t m_max_peds; //0x0108
		char pad_010C[4]; //0x010C
		int32_t m_cur_peds; //0x0110

		CPed* get_ped(const int& index)
		{
			if (index < m_max_peds)
				return m_ped_list->m_peds[index].m_ped;
			return nullptr;
		}
	}; //Size: 0x0114

	class CVehicleHandle
	{
	public:
		class CVehicle* m_vehicle; //0x0000
		int32_t m_handle; //0x0008
		char pad_000C[4]; //0x000C
	}; //Size: 0x0010
	static_assert(sizeof(CVehicleHandle) == 0x10, "CVehicleHandle is not properly sized");

	class CVehicleList
	{
	public:
		class CVehicleHandle m_vehicles[300]; //0x0000
	}; //Size: 0x12C0

	class CVehicleInterface
	{
	public:
		char pad_0000[384]; //0x0000
		class CVehicleList* m_vehicle_list; //0x0180
		int32_t m_max_vehicles; //0x0188
		char pad_018C[4]; //0x018C
		int32_t m_cur_vehicles; //0x0190

		CVehicle* get_vehicle(const int& index)
		{
			if (index < m_max_vehicles)
				return m_vehicle_list->m_vehicles[index].m_vehicle;
			return nullptr;
		}
	}; //Size: 0x0194

	class CReplayInterface
	{
	public:
		char pad_0000[16]; //0x0000
		class CVehicleInterface* m_vehicle_interface; //0x0010
		class CPedInterface* m_ped_interface; //0x0018
		char pad_0020[8]; //0x0020
		class CObjectInterface* m_object_interface; //0x0028
	}; //Size: 0x0030

	class CPedWeaponManager
	{
	public:
		char pad_0000[16]; //0x0000
		class CPed* m_owner; //0x0010
		char pad_0018[8]; //0x0018
		class CWeaponInfo* m_current_weapon_info; //0x0020
		char pad_0028[96]; //0x0028
		class CWeapon* m_weapon; //0x0088
		char pad_0090[308]; //0x0090
		uint32_t N000014E8; //0x01C4
		char pad_01C8[960]; //0x01C8
	}; //Size: 0x0588

	class CPedInventory
	{
	public:
		char pad_0000[16]; //0x0000
		class CPed* m_owner; //0x0010
		char pad_0018[112]; //0x0018
	}; //Size: 0x0088

	class CWeaponInfo
	{
	public:
		char pad_0000[16]; //0x0000
		uint32_t N00000787; //0x0010
		uint32_t N000011D5; //0x0014
		char pad_0018[68]; //0x0018
		uint32_t N000012F6; //0x005C
		char pad_0060[16]; //0x0060
		uint32_t N00000793; //0x0070
		float N000012B9; //0x0074
		float N00000794; //0x0078
		float N000012BC; //0x007C
		float N00000795; //0x0080
		float N000012BF; //0x0084
		float N00000796; //0x0088
		float N000012C2; //0x008C
		float N00000797; //0x0090
		float N000012C5; //0x0094
		float N00000798; //0x0098
		float N000012C8; //0x009C
		float N00000799; //0x00A0
		float N000012CB; //0x00A4
		float N0000079A; //0x00A8
		float N000012CE; //0x00AC
		float N0000079B; //0x00B0
		char pad_00B4[12]; //0x00B4
		float N0000079D; //0x00C0
		float N000012D3; //0x00C4
		float N0000079E; //0x00C8
		float N000012D6; //0x00CC
		float N0000079F; //0x00D0
		float N000012D9; //0x00D4
		float N000007A0; //0x00D8
		float N000012DC; //0x00DC
		char pad_00E0[16]; //0x00E0
		float N000007A3; //0x00F0
		char pad_00F4[4]; //0x00F4
		float N000007A4; //0x00F8
		float N000012E1; //0x00FC
		float N000007A5; //0x0100
		float N000012E4; //0x0104
		float N000007A6; //0x0108
		char pad_010C[4]; //0x010C
		float N000007A7; //0x0110
		float N000012E9; //0x0114
		char pad_0118[8]; //0x0118
		float N000007A9; //0x0120
		float N000012EC; //0x0124
		float N000007AA; //0x0128
		float N000012EF; //0x012C
		char pad_0130[448]; //0x0130
		float CameraFov; //0x02F0
		float N000011CD; //0x02F4
		float N000007E4; //0x02F8
		float FirstPersonScopeFov; //0x02FC
		float FirstPersonScopeAttachmentFov; //0x0300
		char pad_0304[108]; //0x0304
		float N000007F3; //0x0370
		float N000011F0; //0x0374
		float N000007F4; //0x0378
		char pad_037C[20]; //0x037C
		float N000007F7; //0x0390
		float N000011F5; //0x0394
		float N000007F8; //0x0398
		char pad_039C[564]; //0x039C
		float N00000841; //0x05D0
		float N000011FA; //0x05D4
		char pad_05D8[24]; //0x05D8
		char* StatName; //0x05F0
		int32_t KnockdownCount; //0x05F8
		float KillshotImpulseScale; //0x05FC
		uint32_t NmShotTuningSet; //0x0600
		uint32_t AttachBone; //0x0604
		uint32_t N00000848; //0x0608
		uint32_t Component; //0x060C
		uint32_t N00000849; //0x0610
		uint32_t Component2; //0x0614
		uint32_t N0000084A; //0x0618
		uint32_t N00001197; //0x061C
		uint32_t N0000084B; //0x0620
		char pad_0624[4]; //0x0624
		uint32_t N0000084C; //0x0628
		char pad_062C[4]; //0x062C
		uint32_t N0000084D; //0x0630
		char pad_0634[4]; //0x0634
		uint32_t N0000084E; //0x0638
		char pad_063C[4]; //0x063C
		uint32_t N0000084F; //0x0640
		char pad_0644[4]; //0x0644
		uint32_t N00000850; //0x0648
		char pad_064C[4]; //0x064C
		int32_t N00000851; //0x0650
		char pad_0654[4]; //0x0654
		int32_t N00000852; //0x0658
		char pad_065C[4]; //0x065C
		int32_t N00000853; //0x0660
		char pad_0664[4]; //0x0664
		uint32_t WAPFlashLasr; //0x0668
		uint32_t N0000118C; //0x066C
		uint32_t N00000855; //0x0670
		char pad_0674[84]; //0x0674
		uint32_t N00000860; //0x06C8
		uint32_t WAPScop; //0x06CC
		char pad_06D0[96]; //0x06D0
		uint32_t WAPSupp; //0x0730
		char pad_0734[4]; //0x0734
		uint32_t COMPONENT_AT_AR_SUPP_02; //0x0738
		char pad_073C[88]; //0x073C
		uint32_t N000008F6; //0x0794
		uint32_t N0000087C; //0x0798
		uint32_t TintSpecValues; //0x079C
		uint32_t N0000087D; //0x07A0
		char pad_07A4[4]; //0x07A4
		uint32_t N0000087E; //0x07A8
		char pad_07AC[12]; //0x07AC
		uint32_t N00000880; //0x07B8
		char pad_07BC[356]; //0x07BC
		float HudRange; //0x0920
		char pad_0924[4172]; //0x0924
	}; //Size: 0x1970

	class CWeapon
	{
	public:
		char pad_0000[64]; //0x0000
		class CWeaponInfo* m_weapon_info; //0x0040
		char pad_0048[64]; //0x0048
	}; //Size: 0x0088

	class CDriveByWeaponGroupDefault
	{
	public:
		Hash m_driveby_default_unarmed; //0x0000
	private:
		char pad_0004[4]; //0x0004
	public:
		rage::atArray<Hash> m_driveby_default_unarmed_weapon_group_names; //0x0008
		rage::atArray<Hash> m_driveby_default_unarmed_weapon_type_names; //0x0018
	private:
		char pad_0028[8]; //0x0028
	public:
		Hash m_driveby_default_one_handed; //0x0030
	private:
		char pad_0034[4]; //0x0034
	public:
		rage::atArray<Hash> m_driveby_default_one_handed_weapon_group_names; //0x0038
		rage::atArray<Hash> m_driveby_default_one_handed_weapon_type_names; //0x0048
	private:
		char pad_0058[8]; //0x0058
	public:
		Hash m_driveby_default_two_handed; //0x0060
	private:
		char pad_0064[4]; //0x0064
	public:
		rage::atArray<Hash> m_driveby_default_two_handed_weapon_group_names; //0x0068
		rage::atArray<Hash> m_driveby_default_two_handed_weapon_type_names; //0x0078
	private:
		char pad_0088[8]; //0x0088
	public:
		Hash m_driveby_heli_two_handed; //0x0090
	private:
		char pad_0094[4]; //0x0094
	public:
		rage::atArray<Hash> m_driveby_heli_two_handed_weapon_group_names; //0x0098
		rage::atArray<Hash> m_driveby_heli_two_handed_weapon_type_names; //0x00A8
	private:
		char pad_00B8[56]; //0x00B8
	public:
		Hash m_driveby_heli_rpg; //0x00F0
	private:
		char pad_00F4[4]; //0x00F4
	public:
		rage::atArray<Hash> m_driveby_heli_rpg_weapon_group_names; //0x00F8
		rage::atArray<Hash> m_driveby_heli_rpg_weapon_type_names; //0x0108
	private:
		char pad_0118[8]; //0x0118
	public:
		Hash m_driveby_default_rear_one_handed; //0x0120
	private:
		char pad_0124[4]; //0x0124
	public:
		rage::atArray<Hash> m_driveby_default_rear_one_handed_weapon_group_names; //0x0128
		rage::atArray<Hash> m_driveby_default_rear_one_handed_weapon_type_names; //0x0138
	private:
		char pad_0148[8]; //0x0148
	public:
		Hash m_driveby_bike_one_handed; //0x0150
	private:
		char pad_0154[4]; //0x0154
	public:
		rage::atArray<Hash> m_driveby_bike_one_handed_weapon_group_names; //0x0158
		rage::atArray<Hash> m_driveby_bike_one_handed_weapon_type_names; //0x0168
	private:
		char pad_0178[56]; //0x0178
	public:
		Hash m_driveby_bike_melee; //0x01B0
	private:
		char pad_01B4[4]; //0x01B4
	public:
		rage::atArray<Hash> m_driveby_bike_melee_weapon_group_names; //0x01B8
		rage::atArray<Hash> m_driveby_bike_melee_weapon_type_names; //0x01C8
	private:
		char pad_01D8[56]; //0x01D8
	public:
		Hash m_driveby_mounted_throw; //0x0210
	private:
		char pad_0214[4]; //0x0214
	public:
		rage::atArray<Hash> m_driveby_mounted_throw_weapon_group_names; //0x0218
		rage::atArray<Hash> m_driveby_mounted_throw_weapon_type_names; //0x0228
	private:
		char pad_0238[8]; //0x0238
	public:
		Hash m_driveby_throw; //0x0240
	private:
		char pad_0244[4]; //0x0244
	public:
		rage::atArray<Hash> m_driveby_throw_weapon_group_names; //0x0248
		rage::atArray<Hash> m_driveby_throw_weapon_type_names; //0x0258
	private:
		char pad_0268[8]; //0x0268
	public:
		Hash m_driveby_vehicle_weapon_group; //0x0270
	public:
		char pad_0274[4]; //0x0274
		rage::atArray<Hash> m_driveby_vehicle_weapon_group_weapon_group_names; //0x0278
		rage::atArray<Hash> m_driveby_vehicle_weapon_group_weapon_type_names; //0x0288
	private:
		char pad_0298[8]; //0x0298
	}; //Size: 0x02A0

	class CVehicleDriveByMetadataMgr
	{
	public:
		class CDrivebyWeaponGroups* m_drive_by_weapon_groups; //0x0000
	}; //Size: 0x0008

	class CDrivebyWeaponGroups
	{
	public:
		class CDriveByWeaponGroupDefault* m_drive_by_default; //0x0000
	}; //Size: 0x0008

	class CVehicleModelInfoLayout
	{
	private:
		char pad_0000[842]; //0x0000
	public:
		int8_t m_max_seats; //0x034A
	private:
		char pad_034B[5]; //0x034B
	public:
		class CVehicleLayoutMetaData* m_layout_metadata; //0x0350
	}; //Size: 0x0358

	class CVehicleLayoutMetaData
	{
	private:
		char pad_0000[8]; //0x0000
	public:
		class CVehicleSeatAnimInfos* m_seat_info; //0x0008
	}; //Size: 0x0010

	class CVehicleSeatAnimInfos
	{
	private:
		char pad_0000[8]; //0x0000
	public:
		class CVehicleSeatAnimInfo* m_front_left; //0x0008
	private:
		char pad_0010[8]; //0x0010
	public:
		class CVehicleSeatAnimInfo* m_front_right; //0x0018
	private:
		char pad_0020[8]; //0x0020
	public:
		class CVehicleSeatAnimInfo* m_rear_left; //0x0028
	private:
		char pad_0030[8]; //0x0030
	public:
		class CVehicleSeatAnimInfo* m_rear_right; //0x0038
	}; //Size: 0x0040

	class CVehicleSeatAnimInfo
	{
	public:
		uint32_t name; //0x0000
	private:
		char pad_0004[4]; //0x0004
	public:
		class CVehicleDriveByAnimInfo* m_drive_by_info; //0x0008
	}; //Size: 0x0010

	class CVehicleDriveByAnimInfo
	{
	public:
		uint32_t m_name; //0x0000
		float m_min_aim_sweep_heading_angle_degs; //0x0004
		float m_max_aim_sweep_heading_angle_degs; //0x0008
		float m_first_person_min_aim_sweep_heading_angle_degs; //0x000C
		float m_first_person_max_aim_sweep_heading_angle_degs; //0x0010
		float m_first_person_unarmed_min_aim_sweep_heading_angle_degs; //0x0014
		float m_first_person_unarmed_max_aim_sweep_heading_angle_degs; //0x0018
		uint64_t m_unk1; //0x001C
		float m_min_restricted_aim_sweep_heading_angle_degs; //0x0024
		float m_max_restricted_aim_sweep_heading_angle_degs; //0x0028
		float m_min_smash_window_angle_degs; //0x002C
		float m_max_smash_window_angle_degs; //0x0030
		float m_min_smash_window_angle_first_person_degs; //0x0034
		float m_max_smash_window_angle_first_person_degs; //0x0038
		float m_max_speed_param; //0x003C
		float m_max_longitudinal_lean_blend_weight_delta; //0x0040
		float m_max_lateral_lean_blend_weight_delta; //0x0044
		float m_approach_speed_to_within_max_blend_delta; //0x0048
		float m_spine_additive_blend_in_delay; //0x004C
		float m_spine_additive_blend_in_duration_still; //0x0050
		float m_spine_additive_blend_in_duration; //0x0054
		float m_spine_additive_blend_out_delay; //0x0058
		float m_spine_additive_blend_out_duration; //0x005C
		float m_min_unarmed_driveby_yaw_if_window_rolled_up; //0x0060
		float m_max_unarmed_driveby_yaw_if_window_rolled_up; //0x0064
		rage::atArray<const Hash*> m_drive_by_anim_infos; //0x0068
	}; //Size: 0x0078

	class CVehicleSeatMetadataMgr
	{
	public:
		class CDriveBySeatDefault* m_drive_by_seat_defaults; //0x0000
	}; //Size: 0x0008

	class CDriveBySeatDefault
	{
	private:
		char pad_0000[320]; //0x0000
	public:
		class CVehicleDriveByAnimInfo* m_driveby_standard_front_left; //0x0140
		class CVehicleDriveByAnimInfo* m_driveby_standard_front_right; //0x0148
		class CVehicleDriveByAnimInfo* m_driveby_standard_rear_left; //0x0150
		class CVehicleDriveByAnimInfo* m_driveby_standard_rear_right; //0x0158
	}; //Size: 0x0160
}
#pragma pack(pop)