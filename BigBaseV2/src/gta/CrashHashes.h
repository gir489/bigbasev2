#pragma once

#include "natives.hpp"
#include <unordered_set>
#include "joaat.hpp"
#include "gta\PickupRewards.h"

static std::unordered_set<Hash> crash_objects = { RAGE_JOAAT("slod_human"),RAGE_JOAAT("slod_large_quadped"),RAGE_JOAAT("slod_small_quadped"), 0xFBF7D21F, 0xFB631122, 0xE92E717E, 0xDD75614, 0x1B6ED610, 0x8077CE20, 0x6D1BB815, 0x4445C280 };

namespace big
{
	static inline bool is_crash_model(Hash model)
	{
		return crash_objects.find(model) != crash_objects.end();
	}
}