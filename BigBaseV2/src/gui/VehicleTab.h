#pragma once

namespace big
{
	class vehicle_tab
	{
	public:
		static void render_vehicle_tab();
	private:
		static void set_license_plate(std::string license_plate);
		static void fix_car();
		static void clean_car();
		static void max_stats();
		static void set_strong();
		static void flip_car();
		static void boost_vehicle();
	};
}