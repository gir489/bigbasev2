#pragma once

namespace big
{
	class world_tab
	{
	public:
		static void render_world_tab();
	private:
		static void teleport_to_waypoint();
		static void teleport_to_objective();
		static void teleport_to_selected_blip();
	};
}