#pragma once

namespace big
{
	class weapons_tab
	{
	public:
		static void render_weapons_tab();
		static void handle_vehicle_allow_all_weapons();
		static void handle_player_get_in_vehicle_weapons_callback();
		static void give_all_weapons(Ped ped);
	private:
		static void enable_unarmed_array();
		static void enable_one_handed_array();
		static void revert_unarmed_array();
		static void revert_one_armed_array();
		static void resolve_weapon(int, int);
	};
}