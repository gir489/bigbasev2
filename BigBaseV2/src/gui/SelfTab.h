#pragma once

namespace big
{
	class self_tab
	{
	public:
		static void render_self_tab();
	private:
		static void do_save_outfit(char* outfit_file_name_input);
		static void do_load_outfit(std::string* selected_outfit_file);
	};
}