#include "common.hpp"
#include <imgui.h>
#include "fiber_pool.hpp"
#include "gui.hpp"
#include "persist\PersistWeapons.h"
#include "pointers.hpp"
#include "gta\Weapons.h"
#include "CustomCode\WeaponInfo.h"
#include "CustomCode\Settings.h"
#include "WeaponsTab.h"
#include "CustomCode/PlayerInfo.h"
#include "script.hpp"

namespace big
{
	void weapons_tab::render_weapons_tab()
	{
		if (ImGui::BeginTabItem("Weapons"))
		{
			if (ImGui::Button("Get All Weapons"))
			{
				QUEUE_JOB_BEGIN_CLAUSE()
				{
					give_all_weapons(g_local_player.player_ped);
				} QUEUE_JOB_END_CLAUSE
			}

			if (ImGui::Checkbox("Eat Spaghettios", g_settings.options["eat spaghettios"].get<bool*>()))
				g_settings.save();
			ImGui::SameLine();
			if (ImGui::Checkbox("Eat Oppressor Mk IIs", g_settings.options["eat opressormk2"].get<bool*>()))
				g_settings.save();

			if (ImGui::Checkbox("Allow All Weapons In Drive-by", g_settings.options["allow all weapons in drive by"].get<bool*>()))
			{
				handle_vehicle_allow_all_weapons();
				g_settings.save();
			}
			static int all_weap_opt = g_settings.options["allow all weapons in drive by opt"];
			const char* const all_weap_opt_combo[]{ "Unarmed", "SMG" };
			ImGui::SameLine();
			ImGui::PushItemWidth(125);
			if (ImGui::Combo("Character Holds Extra Guns Like", &all_weap_opt, all_weap_opt_combo, IM_ARRAYSIZE(all_weap_opt_combo)))
			{
				g_settings.options["allow all weapons in drive by opt"] = all_weap_opt;
				if (g_settings.options["allow all weapons in drive by"])
					handle_vehicle_allow_all_weapons();
				g_settings.save();
			}
			ImGui::PopItemWidth();

			persist_weapons::do_presentation_layer();

			if (ImGui::CollapsingHeader("Weapon Hotkeys"))
			{
				if (ImGui::Checkbox("Toggle", g_settings.options["weapon hotkeys"][0].get<bool*>()))
					g_settings.save();

				static int preset_id;
				const char* const preset_combo[]{
					"1",
					"2",
					"3",
					"4",
					"5",
					"6",
				};

				static std::string weapon_1("Unassigned"), weapon_2("Unassigned"), weapon_3("Unassigned"), weapon_4("Unassigned");
				static Hash weapon_hash_1{}, weapon_hash_2{}, weapon_hash_3{}, weapon_hash_4{};

				if (weapon_hash_1 != g_settings.options["weapon hotkeys"][preset_id + 1][0])
				{
					weapon_hash_1 = g_settings.options["weapon hotkeys"][preset_id + 1][0];
					if (auto vehicle_weapon_name = weapon_info::resolve_vehicle_weapon_name(weapon_hash_1))
						weapon_1 = vehicle_weapon_name;
					else
						weapon_1 = weapon_hash_1 == 0 ? "Unassigned" : weapon_info::get_weapon_name(weapon_hash_1);
				}
				if (weapon_hash_2 != g_settings.options["weapon hotkeys"][preset_id + 1][1])
				{
					weapon_hash_2 = g_settings.options["weapon hotkeys"][preset_id + 1][1];
					if (auto vehicle_weapon_name = weapon_info::resolve_vehicle_weapon_name(weapon_hash_2))
						weapon_2 = vehicle_weapon_name;
					else
						weapon_2 = weapon_hash_2 == 0 ? "Unassigned" : weapon_info::get_weapon_name(weapon_hash_2);
				}
				if (preset_id == 1)
				{
					if (weapon_hash_3 != g_settings.options["weapon hotkeys"][preset_id + 1][2])
					{
						weapon_hash_3 = g_settings.options["weapon hotkeys"][preset_id + 1][2];
						if (auto vehicle_weapon_name = weapon_info::resolve_vehicle_weapon_name(weapon_hash_3))
							weapon_3 = vehicle_weapon_name;
						else
							weapon_3 = weapon_hash_3 == 0 ? "Unassigned" : weapon_info::get_weapon_name(weapon_hash_3);
					}
					if (weapon_hash_4 != g_settings.options["weapon hotkeys"][preset_id + 1][3])
					{
						weapon_hash_4 = g_settings.options["weapon hotkeys"][preset_id + 1][3];
						if (auto vehicle_weapon_name = weapon_info::resolve_vehicle_weapon_name(weapon_hash_4))
							weapon_4 = vehicle_weapon_name;
						else
							weapon_4 = weapon_hash_4 == 0 ? "Unassigned" : weapon_info::get_weapon_name(weapon_hash_4);
					}
				}

				ImGui::PushItemWidth(250);
				ImGui::Combo("Presets", &preset_id, preset_combo, IM_ARRAYSIZE(preset_combo));
				ImGui::SameLine();
				ImGui::Text("Weapon 1: %s", weapon_1.c_str());
				ImGui::SameLine();
				ImGui::Text("Weapon 2: %s", weapon_2.c_str());

				if (preset_id == 1)
				{
					ImGui::SameLine();
					ImGui::Text("Weapon 3: %s", weapon_3.c_str());
					ImGui::SameLine();
					ImGui::Text("Weapon 4: %s", weapon_4.c_str());
				}

				if (ImGui::Button("Set Weapon 1"))
					resolve_weapon(preset_id, 0);
				ImGui::SameLine();
				if (ImGui::Button("Set Weapon 2"))
					resolve_weapon(preset_id, 1);

				if (preset_id == 1)
				{
					ImGui::SameLine();
					if (ImGui::Button("Set Weapon 3"))
						resolve_weapon(preset_id, 2);
					ImGui::SameLine();
					if (ImGui::Button("Set Weapon 4"))
						resolve_weapon(preset_id, 3);
				}

				if (ImGui::Button("Reset"))
				{
					g_settings.options["weapon hotkeys"][preset_id + 1][0] = 0;
					g_settings.options["weapon hotkeys"][preset_id + 1][1] = 0;
					if (preset_id == 1)
					{
						g_settings.options["weapon hotkeys"][2][2] = 0;
						g_settings.options["weapon hotkeys"][2][3] = 0;
					}
				}
				ImGui::PopItemWidth();
			}

			if (ImGui::CollapsingHeader("Ammunation"))
			{
				static std::string spawn_weapon_name;
				static Hash spawn_weapon_hash;

				ImGui::PushItemWidth(300);
				if (ImGui::BeginCombo("Weapons", spawn_weapon_name.c_str()))
				{
					for (auto const& weapon : weapon_info::get_display_weapons())
					{
						bool is_selected = weapon.second == spawn_weapon_hash;
						if (ImGui::Selectable(weapon.first.c_str(), is_selected, ImGuiSelectableFlags_None))
						{
							spawn_weapon_name = weapon.first;
							spawn_weapon_hash = weapon.second;
						}
						if (is_selected)
							ImGui::SetItemDefaultFocus();
					}

					ImGui::EndCombo();
				}
				ImGui::PopItemWidth();
				ImGui::SameLine();
				if (ImGui::Button("Give Weapon"))
				{
					QUEUE_JOB_BEGIN_CLAUSE()
					{
						WEAPON::GIVE_WEAPON_TO_PED(g_local_player.player_ped, spawn_weapon_hash, 9999, false, true);
					} QUEUE_JOB_END_CLAUSE
				}
				ImGui::SameLine();
				if (ImGui::Button("Remove Weapon"))
				{
					QUEUE_JOB_BEGIN_CLAUSE()
					{
						WEAPON::REMOVE_WEAPON_FROM_PED(g_local_player.player_ped, spawn_weapon_hash);
					} QUEUE_JOB_END_CLAUSE
				}

				if (g_local_player.weapon != 0 && g_local_player.weapon != WEAPON_UNARMED && g_local_player.weapon != GADGET_OBJECT)
				{
					static int last_weapon;
					static std::string attachment_name;
					static Hash attachment_hash;

					if (last_weapon != g_local_player.weapon)
					{
						last_weapon = g_local_player.weapon;
						attachment_name.clear();
						attachment_hash = 0;
					}

					ImGui::Separator();

					ImGui::PushItemWidth(250);
					auto attachments = weapon_info::get_weapon_attachments(g_local_player.weapon);
					if (attachments.size() > 0)
					{
						if (ImGui::BeginCombo("Attachments", attachment_name.c_str()))
						{
							for (auto attachment : attachments)
							{
								if (attachment.loc_name != "WCT_INVALID" && attachment.loc_name != "WCT_RAIL")
								{
									bool is_selected = attachment.hash == attachment_hash;
									if (ImGui::Selectable(weapon_info::get_attachment_name(attachment.loc_name), is_selected, ImGuiSelectableFlags_None))
									{
										attachment_name = weapon_info::get_attachment_name(attachment.loc_name);
										attachment_hash = attachment.hash;
									}
									if (is_selected)
										ImGui::SetItemDefaultFocus();
								}
							}

							ImGui::EndCombo();
						}
						ImGui::SameLine();
						if (ImGui::Button("Add to Weapon"))
						{
							QUEUE_JOB_BEGIN_CLAUSE()
							{
								WEAPON::GIVE_WEAPON_COMPONENT_TO_PED(g_local_player.player_ped, g_local_player.weapon, attachment_hash);
							} QUEUE_JOB_END_CLAUSE
						}
						ImGui::SameLine();
						if (ImGui::Button("Remove from Weapon"))
						{
							QUEUE_JOB_BEGIN_CLAUSE()
							{
								WEAPON::REMOVE_WEAPON_COMPONENT_FROM_PED(g_local_player.player_ped, g_local_player.weapon, attachment_hash);
							} QUEUE_JOB_END_CLAUSE
						}
					}
					ImGui::PopItemWidth();

					if (ImGui::Button("Refill Ammo"))
					{
						QUEUE_JOB_BEGIN_CLAUSE()
						{
							int max_ammo;
							if (WEAPON::GET_MAX_AMMO(g_local_player.player_ped, g_local_player.weapon, &max_ammo))
								WEAPON::SET_PED_AMMO(g_local_player.player_ped, g_local_player.weapon, max_ammo, 0);
						} QUEUE_JOB_END_CLAUSE
					}

					static const char* default_tints[]{
						"Black tint",
						"Green tint",
						"Gold tint",
						"Pink tint",
						"Army tint",
						"LSPD tint",
						"Orange tint",
						"Platinum tint"
					};
					static const char* mk2_tints[]{
						"Classic Black",
						"Classic Grey",
						"Classic Two - Tone",
						"Classic White",
						"Classic Beige",
						"Classic Green",
						"Classic Blue",
						"Classic Earth",
						"Classic Brown & Black",
						"Red Contrast",
						"Blue Contrast",
						"Yellow Contrast",
						"Orange Contrast",
						"Bold Pink",
						"Bold Purple & Yellow",
						"Bold Orange",
						"Bold Green & Purple",
						"Bold Red Features",
						"Bold Green Features",
						"Bold Cyan Features",
						"Bold Yellow Features",
						"Bold Red & White",
						"Bold Blue & White",
						"Metallic Gold",
						"Metallic Platinum",
						"Metallic Grey & Lilac",
						"Metallic Purple & Lime",
						"Metallic Red",
						"Metallic Green",
						"Metallic Blue",
						"Metallic White & Aqua",
						"Metallic Red & Yellow"
					};
					static int tint;

					if (g_local_player.current_weapon_tint_count == 8)
					{
						ImGui::Combo("Tints", &tint, default_tints, IM_ARRAYSIZE(default_tints));
					}
					else
					{
						ImGui::Combo("Tints", &tint, mk2_tints, IM_ARRAYSIZE(mk2_tints));
					}
					ImGui::SameLine();
					if (ImGui::Button("Apply"))
					{
						QUEUE_JOB_BEGIN_CLAUSE()
						{
							WEAPON::SET_PED_WEAPON_TINT_INDEX(g_local_player.player_ped, g_local_player.weapon, tint);
						} QUEUE_JOB_END_CLAUSE
					}

				}
			}

			ImGui::EndTabItem();
		}
	}

	void weapons_tab::give_all_weapons(Ped ped)
	{
		srand((unsigned int)time(NULL));
		for (auto weapon_info : weapon_info::get_all_weapons())
		{
			WEAPON::GIVE_WEAPON_TO_PED(ped, weapon_info.first, 9999, false, false);
			for (auto component : weapon_info.second.attachments)
			{
				WEAPON::GIVE_WEAPON_COMPONENT_TO_PED(ped, weapon_info.first, component.hash);
			}
			WEAPON::GIVE_WEAPON_TO_PED(ped, weapon_info.first, 9999, false, false);
			WEAPON::SET_PED_WEAPON_TINT_INDEX(ped, weapon_info.first, rand() % WEAPON::GET_WEAPON_TINT_COUNT(weapon_info.first));
		}
	}

	void weapons_tab::handle_vehicle_allow_all_weapons()
	{
		if (g_pointers->m_driveby_metadata_mgr->m_drive_by_weapon_groups == nullptr)
			return;
		if (g_running == false)
		{
			revert_unarmed_array();
			revert_one_armed_array();
			return;
		};
		if (g_settings.options["allow all weapons in drive by"])
		{
			if (g_settings.options["allow all weapons in drive by opt"] == 0)
			{
				revert_one_armed_array();
				enable_unarmed_array();
			}
			else
			{
				enable_one_handed_array();
				revert_unarmed_array();
			}
		}
		else
		{
			revert_unarmed_array();
			revert_one_armed_array();
		}
	}

	void weapons_tab::handle_player_get_in_vehicle_weapons_callback()
	{
		auto num_seats = g_local_player.vehicle_ptr->m_model_info->m_vehicle_layout->m_max_seats;
		auto seat_info = g_local_player.vehicle_ptr->m_model_info->m_vehicle_layout->m_layout_metadata->m_seat_info;
		auto defaults = g_pointers->m_vehicle_layout_metadata_mgr->m_drive_by_seat_defaults;
		if (seat_info->m_front_left->m_drive_by_info == nullptr)
			seat_info->m_front_left->m_drive_by_info = defaults->m_driveby_standard_front_left;
		if (num_seats > 1 && seat_info->m_front_right->m_drive_by_info == nullptr)
			seat_info->m_front_right->m_drive_by_info = defaults->m_driveby_standard_front_right;
		if (num_seats > 2 && seat_info->m_rear_left->m_drive_by_info == nullptr)
			seat_info->m_rear_left->m_drive_by_info = defaults->m_driveby_standard_rear_left;
		if (num_seats > 3 && seat_info->m_rear_right->m_drive_by_info == nullptr)
			seat_info->m_rear_right->m_drive_by_info = defaults->m_driveby_standard_rear_right;
	}

	void weapons_tab::enable_unarmed_array()
	{
		if (g_pointers->m_driveby_metadata_mgr->m_drive_by_weapon_groups->m_drive_by_default->m_driveby_default_unarmed_weapon_group_names.data() == nullptr)
		{
			rage::atArray<Hash> unarmed_group_hash;
			unarmed_group_hash.append({ GROUP_MG, GROUP_RIFLE, GROUP_SHOTGUN, GROUP_HEAVY, GROUP_SNIPER, GROUP_SMG });
			g_pointers->m_driveby_metadata_mgr->m_drive_by_weapon_groups->m_drive_by_default->m_driveby_default_unarmed_weapon_group_names = unarmed_group_hash;
		}
	}

	void weapons_tab::enable_one_handed_array()
	{
		if (g_pointers->m_driveby_metadata_mgr->m_drive_by_weapon_groups->m_drive_by_default->m_driveby_default_one_handed_weapon_group_names.size() == 1)
		{
			rage::atArray<Hash> one_handed_groups = g_pointers->m_driveby_metadata_mgr->m_drive_by_weapon_groups->m_drive_by_default->m_driveby_default_one_handed_weapon_group_names;
			one_handed_groups.append({ GROUP_MG, GROUP_RIFLE, GROUP_SHOTGUN, GROUP_HEAVY, GROUP_SNIPER, GROUP_SMG });
			g_pointers->m_driveby_metadata_mgr->m_drive_by_weapon_groups->m_drive_by_default->m_driveby_default_one_handed_weapon_group_names = one_handed_groups;
		}
	}

	void weapons_tab::revert_unarmed_array()
	{
		if (g_pointers->m_driveby_metadata_mgr->m_drive_by_weapon_groups->m_drive_by_default->m_driveby_default_unarmed_weapon_group_names.data() != nullptr)
			g_pointers->m_driveby_metadata_mgr->m_drive_by_weapon_groups->m_drive_by_default->m_driveby_default_unarmed_weapon_group_names.clear();
	}

	void weapons_tab::revert_one_armed_array()
	{
		if (g_pointers->m_driveby_metadata_mgr->m_drive_by_weapon_groups->m_drive_by_default->m_driveby_default_one_handed_weapon_group_names.size() == 7)
		{
			rage::atArray<Hash> one_handed_groups = g_pointers->m_driveby_metadata_mgr->m_drive_by_weapon_groups->m_drive_by_default->m_driveby_default_one_handed_weapon_group_names;
			one_handed_groups.clear();
			one_handed_groups.append(GROUP_PISTOL);
			g_pointers->m_driveby_metadata_mgr->m_drive_by_weapon_groups->m_drive_by_default->m_driveby_default_one_handed_weapon_group_names = one_handed_groups;
		}
	}

	void weapons_tab::resolve_weapon(int preset_id, int index)
	{
		if (g_local_player.vehicle_weapon != 0 && g_local_player.weapon == 0)
			g_settings.options["weapon hotkeys"][preset_id + 1][index] = g_local_player.vehicle_weapon;
		else if (g_local_player.weapon == 0)
			g_settings.options["weapon hotkeys"][preset_id + 1][index] = WEAPON_UNARMED;
		else
			g_settings.options["weapon hotkeys"][preset_id + 1][index] = g_local_player.weapon;
		g_settings.save();
	}
}