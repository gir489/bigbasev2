#pragma once
#include <vector>
#include <imgui.h>
#include "gta\player.hpp"
#include "CustomCode/PlayerAttachments.h"

namespace big
{
	namespace playerlist_info
	{
		struct playerlist_info
		{
			std::string name;
			int id;
			ImVec4 color;
		};
	}
	class players_tab
	{
	public:
		static void render_players_tab();
		static void run_tick();
		
		static inline std::map<std::string, playerlist_info::playerlist_info> playerlist;
	private:
		static ImVec4 get_color_type(Player player);
		static void do_kick();
		static void do_remove_weapons();
		static void do_attach(player_attachment::player_attachment& selected_attachment);
		static void do_attach_all();
		static void do_clear_attach();
	};
}