#include "common.hpp"
#include <imgui.h>
#include "gta_util.hpp"
#include "persist\persistped.h"
#include "fiber_pool.hpp"
#include "gui.hpp"
#include "SelfTab.h"
#include "helpers/RageHelper.h"
#include "CustomCode/Settings.h"
#include "CustomCode/PlayerInfo.h"
#include "script.hpp"
#include "script_global.hpp"

namespace big
{
	void self_tab::render_self_tab()
	{
		if (ImGui::BeginTabItem("Self"))
		{
			if (ImGui::Button("Stop Cutscene"))
			{
				QUEUE_JOB_BEGIN_CLAUSE()
				{
					CUTSCENE::STOP_CUTSCENE_IMMEDIATELY();
					SCRIPT::SHUTDOWN_LOADING_SCREEN();
					SCRIPT::SET_NO_LOADING_SCREEN(TRUE);
				} QUEUE_JOB_END_CLAUSE
			}
			if (auto player_info = g_local_player.player_info)
			{
				ImGui::PushItemWidth(100.f);
				ImGui::SameLine();
				if (ImGui::SliderInt("Wanted Level", &player_info->m_wanted_level, 0, 5))
				{
					g_settings.options["lock wanted level"] = player_info->m_wanted_level;
					g_settings.save();
				}
				ImGui::SameLine();
				if (ImGui::Checkbox("Lock Wanted Level", g_settings.options["lock wanted"].get<bool*>()))
				{
					g_settings.options["lock wanted level"] = player_info->m_wanted_level;
					g_settings.save();
				}
				ImGui::PopItemWidth();
			}

			if (ImGui::Checkbox("Disable Ragdoll", g_settings.options["disable ragdoll"].get<bool*>()))
			{
				PED::SET_PED_CAN_RAGDOLL(g_local_player.player_ped, g_settings.options["disable ragdoll"]);
				PED::SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(g_local_player.player_ped, g_settings.options["disable ragdoll"]);
				g_settings.save();
			}
			ImGui::SameLine();
			if (ImGui::Checkbox("Infinite Ammo", g_settings.options["infinite ammo"].get<bool*>()))
				g_settings.save();
			ImGui::SameLine();
			if (ImGui::Checkbox("Super Jump", g_settings.options["super jump"].get<bool*>()))
				g_settings.save();
			ImGui::SameLine();
			if (ImGui::Checkbox("Super Run", g_settings.options["super run"].get<bool*>()))
				g_settings.save();
			ImGui::SameLine();
			if (ImGui::Checkbox("Mobile Radio", g_settings.options["mobile radio"].get<bool*>()))
				g_settings.save();

			if (ImGui::Checkbox("Godmode", g_settings.options["godmode"].get<bool*>()))
			{
				if (auto ped = g_local_player.ped_ptr)
				{
					if (ped->is_invincible())
						ped->disable_invincible();
				}
				g_settings.save();
			}
			ImGui::SameLine();
			if (ImGui::Checkbox("Semi-Godmode", g_settings.options["semi-godmode"].get<bool*>()))
				g_settings.save();
			ImGui::SameLine();
			if (ImGui::Checkbox("Explosive Melee", g_settings.options["explosive melee"].get<bool*>()))
				g_settings.save();
			ImGui::SameLine();
			if (ImGui::Checkbox("Explosive Ammo", g_settings.options["explosive ammo"].get<bool*>()))
				g_settings.save();

			ImGui::Separator();
			auto outfit_files = persist_ped::list_files();
			static std::string* selected_outfit_file = g_settings.options["selected outfit file"].get<std::string*>();
			ImGui::PushItemWidth(250);
			ImGui::Text("Saved Outfits");
			if (ImGui::ListBoxHeader("##empty", ImVec2(200, 200)))
			{
				for (auto pair : outfit_files)
				{
					if (ImGui::Selectable(pair.c_str(), selected_outfit_file->_Equal(pair)))
					{
						*selected_outfit_file = pair;
						g_settings.save();
					}
				}
				ImGui::ListBoxFooter();
			}
			ImGui::SameLine();
			ImGui::BeginGroup();
			static char outfit_file_name_input[50]{};
			ImGui::InputText("Outfit File Name", outfit_file_name_input, IM_ARRAYSIZE(outfit_file_name_input));
			if (ImGui::IsItemActive())
				g_gui.m_using_keyboard = true;
			ImGui::SameLine();
			if (ImGui::Button("Save Outfit"))
			{
				QUEUE_JOB_BEGIN_CLAUSE()
				{
					do_save_outfit(outfit_file_name_input);
				} QUEUE_JOB_END_CLAUSE
			}
			ImGui::Text("Force Outfit Options");
			if (ImGui::IsItemHovered())
				ImGui::SetTooltip("Once checked, the game will try to set the components from the selected outfit JSON every frame.");
			if (ImGui::Checkbox("Freemode Only", g_settings.options["persist outfit only in freemode"].get<bool*>()))
				g_settings.save();
			ImGui::SameLine();
			if (ImGui::Checkbox("Always Clean Ped", g_settings.options["always clean ped"].get<bool*>()))
				g_settings.save();
			ImGui::SameLine();
			if (ImGui::Checkbox("All", g_settings.options["persist outfit all"].get<bool*>()))
				g_settings.save();

			if (ImGui::Checkbox("Clothes", g_settings.options["persist outfit clothes"].get<bool*>()))
				g_settings.save();
			ImGui::SameLine();
			if (ImGui::Checkbox("Props", g_settings.options["persist outfit props"].get<bool*>()))
				g_settings.save();
			ImGui::SameLine();
			if (ImGui::Checkbox("Model", g_settings.options["persist outfit model"].get<bool*>()))
				g_settings.save();
			ImGui::SameLine();
			if (ImGui::Checkbox("Face", g_settings.options["persist outfit face"].get<bool*>()))
				g_settings.save();
			ImGui::SameLine();
			if (ImGui::Checkbox("Head", g_settings.options["persist outfit head"].get<bool*>()))
				g_settings.save();
			ImGui::SameLine();
			if (ImGui::Checkbox("Tattoos", g_settings.options["persist outfit tattoos"].get<bool*>()))
				g_settings.save();

			if (ImGui::Button("Load Outfit"))
			{
				QUEUE_JOB_BEGIN_CLAUSE()
				{
					do_load_outfit(selected_outfit_file);
				} QUEUE_JOB_END_CLAUSE
			}
			ImGui::EndGroup();

			static char new_name_input[0x14];
			auto spoof_name = g_settings.options["spoof name"].get<std::string>();
			spoof_name.copy(new_name_input, 0x14, 0);
			if (ImGui::InputText("Name", new_name_input, IM_ARRAYSIZE(new_name_input)))
			{
				g_settings.options["spoof name"] = &new_name_input[0];
				g_settings.save();
			}
			ImGui::PopItemWidth();
			ImGui::PushItemWidth(100.f);
			if (ImGui::IsItemActive())
				g_gui.m_using_keyboard = true;
			ImGui::SameLine();
			if (ImGui::Button("Revert Name"))
			{
				g_settings.options["spoof name"].clear();
				g_settings.save();
				ZeroMemory(new_name_input, 0x14);
			}
			ImGui::PopItemWidth();
			
			ImGui::PushItemWidth(100.f);
			ImGui::SameLine();
			if (ImGui::InputScalar("SCID", ImGuiDataType_U64, g_settings.options["spoof scid"].get<uint64_t*>()))
				g_settings.save();
			if (ImGui::IsItemActive())
				g_gui.m_using_keyboard = true;
			ImGui::SameLine();
			if (ImGui::Button("Revert SCID"))
			{
				g_settings.options["spoof scid"].clear();
				g_settings.save();
			}
			ImGui::PopItemWidth();


			if (auto player_info = g_local_player.player_info)
			{
				ImGui::PushItemWidth(125.0f);
				const char* const view_modes[] = { "None", "First Person", "Third Person" };
				static script_global view_mode_global = script_global(Globals::ViewMode).at(Globals::ViewMode_Offset);
				ImGui::Combo("View Mode Lock", view_mode_global.as<int*>(), view_modes, IM_ARRAYSIZE(view_modes));
				ImGui::InputFloat("Run Speed", &player_info->m_run_speed, 0.5f, 1.f, "%.1f");
				ImGui::SameLine();
				ImGui::InputFloat("Swim Speed", &player_info->m_swim_speed, 0.5f, 1.f, "%.1f");
				ImGui::PopItemWidth();
			}

			ImGui::EndTabItem();
		}
	}

	void self_tab::do_save_outfit(char* outfit_file_name_input)
	{
		std::string outfit_file_name = outfit_file_name_input;
		outfit_file_name.append(".json");
		persist_ped::save_ped(outfit_file_name);
		ZeroMemory(outfit_file_name_input, sizeof(outfit_file_name_input));
	}

	void self_tab::do_load_outfit(std::string* selected_outfit_file)
	{
		if (!selected_outfit_file->empty())
			persist_ped::load_ped(*selected_outfit_file);
	}
}