#include "common.hpp"
#include <imgui.h>
#include "fiber_pool.hpp"
#include "gui.hpp"
#include "script.hpp"
#include "helpers\NetworkHelper.h"
#include "gta_util.hpp"
#include "gta\VehicleValues.h"
#include "Persist\PersistPed.h"
#include "Persist\PersistCar.h"
#include "Persist\PersistModder.h"
#include "PlayersTab.h"
#include "gta\PickupRewards.h"
#include "CustomCode/Settings.h"
#include "CustomCode\WeaponInfo.h"
#include "CustomCode/PlayerInfo.h"
#include "CustomCode/PlayerAttachments.h"
#include "gui\WeaponsTab.h"

namespace big
{
	void players_tab::render_players_tab()
	{
		if (ImGui::BeginTabItem("Players"))
		{
			if (ImGui::ListBoxHeader("##playerlist", ImVec2(200, 700)))
			{
				for (auto pair : playerlist)
				{
					bool has_color = pair.second.color.w != 0;
					if (has_color)
						ImGui::PushStyleColor(ImGuiCol_Text, pair.second.color);

					if (!pair.second.name.empty())
					{
						if (ImGui::Selectable(pair.second.name.c_str(), pair.second.id == g_player_info.player))
						{
							if (g_player_info.player != pair.second.id)
								g_player_info.player = pair.second.id;
						}
					}

					if (has_color)
						ImGui::PopStyleColor();
				}
				ImGui::ListBoxFooter();
			}
			g_player_info.tab_flag = true;

			if (g_player_info.player != -1)
			{
				ImGui::SameLine();
				ImGui::BeginGroup();

				if (auto player_info = gta_util::get_player_playerinfo(g_player_info.player))
					g_player_info.wanted_level = player_info->m_wanted_level;

				if (auto player_ptr = g_pointers->m_get_net_player(g_player_info.player))
				{
					if (auto net_player_data = player_ptr->get_net_data())
					{
						g_player_info.ip = net_player_data->m_online_ip;
						g_player_info.port = net_player_data->m_online_port;
						g_player_info.scid = net_player_data->m_rockstar_id;
					}
					g_player_info.is_host = player_ptr->is_host();
					g_player_info.is_modder = g_persist_modder.is_modder(player_ptr);
				}

				if (ImGui::CollapsingHeader("Player Info", ImGuiTreeNodeFlags_DefaultOpen))
				{
					ImGui::Columns(2);

					ImGui::Text("Player ID: %i", g_player_info.player);
					ImGui::Text("IP: %i.%i.%i.%i:%i SCID: %i", g_player_info.ip.m_field1, g_player_info.ip.m_field2, g_player_info.ip.m_field3, g_player_info.ip.m_field4, g_player_info.port, g_player_info.scid);
					ImGui::Text("Rank: %i", g_player_info.rank);
					ImGui::Text("Money: $%s", rage_helper::format_money(g_player_info.money).c_str());
					ImGui::Text("Kills: %i Deaths: %i KDR: %.1f", g_player_info.kills, g_player_info.deaths, g_player_info.kdr);
					ImGui::Text("NetHost: %s | ScriptHost: %s", g_player_info.is_host ? "Yes" : "No", g_player_info.is_script_host ? "Yes" : "No");

					ImGui::NextColumn();

					ImGui::Text("Health: %i/%i Armor: %i/%i", g_player_info.health, g_player_info.max_health, g_player_info.armor, g_player_info.max_armor);
					ImGui::Text("Wanted Level: %i", g_player_info.wanted_level);
					if(g_player_info.vehicle_state == 2)
						ImGui::Text("Last Vehicle: %s", g_player_info.vehicle_name.c_str());
					else
						ImGui::Text("Vehicle: %s", g_player_info.vehicle_state == 1 ? g_player_info.vehicle_name.c_str() : "None");
					ImGui::Text("Speed: %.0f MPH", g_player_info.speed);
					ImGui::Text("Street: %s", g_player_info.street_name.c_str());
					ImGui::Text("Zone: %s", g_player_info.zone_name.c_str());

					ImGui::Columns();
				}

				ImGui::Separator();

				if (g_player_info.player != g_local_player.player)
				{
					static std::uint64_t spectator_scid{};
					bool is_spectating = spectator_scid == g_player_info.scid;
					if (ImGui::Checkbox("Spectate", &is_spectating))
					{
						if (is_spectating)
							spectator_scid = g_player_info.scid;
						else
							spectator_scid = 0;
						QUEUE_JOB_BEGIN_CLAUSE(is_spectating)
						{
							NETWORK::NETWORK_SET_IN_SPECTATOR_MODE(is_spectating, g_player_info.player_ped);
							g_local_player.player_info->m_player_controls = 0;
						} QUEUE_JOB_END_CLAUSE
					}
				}
				if (*big::g_pointers->m_is_session_started && !g_settings.player_options[g_player_info.scid].empty())
				{
					if (g_player_info.player != g_local_player.player)
						ImGui::SameLine();
					ImGui::Checkbox("Off The Radar", g_settings.player_options[g_player_info.scid]["otr"].get<bool*>());
					ImGui::SameLine();
					ImGui::Checkbox("Sound Spam", g_settings.player_options[g_player_info.scid]["sound spam"].get<bool*>());
					//ImGui::SameLine();
					ImGui::Checkbox("Semi-godmode", g_settings.player_options[g_player_info.scid]["semi-godmode"].get<bool*>());
					ImGui::SameLine();
					ImGui::Checkbox("Never Wanted", g_settings.player_options[g_player_info.scid]["never wanted"].get<bool*>());
					ImGui::SameLine();
					ImGui::Checkbox("Money Drop", g_settings.player_options[g_player_info.scid]["money drop"].get<bool*>());
					ImGui::SameLine();
					ImGui::Checkbox("RP Drop", g_settings.player_options[g_player_info.scid]["rp drop"].get<bool*>());
					ImGui::Separator();
				}

				if (ImGui::Button("Teleport##1"))
				{
					QUEUE_JOB_BEGIN_CLAUSE()
					{
						Vector3 coords = ENTITY::GET_ENTITY_COORDS(g_player_info.player_ped, false);
						Entity e = g_local_player.player_ped;
						if (g_local_player.is_in_vehicle())
							e = g_local_player.vehicle;
						if (network_helper::request_control(e, REQUEST_CONTROL_TIME))
							ENTITY::SET_ENTITY_COORDS_NO_OFFSET(e, coords.x, coords.y, coords.z + 3.f, false, false, true);
					} QUEUE_JOB_END_CLAUSE
				}
				ImGui::SameLine();
				if (ImGui::Button("Explode"))
				{
					QUEUE_JOB_BEGIN_CLAUSE()
					{
						Ped ped = g_player_info.player_ped;
						rage_helper::stop_ped_vehicle(ped);
						Vector3 pos = ENTITY::GET_ENTITY_COORDS(ped, FALSE);
						for (int j = 0; j < 3; j++)
							FIRE::ADD_EXPLOSION(pos.x, pos.y, pos.z, EXPLOSION_CAR, 10.f, FALSE, TRUE, 0.f, FALSE);
					} QUEUE_JOB_END_CLAUSE
				}
				ImGui::SameLine();
				if (ImGui::Button("Blame Explode All"))
				{
					QUEUE_JOB_BEGIN_CLAUSE()
					{
						Ped ped = g_player_info.player_ped;
						for (int i = 0; i < gta::num_players; i++)
						{
							if (!NETWORK::NETWORK_IS_PLAYER_CONNECTED(i) || i == g_local_player.player || network_helper::is_player_friend(i) || i == g_player_info.player)
								continue;

							Ped victim = PLAYER::GET_PLAYER_PED_SCRIPT_INDEX(i);
							rage_helper::stop_ped_vehicle(victim);
							for (int j = 0; j < 3; j++)
								rage_helper::blame_explosion_on_ped(ped, victim);
						}
					} QUEUE_JOB_END_CLAUSE
				}
				ImGui::SameLine();
				if (ImGui::Button("Copy Ped"))
				{
					QUEUE_JOB_BEGIN_CLAUSE()
					{
						int last_seat_index = -2;

						if (g_local_player.is_in_vehicle())
						{
							for (int i = -1; i < 10; i++)
							{
								if (VEHICLE::GET_PED_IN_VEHICLE_SEAT(g_local_player.vehicle, i, 0))
									last_seat_index = i;
							}
						}

						persist_ped::clone_ped(g_player_info.player_ped, g_player_info.player);

						if (last_seat_index != -2)
							PED::SET_PED_INTO_VEHICLE(g_local_player.player_ped, g_local_player.vehicle, last_seat_index);
					} QUEUE_JOB_END_CLAUSE
				}

				if (ImGui::Button("Kick"))
				{
					QUEUE_JOB_BEGIN_CLAUSE()
					{
						do_kick();
					} QUEUE_JOB_END_CLAUSE
				}
				ImGui::SameLine();
				if (ImGui::Button("Crash 1"))
				{
					QUEUE_JOB_BEGIN_CLAUSE()
					{
						network_helper::crash_player(g_player_info.player);
					} QUEUE_JOB_END_CLAUSE
				}

				static script_global info_global = script_global(Globals::PlayerInformation);
				auto player_info_global = info_global.at(g_player_info.player, Globals::PlayerInformation_Size);
				if (*player_info_global.at(11).as<int*>() != -1) //Check if they're in a company.
				{
					ImGui::SameLine();
					if (ImGui::Button("CEO Kick"))
					{
						QUEUE_JOB_BEGIN_CLAUSE()
						{
							network_helper::ceo_kick(g_player_info.player);
						} QUEUE_JOB_END_CLAUSE
					}
					ImGui::SameLine();
					if (ImGui::Button("CEO Ban"))
					{
						QUEUE_JOB_BEGIN_CLAUSE()
						{
							int64_t args[3] = { Events::CEOBan, g_local_player.player, 1 };
							g_pointers->m_trigger_script_event(1, &args[0], 3, (1 << g_player_info.player));
						} QUEUE_JOB_END_CLAUSE
					}
				}

				if (ImGui::Button("Clear Wantel Level"))
				{
					QUEUE_JOB_BEGIN_CLAUSE()
					{
						int64_t args[3] = { Events::ClearWantedLevel, g_local_player.player, *script_global(Globals::PlayerInformation).at(g_player_info.player, Globals::PlayerInformation_Size).at(OtrAndWatedLevel).as<int*>() };
						g_pointers->m_trigger_script_event(1, &args[0], 3, (1 << g_player_info.player));
					} QUEUE_JOB_END_CLAUSE
				}
				ImGui::SameLine();
				if (ImGui::Button("Give All Weapons"))
				{
					QUEUE_JOB_BEGIN_CLAUSE()
					{
						weapons_tab::give_all_weapons(g_player_info.player_ped);
					} QUEUE_JOB_END_CLAUSE
				}
				ImGui::SameLine();
				if (ImGui::Button("Remove All Weapons"))
				{
					QUEUE_JOB_BEGIN_CLAUSE()
					{
						do_remove_weapons();
					} QUEUE_JOB_END_CLAUSE
				}

				if (g_player_info.vehicle_state != 0)
				{
					ImGui::Separator();
					ImGui::Text("Vehicle Options:");
					if (ImGui::Button("Teleport##2"))
					{
						QUEUE_JOB_BEGIN_CLAUSE()
						{
							for (int i = SEAT_BACKPASSENGER; i >= SEAT_DRIVER; i--)
								PED::SET_PED_INTO_VEHICLE(g_local_player.player_ped, g_player_info.player_vehicle, i);
						} QUEUE_JOB_END_CLAUSE
					}
					ImGui::SameLine();
					if (ImGui::Button("Clone"))
					{
						QUEUE_JOB_BEGIN_CLAUSE()
						{
							Vehicle vehicle = persist_car::clone_ped_car(g_player_info.player_ped, g_player_info.player_vehicle);
							vehicle_helper::boost_spawned_vehicle(vehicle);
						} QUEUE_JOB_END_CLAUSE
					}
					ImGui::SameLine();
					if (ImGui::Button("Eject"))
					{
						QUEUE_JOB_BEGIN_CLAUSE()
						{
							int64_t args[9] = { Events::GetOutOfCar, g_local_player.player, g_player_info.player, 0, 0, 0, 0, -1, MISC::GET_FRAME_COUNT() };
							g_pointers->m_trigger_script_event(1, &args[0], 9, (1 << g_player_info.player));
						} QUEUE_JOB_END_CLAUSE
					}
					ImGui::SameLine();
					if (ImGui::Button("Launch Forward"))
					{
						QUEUE_JOB_BEGIN_CLAUSE()
						{
							if (network_helper::request_control(g_player_info.player_vehicle, REQUEST_CONTROL_TIME))
								ENTITY::APPLY_FORCE_TO_ENTITY(g_player_info.player_vehicle, 1, 0.f, 300.f, 0.f, 0.f, 0.f, 0.f, 0, true, true, true, false, true);
						} QUEUE_JOB_END_CLAUSE
					}
					ImGui::SameLine();
					if (ImGui::Button("Launch into Sky"))
					{
						QUEUE_JOB_BEGIN_CLAUSE()
						{
							if (network_helper::request_control(g_player_info.player_vehicle, REQUEST_CONTROL_TIME))
								ENTITY::APPLY_FORCE_TO_ENTITY(g_player_info.player_vehicle, 1, 0.f, 0.f, 300.f, 0.f, 0.f, 0.f, 0, false, true, true, false, true);
						} QUEUE_JOB_END_CLAUSE
					}
					ImGui::SameLine();
					if (ImGui::Button("Stop"))
					{
						QUEUE_JOB_BEGIN_CLAUSE()
						{
							rage_helper::stop_ped_vehicle(g_player_info.player_ped);
						} QUEUE_JOB_END_CLAUSE
					}
				}

				ImGui::Separator();

				static player_attachment::player_attachment selected_attachment = player_attachments::get_attachments()[0];
				ImGui::PushItemWidth(250);
				if (ImGui::BeginCombo("Objects", selected_attachment.display_name.c_str()))
				{
					for (auto attachment : player_attachments::get_attachments())
					{
						bool is_selected = attachment.model_hash == selected_attachment.model_hash;
						if (ImGui::Selectable(attachment.display_name.c_str(), is_selected, ImGuiSelectableFlags_None))
						{
							selected_attachment = attachment;
						}
					}

					ImGui::EndCombo();
				}
				ImGui::PopItemWidth();
				ImGui::SameLine();
				if (ImGui::Button("Attach"))
				{
					QUEUE_JOB_BEGIN_CLAUSE()
					{
						do_attach(selected_attachment);
					} QUEUE_JOB_END_CLAUSE
				}
				ImGui::SameLine();
				if (ImGui::Button("Attach All"))
				{
					QUEUE_JOB_BEGIN_CLAUSE()
					{
						do_attach_all();
					} QUEUE_JOB_END_CLAUSE
				}
				ImGui::SameLine();
				if (ImGui::Button("Clear"))
				{
					QUEUE_JOB_BEGIN_CLAUSE()
					{
						do_clear_attach();
					} QUEUE_JOB_END_CLAUSE
				}
				ImGui::EndGroup();
			}

			ImGui::EndTabItem();
		}
	}

	void players_tab::run_tick()
	{
		if (*g_pointers->m_is_session_started == false)
		{
			if (playerlist.size() != 1)
			{
				playerlist.clear();
				std::string name = NETWORK::NETWORK_PLAYER_GET_NAME(g_local_player.player);
				playerlist[name] = { NETWORK::NETWORK_PLAYER_GET_NAME(g_local_player.player), g_local_player.player, ImVec4{} };
			}
		}
		else
		{
			if (gta_util::get_connected_players() != playerlist.size())
			{
				playerlist.clear();
				std::set<uint64_t> scids_connected;
				for (Player i = 0; i < gta::num_players; i++)
				{
					if (CNetGamePlayer* net_player = g_pointers->m_get_net_player(i))
					{
						network_helper::validate_scid(net_player->get_net_data());
						auto cstr_name = net_player->get_name();
						std::string name = cstr_name;
						transform(name.begin(), name.end(), name.begin(), ::tolower);
						playerlist[name] = { cstr_name, i, get_color_type(i) };
						scids_connected.emplace(net_player->get_net_data()->m_rockstar_id);
					}
				}
				g_persist_modder.clean_list(scids_connected);
			}
		}
		if (!g_player_info.tab_flag)
		{
			if (g_player_info.player != -1 && !NETWORK::NETWORK_IS_PLAYER_CONNECTED(g_player_info.player))
			{
				memset(&g_player_info, 0, sizeof(player_info_class));
				g_player_info.player = -1;
			}
			return;
		}
		else
		{
			g_player_info.tab_flag = false;
		}

		if (!NETWORK::NETWORK_IS_PLAYER_CONNECTED(g_player_info.player))
			g_player_info.player = -1;

		g_player_info.money = network_helper::get_player_stat<int>(g_player_info.player, MoneyAll);
		g_player_info.rank = network_helper::get_player_stat<int>(g_player_info.player, Rank);
		g_player_info.kills = network_helper::get_player_stat<int>(g_player_info.player, Kills);
		g_player_info.deaths = network_helper::get_player_stat<int>(g_player_info.player, Deaths);
		g_player_info.kdr = network_helper::get_player_stat<float>(g_player_info.player, KDRatio);

		g_player_info.player_ped = PLAYER::GET_PLAYER_PED_SCRIPT_INDEX(g_player_info.player);
		Vector3 coords = ENTITY::GET_ENTITY_COORDS(g_player_info.player_ped, FALSE);

		Hash street_hash, cross_street;
		PATHFIND::GET_STREET_NAME_AT_COORD(coords.x, coords.y, coords.z, &street_hash, &cross_street);

		g_player_info.street_name = HUD::GET_STREET_NAME_FROM_HASH_KEY(street_hash);
		g_player_info.zone_name = HUD::_GET_LABEL_TEXT(ZONE::GET_NAME_OF_ZONE(coords.x, coords.y, coords.z));

		g_player_info.health = ENTITY::GET_ENTITY_HEALTH(g_player_info.player_ped);
		g_player_info.max_health = ENTITY::GET_ENTITY_MAX_HEALTH(g_player_info.player_ped);

		g_player_info.armor = PED::GET_PED_ARMOUR(g_player_info.player_ped);
		g_player_info.max_armor = PLAYER::GET_PLAYER_MAX_ARMOUR(g_player_info.player);

		g_player_info.is_friend = network_helper::is_player_friend(g_player_info.player);
		g_player_info.vehicle_state = PED::IS_PED_IN_ANY_VEHICLE(g_player_info.player_ped, FALSE);
		g_player_info.is_script_host = g_player_info.player == NETWORK::NETWORK_GET_HOST_OF_SCRIPT("freemode", -1, 0);

		if (g_player_info.vehicle_state)
		{
			g_player_info.player_vehicle = PED::GET_VEHICLE_PED_IS_IN(g_player_info.player_ped, FALSE);
			g_player_info.vehicle_name = HUD::_GET_LABEL_TEXT(VEHICLE::GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(ENTITY::GET_ENTITY_MODEL(g_player_info.player_vehicle)));
		}
		else if (PED::GET_VEHICLE_PED_IS_IN(g_player_info.player_ped, TRUE) != NULL)
		{
			g_player_info.vehicle_state = 2;
			g_player_info.player_vehicle = PED::GET_VEHICLE_PED_IS_IN(g_player_info.player_ped, TRUE);
			g_player_info.vehicle_name = HUD::_GET_LABEL_TEXT(VEHICLE::GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(ENTITY::GET_ENTITY_MODEL(g_player_info.player_vehicle)));
		}

		g_player_info.speed = ENTITY::GET_ENTITY_SPEED(g_player_info.player_ped) * 2.2369362920544f;
	}

	ImVec4 players_tab::get_color_type(Player player)
	{
		ImVec4 color{};

		if (g_persist_modder.is_modder(g_pointers->m_get_net_player(player)))
			color = { 1.f, 0.17f, 0.17f, 1.f };
		if (network_helper::is_player_friend(player))
			color = { 0.f, 0.94f, 0.94f, 1.f };

		return color;
	}
	
	void players_tab::do_kick()
	{
		network_helper::kick_player(g_player_info.player);
	}

	void players_tab::do_remove_weapons()
	{
		for (auto weapon_info : weapon_info::get_all_weapons())
			WEAPON::REMOVE_WEAPON_FROM_PED(g_player_info.player_ped, weapon_info.first);
	}

	void players_tab::do_attach(player_attachment::player_attachment& selected_attachment)
	{
		network_helper::spawn_attached_object(selected_attachment, g_player_info.player);
	}

	void players_tab::do_attach_all()
	{
		for (player_attachment::player_attachment attachment : player_attachments::get_attachments())
			network_helper::spawn_attached_object(attachment, g_player_info.player);
	}

	void players_tab::do_clear_attach()
	{
		network_helper::remove_attachments(g_player_info.player_ped);
	}
}