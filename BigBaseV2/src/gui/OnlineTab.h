#pragma once

#include "script_local.hpp"

namespace big
{
	class online_tab
	{
	public:
		static void render_online_tab();
		static void get_diamond_heist_stats();
		static void set_poker_cards(GtaThread* poker_thread, bool force = false);
	private:
		static void handle_poker_cards(int card_index, int player_count, script_local anti_cheat_deck, script_local current_deck);
		static void do_apply_rank(uint32_t& rank);
		static void do_change_character();
		static void do_rp_loop(bool& do_rp);
		static void set_stat(Hash stat, int value);
		static void set_bitset(int value);
		static void set_bitset_one(int value);
	};
}