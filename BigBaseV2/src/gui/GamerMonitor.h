#pragma once
#include "PlayersTab.h"

namespace big
{
	namespace gamerlist_info
	{
		enum class gamerlist_status : uint8_t
		{
			offline = 0,
			online = 1,
			session = 2
		};

		struct gamerlist_info
		{
			std::string name;
			int id;
			gamerlist_status status;
		};
		static void to_json(nlohmann::json& j, const gamerlist_info& info)
		{
			j = nlohmann::json{ {"name", info.name}, {"id", info.id} };
		}

		static void from_json(const nlohmann::json& j, gamerlist_info& info)
		{
			j.at("name").get_to(info.name); j.at("id").get_to(info.id);
		}
	}
	class gamer_monitor
	{
	public:
		static void render_gamer_monitor();
		static void script_func();

		static inline std::map<std::string, gamerlist_info::gamerlist_info> gamer_list;

	private:
		static ImVec4 get_gamer_color(gamerlist_info::gamerlist_info info);
	};
}