#include "common.hpp"
#include <imgui.h>
#include "fiber_pool.hpp"
#include "helpers\VehicleHelper.h"
#include "persist\PersistCar.h"
#include "gui.hpp"
#include "VehicleTab.h"
#include "CustomCode\Settings.h"
#include "script.hpp"
#include "gta_util.hpp"
#include "CustomCode/PlayerInfo.h"

namespace big
{
	void vehicle_tab::render_vehicle_tab()
	{
		if (ImGui::BeginTabItem("Vehicle"))
		{
			ImGui::Text("Spawn Options");
			if (ImGui::Checkbox("In Vehicle", g_settings.options["spawn in vehicle"].get<bool*>()))
				g_settings.save();
			ImGui::SameLine();
			if (ImGui::Checkbox("Full Stats", g_settings.options["spawn vehicle boosted"].get<bool*>()))
				g_settings.save();
			ImGui::SameLine();
			if (ImGui::Checkbox("Strong", g_settings.options["spawn vehicle strong"].get<bool*>()))
				g_settings.save();
			ImGui::SameLine();
			if (ImGui::Checkbox("Godmode##1", g_settings.options["spawn vehicle godmode"].get<bool*>()))
				g_settings.save();
			ImGui::Text("Vehicle Options");
			if (ImGui::Checkbox("Always Clean", g_settings.options["always car clean"].get<bool*>()))
				g_settings.save();
			ImGui::SameLine();
			if (ImGui::Checkbox("Always Fixed", g_settings.options["always car fixed"].get<bool*>()))
				g_settings.save();
			ImGui::SameLine();
			if (ImGui::Checkbox("Always Personal Vehicle Godmode", g_settings.options["always car godmode"].get<bool*>()))
				g_settings.save();

			if (ImGui::Checkbox("Flymode", g_settings.options["vehicle flymode"].get<bool*>()))
				g_settings.save();
			static bool vehicle_godmode;
			if (auto ped = g_local_player.ped_ptr)
			{
				if (auto last_vehicle = ped->m_last_vehicle)
				{
					vehicle_godmode = last_vehicle->is_invincible();
					ImGui::SameLine();
					if (ImGui::Checkbox("Godmode##2", &vehicle_godmode))
					{
						QUEUE_JOB_BEGIN_CLAUSE()
{
							vehicle_helper::set_vehicle_godmode(g_local_player.last_vehicle, vehicle_godmode);
						} QUEUE_JOB_END_CLAUSE
					}
				}
			}
			ImGui::SameLine();
			if (ImGui::Checkbox("Infinite Boost", g_settings.options["infinite car boost"].get<bool*>()))
				g_settings.save();

			if (ImGui::CollapsingHeader("Vehicle Spawner"))
			{
				static char model_name[30]{};
				ImGui::PushItemWidth(250);
				ImGui::InputText("Input Model", model_name, IM_ARRAYSIZE(model_name));
				if (ImGui::IsItemActive())
					g_gui.m_using_keyboard = true;
				ImGui::SameLine();
				if (ImGui::Button("Spawn"))
				{
					QUEUE_JOB_BEGIN_CLAUSE()
					{
						Hash model = MISC::GET_HASH_KEY(model_name);
						if (STREAMING::IS_MODEL_IN_CDIMAGE(model) && STREAMING::IS_MODEL_VALID(model) && STREAMING::IS_MODEL_A_VEHICLE(model))
						{
							Vector3 coords = ENTITY::GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(g_local_player.player_ped, 0.0f, 5.0f, 0.0f);
							Vehicle vehicle = vehicle_helper::create_vehicle(model, coords.x, coords.y, coords.z, ENTITY::GET_ENTITY_HEADING(g_local_player.player_ped));
							vehicle_helper::boost_spawned_vehicle(vehicle);
						}
						ZeroMemory(model_name, sizeof(model_name));
					} QUEUE_JOB_END_CLAUSE
				}
				ImGui::PopItemWidth();
			}

			ImGui::Separator();
			persist_car::do_presentation_layer();
			ImGui::Separator();

			static char license_plate[9]{};
			ImGui::PushItemWidth(250);
			ImGui::InputText("License Plate", license_plate, IM_ARRAYSIZE(license_plate));
			if (ImGui::IsItemActive())
				g_gui.m_using_keyboard = true;
			ImGui::SameLine();
			if (ImGui::Button("Set License Plate"))
				set_license_plate(license_plate);
			ImGui::PopItemWidth();
			ImGui::Separator();

			if (ImGui::Button("Fix Car"))
				fix_car();
			ImGui::SameLine();
			if (ImGui::Button("Clean Car"))
				clean_car();
			ImGui::SameLine();
			if (ImGui::Button("Max Stats"))
				max_stats();
			ImGui::SameLine();
			if (ImGui::Button("Set Strong"))
				set_strong();
			ImGui::SameLine();
			if (ImGui::Button("Flip Car"))
				flip_car();
			ImGui::SameLine();
			if (ImGui::Button("Boost Vehicle"))
				boost_vehicle();

			if (ImGui::Button("Launch Forward"))
			{
				QUEUE_JOB_BEGIN_CLAUSE()
				{
					if (network_helper::request_control(g_local_player.vehicle, REQUEST_CONTROL_TIME))
						ENTITY::APPLY_FORCE_TO_ENTITY(g_local_player.vehicle, 1, 0.f, 300.f, 0.f, 0.f, 0.f, 0.f, 0, true, true, true, false, true);
				} QUEUE_JOB_END_CLAUSE
			}
			ImGui::SameLine();
			if (ImGui::Button("Launch into Sky"))
			{
				QUEUE_JOB_BEGIN_CLAUSE()
				{
					if (network_helper::request_control(g_local_player.vehicle, REQUEST_CONTROL_TIME))
						ENTITY::APPLY_FORCE_TO_ENTITY(g_local_player.vehicle, 1, 0.f, 0.f, 300.f, 0.f, 0.f, 0.f, 0, false, true, true, false, true);
				} QUEUE_JOB_END_CLAUSE
			}
			if (g_local_player.personal_vehicle != -1 && *g_pointers->m_is_session_started)
			{
				ImGui::SameLine();
				if (ImGui::Button("Teleport In To Personal Vehicle"))
				{
					QUEUE_JOB_BEGIN_CLAUSE()
					{
						for (int i = SEAT_BACKPASSENGER; i >= SEAT_DRIVER; i--)
							PED::SET_PED_INTO_VEHICLE(g_local_player.player_ped, g_local_player.personal_vehicle, i);
					} QUEUE_JOB_END_CLAUSE
				}
			}
			ImGui::EndTabItem();
		}
	}

	void vehicle_tab::set_license_plate(std::string license_plate)
	{
		static std::string license_plate_with_spaces;;
		license_plate_with_spaces = "         ";
		auto license_plate_input_length = license_plate.length();
		license_plate_with_spaces.replace((int)(3.5 - (license_plate_input_length / 2)), license_plate_input_length, license_plate);
		QUEUE_JOB_BEGIN_CLAUSE()
		{
			if (g_local_player.is_in_vehicle() && network_helper::request_control(g_local_player.vehicle, REQUEST_CONTROL_TIME))
				VEHICLE::SET_VEHICLE_NUMBER_PLATE_TEXT(g_local_player.vehicle, license_plate_with_spaces.c_str());
		} QUEUE_JOB_END_CLAUSE
	}

	void vehicle_tab::fix_car()
	{
		QUEUE_JOB_BEGIN_CLAUSE()
		{
			if (g_local_player.is_in_vehicle())
				vehicle_helper::fix_vehicle(g_local_player.vehicle);
		} QUEUE_JOB_END_CLAUSE
	}

	void vehicle_tab::clean_car()
	{
		QUEUE_JOB_BEGIN_CLAUSE()
		{
			if (g_local_player.is_in_vehicle())
				vehicle_helper::clear_dirt(g_local_player.vehicle);
		} QUEUE_JOB_END_CLAUSE
	}
	void vehicle_tab::max_stats()
	{
		QUEUE_JOB_BEGIN_CLAUSE()
		{
			if (g_local_player.is_in_vehicle())
				vehicle_helper::set_max_stats(g_local_player.vehicle);
		} QUEUE_JOB_END_CLAUSE
	}
	void vehicle_tab::set_strong()
	{
		QUEUE_JOB_BEGIN_CLAUSE()
		{
			if (g_local_player.is_in_vehicle())
				vehicle_helper::set_vehicle_strong(g_local_player.vehicle);
		} QUEUE_JOB_END_CLAUSE
	}
	void vehicle_tab::flip_car()
	{
		QUEUE_JOB_BEGIN_CLAUSE()
{
			if (g_local_player.is_in_vehicle())
				vehicle_helper::flip_car(g_local_player.vehicle);
		} QUEUE_JOB_END_CLAUSE
	}
	void vehicle_tab::boost_vehicle()
	{
		QUEUE_JOB_BEGIN_CLAUSE()
		{
			if (g_local_player.is_in_vehicle())
				VEHICLE::SET_VEHICLE_FORWARD_SPEED(g_local_player.vehicle, VEHICLE::GET_VEHICLE_MODEL_ESTIMATED_MAX_SPEED(ENTITY::GET_ENTITY_MODEL(g_local_player.vehicle)));
		} QUEUE_JOB_END_CLAUSE
	}
}