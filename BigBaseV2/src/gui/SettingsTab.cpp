#include "common.hpp"
#include <imgui.h>
#include "gui.hpp"
#include "SettingsTab.h"
#include "CustomCode/Settings.h"
#include "CustomCode/PlayerInfo.h"
#include "script_global.hpp"
#include "gta/joaat.hpp"
#include "gui/WeaponsTab.h"
#include "pointers.hpp"
#include "gta_util.hpp"
#include "script.hpp"
#include "gui/PlayersTab.h"
#include "helpers/NetworkHelper.h"

namespace big
{
	void settings_tab::render_settings_tab()
	{
		if (ImGui::BeginTabItem("Settings"))
		{
			if (ImGui::Checkbox("Log Script Events", g_settings.options["log script events"].get<bool*>()))
				g_settings.save();
			ImGui::SameLine();
			ImGui::PushItemWidth(100.f);
			static const char* const network_events_combo[] = { "Off", "On", "All" };
			if (ImGui::Combo("Log Network Events", (PINT)g_settings.options["log network events"].get<int64_t*>(), network_events_combo, IM_ARRAYSIZE(network_events_combo))) //if (ImGui::Checkbox("Log Network Events", g_settings.options["log network events"].get<bool*>()))
				g_settings.save();
			ImGui::PopItemWidth();
			ImGui::SameLine();
			if (ImGui::Checkbox("Log Object Events", g_settings.options["log object events"].get<bool*>()))
				g_settings.save();
			ImGui::SameLine();
			if (ImGui::Checkbox("Log Chat Events", g_settings.options["log chat events"].get<bool*>()))
				g_settings.save();

			ImGui::Separator();

			if (ImGui::Checkbox("Auto Kick on Report", g_settings.options["auto kick on report"].get<bool*>()))
				g_settings.save();
			ImGui::SameLine();
			if (ImGui::Checkbox("Auto Kick on Vote Kick", g_settings.options["auto kick on vote kick"].get<bool*>()))
				g_settings.save();

			if (ImGui::Checkbox("Gamer Mode", g_settings.options["gamer mode"].get<bool*>()))
				g_settings.save();
			ImGui::PopItemWidth();

			if (ImGui::CollapsingHeader("Script Event Tester"))
			{
				static event_json::event_json event_obj{};
				ImGui::SetNextItemWidth(200.f);
				ImGui::SliderInt("Size Of Event Array", &event_obj.size_of_args_array, 2, 53);
				event_obj.args.resize(event_obj.size_of_args_array);
				event_obj.arg_is_hex.resize(event_obj.size_of_args_array - 2);
				ImGui::SetNextItemWidth(300.f);
				ImGui::InputScalar("Event ID", ImGuiDataType_S64, &event_obj.args[0]);
				for (int i = 2; i < event_obj.size_of_args_array; i++) 
				{
					ImGui::SetNextItemWidth(200.f);
					ImGui::InputScalar(fmt::format("Arg #{}", i).c_str(), ((event_obj.arg_is_hex[i-2]) ? ImGuiDataType_U64 : ImGuiDataType_S64), &event_obj.args[i], NULL, NULL, ((event_obj.arg_is_hex[i-2]) ? "%p" : NULL), ((event_obj.arg_is_hex[i-2]) ? ImGuiInputTextFlags_CharsHexadecimal : ImGuiInputTextFlags_CharsDecimal));
					ImGui::SameLine();
					ImGui::SetNextItemWidth(50.f);
					bool arg_is_hex_tmp = event_obj.arg_is_hex[i-2];
					if (ImGui::Checkbox(fmt::format("Hex##{}", i).c_str(), &arg_is_hex_tmp))
						event_obj.arg_is_hex[i-2] = arg_is_hex_tmp;
					if (i != (event_obj.size_of_args_array - 1) && ((i-1) % 3 != 0))
						ImGui::SameLine();
				}

				ImGui::SetNextItemWidth(200.f);
				if (ImGui::BeginCombo("Target", g_player_info.player_name.c_str()))
				{
					for (auto pair : players_tab::playerlist)
					{
						if (ImGui::Selectable(pair.second.name.c_str(), g_player_info.script_event_tester_target_player == pair.second.id))
						{
							g_player_info.player_name = pair.second.name;
							g_player_info.script_event_tester_target_player = pair.second.id;
						}
					}
					ImGui::EndCombo();
				}
				ImGui::SameLine();
				if (ImGui::Button("Send Event"))
				{
					event_obj.args[1] = g_local_player.player;
					QUEUE_JOB_BEGIN_CLAUSE()
					{
						g_pointers->m_trigger_script_event(1, &event_obj.args[0], event_obj.size_of_args_array, (1 << g_player_info.script_event_tester_target_player));
					} QUEUE_JOB_END_CLAUSE
				}

				auto events = list_events();
				static std::string selected_event;
				ImGui::PushItemWidth(250);
				ImGui::Text("Saved Events");
				if (ImGui::ListBoxHeader("##selectedevent", ImVec2(200, 200)))
				{
					for (auto pair : events)
					{
						if (ImGui::Selectable(pair.c_str(), selected_event == pair))
							selected_event = pair;
					}
					ImGui::ListBoxFooter();
				}
				ImGui::SameLine();
				ImGui::BeginGroup();
				static char event_name[50]{};
				ImGui::InputText("##EventName", event_name, IM_ARRAYSIZE(event_name));
				if (ImGui::IsItemActive())
					g_gui.m_using_keyboard = true;
				if (ImGui::Button("Save Event"))
				{
					save_event(event_name, event_obj);
				}
				ImGui::SameLine();
				if (ImGui::Button("Load Event"))
				{
					load_event_menu(selected_event, event_obj);
				}
				ImGui::SameLine();
				if (ImGui::Button("Delete Event"))
				{
					if (!selected_event.empty())
					{
						delete_event(selected_event);
						selected_event.clear();
					}
				}
				ImGui::SameLine();
				if (ImGui::Button("Clear"))
				{
					event_obj.size_of_args_array = 2;
					event_obj.args.clear();
					event_obj.arg_is_hex.clear();
				}
				ImGui::PopItemWidth();
				ImGui::EndGroup();
			}

			if (ImGui::CollapsingHeader("Script Monitor"))
			{
				static GtaThread* selected_thread{};
				if (ImGui::ListBoxHeader("##scripts", ImVec2(250, 500)))
				{
					if (g_settings.options["script monitor sorted"])
					{
						std::map<std::string, GtaThread*> sorted_threads;
						for (auto thread : *g_pointers->m_script_threads)
							if (thread && thread->m_context.m_thread_id && thread->m_handler)
								sorted_threads[thread->m_name] = thread;
						for (auto thread : sorted_threads)
							if (ImGui::Selectable(thread.second->m_name, thread.second == selected_thread))
								selected_thread = thread.second;
					}
					else
					{
						for (auto thread : *g_pointers->m_script_threads)
							if (thread && thread->m_context.m_thread_id && thread->m_handler)
								if (ImGui::Selectable(thread->m_name, thread == selected_thread))
									selected_thread = thread;
					}
					ImGui::ListBoxFooter();
				}
				ImGui::SameLine();
				ImGui::BeginGroup();
				if (ImGui::Checkbox("Sorted?", g_settings.options["script monitor sorted"].get<bool*>()))
					g_settings.save();
				ImGui::TextColored(ImVec4(0.5f, 0.5f, 0.5f, 1.0f), "Script Info:");
				if (selected_thread)
				{
					if(auto net_component = selected_thread->m_net_component)
						if(auto owner_list = net_component->m_owner_list)
							if(auto owner = owner_list->m_owner)
								ImGui::Text(fmt::format("Host: {}", owner->get_name()).c_str());
					if (ImGui::Button(fmt::format("Script Pointer: 0x{:X}", (DWORD64)selected_thread).c_str()))
						set_clipboard(fmt::format("0x{:X}", (DWORD64)selected_thread).c_str());
					if(ImGui::Button(fmt::format("m_stack: 0x{:X}", (DWORD64)selected_thread->m_stack).c_str()))
						set_clipboard(fmt::format("0x{:X}", (DWORD64)selected_thread->m_stack).c_str());
					ImGui::Text(fmt::format("m_exit_message: {}", (selected_thread->m_exit_message) ? selected_thread->m_exit_message : "").c_str());
					if (ImGui::Button(fmt::format("m_handler: 0x{:X}", (DWORD64)selected_thread->m_handler).c_str()))
						set_clipboard(fmt::format("0x{:X}", (DWORD64)selected_thread->m_handler).c_str());
					if (selected_thread->m_net_component != nullptr && ImGui::Button(fmt::format("m_net_component: 0x{:X}", (DWORD64)selected_thread->m_net_component).c_str()))
						set_clipboard(fmt::format("0x{:X}", (DWORD64)selected_thread->m_net_component).c_str());
					ImGui::Text(fmt::format("m_thread_id: {}", selected_thread->m_context.m_thread_id).c_str());
					ImGui::Text(fmt::format("m_instance_id: {}", selected_thread->m_instance_id).c_str());
					ImGui::Text(fmt::format("m_flag1: {:X}", selected_thread->m_flag1).c_str());
					ImGui::Text(fmt::format("m_safe_for_network_game: {}", selected_thread->m_safe_for_network_game).c_str());
					ImGui::Text(fmt::format("m_is_minigame_script: {}", selected_thread->m_is_minigame_script).c_str());
					ImGui::Text(fmt::format("m_can_be_paused: {}", selected_thread->m_can_be_paused).c_str());
					ImGui::Text(fmt::format("m_can_remove_blips_from_other_scripts: {}", selected_thread->m_can_remove_blips_from_other_scripts).c_str());
					if (selected_thread->m_net_component != nullptr)
					{
						if (ImGui::Button("Take Control"))
						{
							QUEUE_JOB_BEGIN_CLAUSE()
							{
								network_helper::take_control_of_script(selected_thread);
							} QUEUE_JOB_END_CLAUSE
						}
						ImGui::SameLine();
					}
					if (ImGui::Button("Kill Script"))
					{
						QUEUE_JOB_BEGIN_CLAUSE()
						{
							SCRIPT::TERMINATE_THREAD(selected_thread->m_context.m_thread_id);
						} QUEUE_JOB_END_CLAUSE
					}
					if (selected_thread->m_net_component != nullptr)
					{
						ImGui::SameLine();
						bool maintain_control = g_local_player.maintain_control.find(selected_thread->m_name) != g_local_player.maintain_control.end();
						if (ImGui::Checkbox("Maintain Host Control", &maintain_control))
						{
							if (maintain_control)
								g_local_player.maintain_control[selected_thread->m_name] = selected_thread;
							else
								g_local_player.maintain_control.erase(selected_thread->m_name);
						}
					}
				}
				ImGui::EndGroup();
			}

			if (*g_pointers->m_script_globals != nullptr)
			{
				if (ImGui::CollapsingHeader("Global Tester"))
				{
					static global_test_json::global_test_json global_test{};
					static script_global glo_bal_sunday = script_global(global_test.global_index);
					ImGui::SetNextItemWidth(200.f);
					if (ImGui::InputScalar("Global", ImGuiDataType_U64, &global_test.global_index))
						glo_bal_sunday = script_global(global_test.global_index);

					for (int i = 0; i < global_test.global_appendages.size(); i++)
					{
						auto item = global_test.global_appendages[i];
						switch (item.type)
						{
							case GlobalAppendageType_At:
								ImGui::SetNextItemWidth(200.f);
								ImGui::InputScalar(fmt::format("At##{}{}", i, item.type).c_str(), ImGuiDataType_S64, &global_test.global_appendages[i].index);
								ImGui::SameLine();
								ImGui::SetNextItemWidth(200.f);
								ImGui::InputScalar(fmt::format("Size##{}{}", i, item.type).c_str(), ImGuiDataType_S64, &global_test.global_appendages[i].size);
								break;
							case GlobalAppendageType_ReadGlobal:
								ImGui::Text(fmt::format("Read Global {}", item.global_name).c_str());
								ImGui::SameLine();
								ImGui::SetNextItemWidth(200.f);
								ImGui::InputScalar(fmt::format("Size##{}{}", i, item.type).c_str(), ImGuiDataType_S64, &global_test.global_appendages[i].size);
								break;
							case GlobalAppendageType_PlayerId:
								ImGui::SetNextItemWidth(200.f);
								ImGui::InputScalar(fmt::format("Read Player ID Size##{}{}", i, item.type).c_str(), ImGuiDataType_S64, &global_test.global_appendages[i].size);
								break;
						}
					}

					if (ImGui::Button("Add Offset"))
						global_test.global_appendages.push_back({ GlobalAppendageType_At, 0LL, 0ULL });
					ImGui::SameLine();
					if (ImGui::Button("Add Read Player Id"))
						global_test.global_appendages.push_back({ GlobalAppendageType_PlayerId, 0LL, 0ULL });

					if (global_test.global_appendages.size() > 0 && ImGui::Button("Remove Offset"))
						global_test.global_appendages.pop_back();
					
					if (auto ptr = get_global_ptr(global_test))
					{
						ImGui::SetNextItemWidth(200.f);
						ImGui::InputScalar("Value", ImGuiDataType_S64, ptr);
					}
					else
						ImGui::Text("INVALID_GLOBAL_READ");

					auto globals = list_globals();
					static std::string selected_global;
					ImGui::Text("Saved Globals");
					if (ImGui::ListBoxHeader("##savedglobals", ImVec2(200, 200)))
					{
						for (auto pair : globals)
						{
							if (ImGui::Selectable(pair.first.c_str(), selected_global == pair.first))
								selected_global = std::string(pair.first);
						}
						ImGui::ListBoxFooter();
					}
					ImGui::SameLine();
					if (ImGui::ListBoxHeader("##globalvalues", ImVec2(200, 200)))
					{
						for (auto pair : globals)
						{
							if(auto ptr = get_global_ptr(pair.second))
								ImGui::Selectable(fmt::format("{}", *ptr).c_str(), false, ImGuiSelectableFlags_Disabled);
							else
								ImGui::Selectable("INVALID_GLOBAL_READ", false, ImGuiSelectableFlags_Disabled);
						}
						ImGui::ListBoxFooter();
					}
					ImGui::SameLine();
					ImGui::BeginGroup();
					static char global_name[50]{};
					ImGui::SetNextItemWidth(200.f);
					ImGui::InputText("##GlobalName", global_name, IM_ARRAYSIZE(global_name));
					if (ImGui::IsItemActive())
						g_gui.m_using_keyboard = true;
					if (ImGui::Button("Save Global"))
					{
						save_global(global_name, global_test);
					}
					ImGui::SameLine();
					if (ImGui::Button("Load Global"))
					{
						load_global_menu(selected_global, global_test);
					}

					if (ImGui::Button("Delete Global"))
					{
						if (!selected_global.empty())
						{
							delete_global(selected_global);
							selected_global.clear();
						}
					}
					ImGui::SameLine();
					if (ImGui::Button("Add Read Global"))
					{
						global_test.global_appendages.push_back({ GlobalAppendageType_ReadGlobal, 0LL, 0ULL, selected_global });
					}
					ImGui::SameLine();
					if (ImGui::Button("Clear"))
					{
						global_test.global_index = 0;
						global_test.global_appendages.clear();
					}
					ImGui::EndGroup();
				}
			}

			ImGui::Separator();
			if (ImGui::Checkbox("Unlock all Bunker research", g_settings.options["unlock bunker research"].get<bool*>()))
			{
				static script_global bunker_research = script_global(Globals::Tuneables).at(Globals::BunkerResearch);
				if (*bunker_research.as<bool*>() == true)
					*bunker_research.as<bool*>() = false;
				g_settings.save();
			}
			ImGui::SameLine();
			if (ImGui::Checkbox("Anti-Afk", g_settings.options["anti-afk"].get<bool*>()))
			{
				static script_global tunables_global = script_global(Globals::Tuneables);
				*tunables_global.at(Tuneables_AFK).as<int*>() = 120000;
				*tunables_global.at(Tuneables_AFK+1).as<int*>() = 300000;
				*tunables_global.at(Tuneables_AFK+2).as<int*>() = 600000;
				*tunables_global.at(Tuneables_AFK+3).as<int*>() = 900000;
				g_settings.save();
			}
			if (ImGui::Checkbox("Always Off the Radar", g_settings.options["always off the radar"].get<bool*>()))
				g_settings.save();
			ImGui::SameLine();
			if (ImGui::Checkbox("Remove OTR for others", g_settings.options["remove otr from other players"].get<bool*>()))
				g_settings.save();
			ImGui::Separator();
			if (ImGui::Checkbox("Semi-Godmode For Everyone During Missions", g_settings.options["semi-godmode for everyone"].get<bool*>()))
				g_settings.save();

			ImGui::Separator();

			if (ImGui::Checkbox("Name ESP", g_settings.options["name esp"].get<bool*>()))
				g_settings.save();
			ImGui::Separator();

			if (ImGui::Button("Dump Player Pointers"))
			{
				LOG(INFO) << "Player's CPed: [" << HEX_TO_UPPER(gta_util::get_local_ped()) << "]";
				LOG(INFO) << "Player's CPlayerInfo: [" << HEX_TO_UPPER(g_local_player.player_info) << "]";
				LOG(INFO) << "Player's CVehicle: [" << HEX_TO_UPPER(g_local_player.vehicle_ptr) << "]";
				LOG(INFO) << "Player's CNetGamePlayer: [" << HEX_TO_UPPER(g_local_player.net_game_player) << "]";
				if(g_local_player.net_game_player)
					LOG(INFO) << "Player's netPlayerData: [" << HEX_TO_UPPER(g_local_player.net_game_player->get_net_data()) << "]";
				LOG(INFO) << "Player's g_pointers->m_get_network_name(): [" << HEX_TO_UPPER(g_pointers->m_get_network_name()) << "]";
			}

			ImGui::Text("Chat Commands:");

			if (ImGui::Checkbox("Toggle", g_settings.options["chat command toggle"].get<bool*>()))
				g_settings.save();
			if (g_settings.options["chat command toggle"])
			{
				if (ImGui::Checkbox("Spawn", g_settings.options["spawn chat command"].get<bool*>()))
					g_settings.save();
				ImGui::SameLine();
				if (ImGui::Checkbox("Weapons", g_settings.options["weapons chat command"].get<bool*>()))
					g_settings.save();
				ImGui::SameLine();
				if (ImGui::Checkbox("Explode", g_settings.options["explode chat command"].get<bool*>()))
					g_settings.save();
				ImGui::SameLine();
				if (ImGui::Checkbox("Money", g_settings.options["money chat command"].get<bool*>()))
					g_settings.save();
				ImGui::SameLine();
				if (ImGui::Checkbox("Semigod", g_settings.options["semigod chat command"].get<bool*>()))
					g_settings.save();
			}

			if (ImGui::Button("Unload"))
			{
				g_local_player.casino_check_controls_enable();
				g_running = false;
				weapons_tab::handle_vehicle_allow_all_weapons();
			}

			ImGui::EndTabItem();
		}
	}

	std::filesystem::path settings_tab::get_events_config()
	{
		auto file_path = std::filesystem::path(std::getenv("appdata"));
		file_path /= "BigBaseV2";

		if (!std::filesystem::exists(file_path))
		{
			std::filesystem::create_directory(file_path);
		}
		else if (!std::filesystem::is_directory(file_path))
		{
			std::filesystem::remove(file_path);
			std::filesystem::create_directory(file_path);
		}

		file_path /= "TestEvents.json";

		return file_path;
	}

	nlohmann::json settings_tab::get_events_json()
	{
		auto file_path = get_events_config();
		nlohmann::json locations;
		std::ifstream file(file_path);

		if (!file.fail())
			file >> locations;

		return locations;
	}

	std::vector<std::string> settings_tab::list_events()
	{
		std::vector<std::string> return_value;
		auto json = get_events_json();
		for (auto& item : json.items())
			return_value.push_back(item.key());
		return return_value;
	}

	void settings_tab::load_event_menu(std::string& selected_event, event_json::event_json &event_obj)
	{
		if (!selected_event.empty())
		{
			auto events = get_events_json();
			if (events[selected_event].is_null())
				return;
			event_obj = events[selected_event].get<event_json::event_json>();
		}
	}

	void settings_tab::save_event(char* event_name, event_json::event_json& event_obj)
	{
		std::string teleport_name_string = event_name;
		if (!teleport_name_string.empty())
		{
			auto json = get_events_json();
			json[event_name] = event_obj;

			auto file_path = get_events_config();
			std::ofstream file(file_path, std::ios::out | std::ios::trunc);
			file << json.dump(4);
			file.close();
			ZeroMemory(event_name, sizeof(event_name));
		}
	}

	void settings_tab::delete_event(std::string name)
	{
		auto locations = get_events_json();
		if (locations[name].is_null())
			return;
		locations.erase(name);
		auto file_path = get_events_config();
		std::ofstream file(file_path, std::ios::out | std::ios::trunc);
		file << locations.dump(4);
		file.close();
	}

	std::filesystem::path settings_tab::get_globals_config()
	{
		auto file_path = std::filesystem::path(std::getenv("appdata"));
		file_path /= "BigBaseV2";

		if (!std::filesystem::exists(file_path))
		{
			std::filesystem::create_directory(file_path);
		}
		else if (!std::filesystem::is_directory(file_path))
		{
			std::filesystem::remove(file_path);
			std::filesystem::create_directory(file_path);
		}

		file_path /= "TestGlobals.json";

		return file_path;
	}

	nlohmann::json settings_tab::get_globals_json()
	{
		auto file_path = get_globals_config();
		nlohmann::json locations;
		std::ifstream file(file_path);

		if (!file.fail())
			file >> locations;

		return locations;
	}

	std::map<std::string, global_test_json::global_test_json> settings_tab::list_globals()
	{
		auto json = get_globals_json();
		std::map<std::string, global_test_json::global_test_json> return_value;
		for (auto& item : json.items())
			return_value[item.key()] = item.value();
		return json;
	}

	void settings_tab::load_global_menu(const std::string& selected_global, global_test_json::global_test_json& global_obj)
	{
		if (!selected_global.empty())
		{
			auto globals = get_globals_json();
			if (globals[selected_global].is_null())
				return;
			global_obj = globals[selected_global].get<global_test_json::global_test_json>();
		}
	}

	void settings_tab::save_global(char* global_name, global_test_json::global_test_json& global_obj)
	{
		std::string teleport_name_string = global_name;
		if (!teleport_name_string.empty())
		{
			auto json = get_globals_json();
			json[global_name] = global_obj;

			auto file_path = get_globals_config();
			std::ofstream file(file_path, std::ios::out | std::ios::trunc);
			file << json.dump(4);
			file.close();
			ZeroMemory(global_name, sizeof(global_name));
		}
	}

	void settings_tab::delete_global(std::string name)
	{
		auto locations = get_globals_json();
		if (locations[name].is_null())
			return;
		locations.erase(name);
		auto file_path = get_globals_config();
		std::ofstream file(file_path, std::ios::out | std::ios::trunc);
		file << locations.dump(4);
		file.close();
	}

	int64_t* big::settings_tab::get_global_ptr(global_test_json::global_test_json& global_test)
	{
		script_global global_to_read = script_global(global_test.global_index);
		for (auto item : global_test.global_appendages)
		{
			if (item.type == GlobalAppendageType_At)
			{
				if (item.size != 0)
					global_to_read = global_to_read.at(item.index, item.size);
				else
					global_to_read = global_to_read.at(item.index);
			}
			else if (item.type == GlobalAppendageType_ReadGlobal)
			{
				global_test_json::global_test_json global_read;
				load_global_menu(item.global_name, global_read);
				if (auto ptr = get_global_ptr(global_read))
					if(item.size != 0)
						global_to_read = global_to_read.at(*ptr, item.size);
					else
						global_to_read = global_to_read.at(*ptr);
				else
					LOG(WARNING) << "Failed to read " << item.global_name << "for get_global_ptr";
			}
			else if (item.type == GlobalAppendageType_PlayerId)
			{
				if (item.size != 0)
					global_to_read = global_to_read.at(g_local_player.player, item.size);
				else
					global_to_read = global_to_read.at(g_local_player.player);
			}
		}
		auto retn_val = global_to_read.as<int64_t*>();
		if ((size_t)retn_val < UINT32_MAX)
			return nullptr;
		return retn_val;
	}

	void settings_tab::set_clipboard(const char* message)
	{
		HGLOBAL h;
		LPVOID p;
		int size;
		//calc the num of unicode char
		size = MultiByteToWideChar(CP_UTF8, NULL, message, -1, NULL, 0);
		if (!size) return;
		h = GlobalAlloc(GHND | GMEM_SHARE, size * 2);
		if (!h) return;
		p = GlobalLock(h);
		//utf8 to unicode
		MultiByteToWideChar(CP_UTF8, NULL, message, -1, (LPWSTR)p, size);
		GlobalUnlock(h);
		OpenClipboard(NULL);
		EmptyClipboard();
		SetClipboardData(CF_UNICODETEXT, h);
		CloseClipboard();
	}
}