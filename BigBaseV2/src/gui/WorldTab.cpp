#include "common.hpp"
#include <imgui.h>
#include "fiber_pool.hpp"
#include "gta\enums.hpp"
#include "pointers.hpp"
#include "script.hpp"
#include "helpers\NetworkHelper.h"
#include "helpers\BlipHelper.h"
#include "persist\PersistTeleport.h"
#include "WorldTab.h"
#include "gui.hpp"
#include "CustomCode/PlayerInfo.h"
#include "helpers/VehicleHelper.h"
#include "CustomCode/Settings.h"

namespace big
{
	void world_tab::render_world_tab()
	{
		if (ImGui::BeginTabItem("World"))
		{
			if (ImGui::Button("Teleport to Waypoint"))
				teleport_to_waypoint();
			ImGui::SameLine();
			if (ImGui::Button("Teleport to Objective"))
				teleport_to_objective();
			ImGui::SameLine();
			if (ImGui::Button("Teleport to Selected Blip"))
				teleport_to_selected_blip();
			ImGui::SameLine();
			if (ImGui::Checkbox("Add Aircraft Height", g_settings.options["add aircraft height"].get<bool*>()))
				g_settings.save();

			persist_teleport::do_presentation_layer();

			if (ImGui::Button("Dump Selected Blip Info"))
			{
				if (auto blip = blip_helper::get_selected_blip())
					LOG(INFO) << "Blip #" << blip->m_blip_array_index << " " << HEX_TO_UPPER(blip) << " m_scale_x:" << blip->m_scale_x << " m_scale_y:" << blip->m_scale_y << " m_rotation:" << blip->m_rotation << " m_icon:" << blip->m_icon << " m_color:" << HEX_TO_UPPER(blip->m_color) << " m_message:" << (blip->m_message == NULL ? "" : blip->m_message);
			}

			//ImGui::Separator();

			//static int weather_index;
			//ImGui::PushItemWidth(250);
			//ImGui::Combo("Weather", &weather_index, weather_combo, IM_ARRAYSIZE(weather_combo));
			//ImGui::SameLine();
			//if (ImGui::Button("Set Weather"))
			//{
			//	MISC::SET_OVERRIDE_WEATHER(weather_combo[weather_index]);
			//	if (*g_pointers->m_is_session_started)
			//		g_pointers->m_set_lobby_weather(true, weather_index, 76, nullptr);
			//}
			//ImGui::PopItemWidth();

			//static int hour, minute = 0;
			//ImGui::SliderInt("Hour", &hour, 0, 23);
			//ImGui::SameLine();
			//ImGui::SliderInt("Minute", &minute, 0, 59);
			//ImGui::SameLine();
			//if (ImGui::Button("Set Time"))
			//{
			//	NETWORK::NETWORK_OVERRIDE_CLOCK_TIME(hour, minute, 0);
			//	if (*g_pointers->m_is_session_started)
			//		g_pointers->m_set_lobby_time(1, 0);
			//}

			ImGui::EndTabItem();
		}
	}

	void world_tab::teleport_to_waypoint()
	{
		QUEUE_JOB_BEGIN_CLAUSE()
		{
			Entity e = g_local_player.player_ped;
			if (g_local_player.is_in_vehicle())
				e = g_local_player.vehicle;
			if (network_helper::request_control(e, REQUEST_CONTROL_TIME))
			{
				bool bBlipFound = false;
				static Vector3 coords, oldLocation;
				if (auto blip = blip_helper::get_waypoint_blip())
				{
					coords.x = blip->m_x;
					coords.y = blip->m_y;
					coords.z = blip->m_z;
					bBlipFound = true;
					oldLocation = ENTITY::GET_ENTITY_COORDS(e, FALSE);
				}
				if (bBlipFound)
				{
					bool groundFound;
					groundFound = false;
					static const float ground_heights[] = { 300.0, 100.0, 150.0, 50.0, 0.0, 200.0, 250.0, 300.0, 350.0, 400.0, 450.0, 500.0, 550.0, 600.0, 650.0, 700.0, 750.0, 800.0 };
					for (float ground_height : ground_heights)
					{
						ENTITY::SET_ENTITY_COORDS_NO_OFFSET(e, coords.x, coords.y, ground_height, FALSE, FALSE, TRUE);
						if (MISC::GET_GROUND_Z_FOR_3D_COORD(coords.x, coords.y, ground_height, &coords.z, 0, 0) == TRUE)
						{
							groundFound = true;
							ENTITY::SET_ENTITY_COORDS_NO_OFFSET(e, coords.x, coords.y, coords.z + 3, FALSE, FALSE, TRUE);
							return;
						}
						script::get_current()->yield(5ms);
					}
					if (!groundFound)
					{
						ENTITY::SET_ENTITY_COORDS_NO_OFFSET(e, oldLocation.x, oldLocation.y, oldLocation.z, FALSE, FALSE, TRUE);
					}
				}
			}
		} QUEUE_JOB_END_CLAUSE
	}

	void world_tab::teleport_to_objective()
	{
		QUEUE_JOB_BEGIN_CLAUSE()
		{
			if (auto blip = blip_helper::get_mission_blip())
			{
				Entity e = g_local_player.player_ped;
				if (g_local_player.is_in_vehicle())
					e = g_local_player.vehicle;
				if (network_helper::request_control(e, REQUEST_CONTROL_TIME))
				{
					float z = blip->m_z;
					if ((VEHICLE::IS_THIS_MODEL_A_HELI(ENTITY::GET_ENTITY_MODEL(e)) || VEHICLE::IS_THIS_MODEL_A_PLANE(ENTITY::GET_ENTITY_MODEL(e))) && g_settings.options["add aircraft height"])
						z += 20;
					ENTITY::SET_ENTITY_COORDS_NO_OFFSET(e, blip->m_x, blip->m_y, z, FALSE, FALSE, TRUE);
				}
			}
		} QUEUE_JOB_END_CLAUSE
	}

	void world_tab::teleport_to_selected_blip()
	{
		if (auto blip = blip_helper::get_selected_blip())
		{
			QUEUE_JOB_BEGIN_CLAUSE(blip)
			{
				Entity e = g_local_player.player_ped;
				if (g_local_player.is_in_vehicle())
					e = g_local_player.vehicle;
				if (network_helper::request_control(e, REQUEST_CONTROL_TIME))
				{
					float z = blip->m_z;
					if ((VEHICLE::IS_THIS_MODEL_A_HELI(ENTITY::GET_ENTITY_MODEL(e)) || VEHICLE::IS_THIS_MODEL_A_PLANE(ENTITY::GET_ENTITY_MODEL(e))) && g_settings.options["add aircraft height"])
						z += 20;
					ENTITY::SET_ENTITY_COORDS_NO_OFFSET(e, blip->m_x, blip->m_y, z, FALSE, FALSE, TRUE);
				}
			} QUEUE_JOB_END_CLAUSE
		}
	}
}
