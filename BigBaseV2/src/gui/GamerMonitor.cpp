#include "common.hpp"
#include "imgui.h"
#include "GamerMonitor.h"
#include "PlayersTab.h"
#include "CustomCode/Settings.h"
#include "helpers/NetworkHelper.h"
#include "hooking.hpp"
#include "script.hpp"
#include "gui.hpp"


namespace big
{
	static bool is_open = false;
	void gamer_monitor::render_gamer_monitor()
	{
		is_open = ImGui::BeginTabItem("Gamer Monitor");
		if (is_open)
		{
			static int scid;
			ImGui::PushItemWidth(150);
			ImGui::InputScalar("SCID", ImGuiDataType_S32, &scid);
			if (ImGui::IsItemActive())
				g_gui.m_using_keyboard = true;
			ImGui::SameLine();
			static char name[0x20]{};
			ImGui::InputText("Name", name, IM_ARRAYSIZE(name));
			if (ImGui::IsItemActive())
				g_gui.m_using_keyboard = true;
			ImGui::SameLine();

			if (ImGui::Button("Add Gamer"))
			{
				std::string lower_name = name;
				transform(lower_name.begin(), lower_name.end(), lower_name.begin(), ::tolower);
				gamerlist_info::gamerlist_info gamer_info;
				gamer_info.id = scid;
				gamer_info.name = name;
				gamer_list[lower_name] = gamer_info;
				g_settings.options["gamer list"] = gamer_list;
				g_settings.save();
			}

			static std::string selected_name{};
			if (ImGui::ListBoxHeader("##gamerlist", ImVec2(200, -1)))
			{
				for (auto pair : gamer_list)
				{
					ImGui::PushStyleColor(ImGuiCol_Text, get_gamer_color(pair.second));

					if (ImGui::Selectable(pair.second.name.c_str(), pair.first == selected_name))
						selected_name = pair.first;

					ImGui::PopStyleColor();
				}
				ImGui::ListBoxFooter();
			}

			if (!selected_name.empty())
			{
				ImGui::SameLine();
				ImGui::BeginGroup();

				auto gamer_info = gamer_list[selected_name];

				ImGui::Text("SCID: %i", gamer_info.id);

				if (ImGui::Button("Remove Gamer"))
				{
					gamer_list.erase(selected_name);
					g_settings.options["gamer list"] = gamer_list;
					g_settings.save();
					selected_name.clear();
				}

				if (gamer_info.status == gamerlist_info::gamerlist_status::session)
				{
					ImGui::SameLine();
					if (ImGui::Button("Join"))
					{
						QUEUE_JOB_BEGIN_CLAUSE(gamer_info)
						{
							network_helper::join_player(gamer_info.id);
						} QUEUE_JOB_END_CLAUSE
					}
				}

				ImGui::EndGroup();
			}
			ImGui::EndTabItem();
		}
	}

	ImVec4 gamer_monitor::get_gamer_color(gamerlist_info::gamerlist_info info)
	{
		switch (info.status)
		{
			case gamerlist_info::gamerlist_status::online:
				return ImColor(0, 160, 0, 255);
			case gamerlist_info::gamerlist_status::session:
				return ImColor(0, 255, 0, 255);
		}
		return ImColor(190, 190, 190, 255);
	}

	void gamer_monitor::script_func()
	{
		gamer_list = g_settings.options["gamer list"].get<std::map<std::string, gamerlist_info::gamerlist_info>>();
		while (true)
		{
			if (is_open && g_gui.m_opened)
			{
				std::vector<std::unordered_map<uint64_t, std::string>> to_run;

				int index = 0;
				int group_index = -1;
				for (auto entry : gamer_list)
				{
					if (index % 32 == 0)
					{
						group_index++;
						to_run.push_back(std::unordered_map<uint64_t, std::string>());
					}

					to_run[group_index].emplace(entry.second.id, entry.first);

					index++;
				}

				for (int i = 0; i < to_run.size(); i++)
				{
					network_helper::request_session_status(to_run[i]);
					while (!g_pending_gamer_group.empty())
					{
						script::get_current()->yield(500ms);
					}
				}

				script::get_current()->yield(5s);
			}
			else
			{
				script::get_current()->yield();
			}	
		}
	}
}