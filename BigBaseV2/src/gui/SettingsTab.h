#pragma once

#include <array>

namespace big
{
	enum GlobalAppendageType
	{
		GlobalAppendageType_At,
		GlobalAppendageType_ReadGlobal,
		GlobalAppendageType_PlayerId,
	};

	namespace global_test_json_inner
	{
		struct global_test_json_inner
		{
			GlobalAppendageType type{};
			std::ptrdiff_t index{};
			std::size_t size{};
			std::string global_name{};
		};
		static void to_json(nlohmann::json& j, const global_test_json_inner& inner) {
			j = nlohmann::json{ {"type", inner.type}, {"index", inner.index}, {"size", inner.size}, {"global_name", inner.global_name} };
		}

		static void from_json(const nlohmann::json& j, global_test_json_inner& inner) {
			j.at("type").get_to(inner.type); j.at("index").get_to(inner.index); j.at("size").get_to(inner.size); j.at("global_name").get_to(inner.global_name);
		}
	}

	namespace global_test_json
	{
		struct global_test_json
		{
			std::size_t global_index{};
			std::vector<global_test_json_inner::global_test_json_inner> global_appendages{};
		};
		static void to_json(nlohmann::json& j, const global_test_json& global) {
			j = nlohmann::json{ {"global_index", global.global_index}, {"global_appendages", global.global_appendages} };
		}

		static void from_json(const nlohmann::json& j, global_test_json& global) {
			j.at("global_index").get_to(global.global_index); j.at("global_appendages").get_to(global.global_appendages);
		}
	}

	namespace event_json
	{
		struct event_json
		{
			int size_of_args_array = 2;
			std::vector<int64_t> args{};
			std::vector<bool> arg_is_hex{};
		};
		static void to_json(nlohmann::json& j, const event_json& attachment) {
			j = nlohmann::json{ {"size_of_args_array", attachment.size_of_args_array},
								{"args", attachment.args}, {"arg_is_hex", attachment.arg_is_hex} };
		}

		static void from_json(const nlohmann::json& j, event_json& attachment) {
			j.at("size_of_args_array").get_to(attachment.size_of_args_array);
			j.at("args").get_to(attachment.args); j.at("arg_is_hex").get_to(attachment.arg_is_hex);
		}
	};

	class settings_tab
	{
	public:
		static void render_settings_tab();
	private:
		static std::filesystem::path get_events_config();
		static nlohmann::json get_events_json();
		static std::vector<std::string> list_events();
		static void load_event_menu(std::string& selected_event, event_json::event_json& event_obj);
		static void save_event(char* event_name, event_json::event_json& event_obj);
		static void delete_event(std::string name);
		static std::filesystem::path get_globals_config();
		static nlohmann::json get_globals_json();
		static std::map<std::string, global_test_json::global_test_json> list_globals();
		static void load_global_menu(const std::string&, global_test_json::global_test_json&);
		static void save_global(char*, global_test_json::global_test_json&);
		static void delete_global(std::string);
		static int64_t* get_global_ptr(global_test_json::global_test_json&);
		static void set_clipboard(const char* message);
	};
}