#pragma once
#include "natives.hpp"
#include "gta\player.hpp"

namespace big
{
	class rage_helper
	{
	public:
		static void blame_explosion_on_ped(Ped victim, Ped target, float x = 0.0f, float y = 0.0f, float z = 0.0f);
		static const char* get_player_headshot(Player playerParam);
		static std::string format_money(std::uint32_t money);
		static bool validate_player_id(int64_t player);
		static void stop_ped_vehicle(Ped ped);
		static void change_name(std::string& new_name, rage::netPlayerData* = NULL);
	};
}