#include "common.hpp"
#include "DrawHelper.h"
#include "fiber_pool.hpp"
#include "helpers/RageHelper.h"
#include "script.hpp"

namespace big
{
	void draw_helper::draw_notification(const std::string& message, const std::string& picture, const std::string& sender, const std::string& subject, HudColor color)
	{
		HUD::BEGIN_TEXT_COMMAND_THEFEED_POST("STRING");
		HUD::ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(message.c_str());
		if (color != HUD_COLOUR_BLACK)
			HUD::_THEFEED_SET_NEXT_POST_BACKGROUND_COLOR(color);
		if (!(picture.empty() && sender.empty()))
			HUD::END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT_TU(picture.c_str(), picture.c_str(), 1, 0, sender.c_str(), subject.c_str(), 1);
		HUD::END_TEXT_COMMAND_THEFEED_POST_TICKER(false, false);
	}

	void draw_helper::draw_headshot_notification(const std::string& message, const std::string& name, const Player player, const std::string& subject, HudColor color)
	{
		QUEUE_JOB_BEGIN_CLAUSE(message, color, name, player, subject)
		{
			HUD::BEGIN_TEXT_COMMAND_THEFEED_POST("STRING");
			HUD::ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(message.c_str());
			if (color != HUD_COLOUR_BLACK)
				HUD::_THEFEED_SET_NEXT_POST_BACKGROUND_COLOR(color);
			auto headshot = rage_helper::get_player_headshot(player);
			HUD::END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT_TU(headshot, headshot, 1, 0, name.c_str(), subject.c_str(), 1);
			HUD::END_TEXT_COMMAND_THEFEED_POST_TICKER(false, false);
		} QUEUE_JOB_END_CLAUSE
	}
}