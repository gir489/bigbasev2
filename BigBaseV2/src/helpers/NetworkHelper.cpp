#include "common.hpp"
#include "helpers\NetworkHelper.h"
#include "script.hpp"
#include "hooking.hpp"
#include "CustomCode/PlayerInfo.h"
#include "fiber_pool.hpp"
#include "CustomCode/PlayerAttachments.h"
#include "gta_util.hpp"
#include "persist/PersistModder.h"

namespace big
{
	BOOL network_helper::is_player_friend(Player player)
	{
		int handle[26];
		NETWORK::NETWORK_HANDLE_FROM_PLAYER(player, &handle[0], 13);
		return NETWORK::NETWORK_IS_HANDLE_VALID(&handle[0], 13) && NETWORK::NETWORK_IS_FRIEND(&handle[0]);
	}

	int network_helper::get_network_handle(Player player)
	{
		int handle[26];
		NETWORK::NETWORK_HANDLE_FROM_PLAYER(player, &handle[0], 13);
		if (NETWORK::NETWORK_IS_HANDLE_VALID(&handle[0], 13))
			return handle[0];
		return NULL;
	}

	Player network_helper::get_player_from_scid(uint64_t scid)
	{
		std::string memberHandle = std::to_string(scid);
		int handle[26];
		NETWORK::NETWORK_HANDLE_FROM_MEMBER_ID((char*)memberHandle.c_str(), &handle[0], 13);
		if (NETWORK::NETWORK_IS_HANDLE_VALID(&handle[0], 13))
		{
			Player player = NETWORK::NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(&handle[0]);
			if (player)
				return player;
		}
		return -1;
	}

	const char* network_helper::get_name_from_scid(uint64_t scid)
	{
		Player messenger = get_player_from_scid(scid);
		if (messenger == -1)
			return "UNKNOWN_NAME";
		return NETWORK::NETWORK_PLAYER_GET_NAME(messenger);
	}

	bool network_helper::is_player_network_host(Player player)
	{
		CNetGamePlayer* net_player = g_pointers->m_get_net_player(player);
		if (net_player == nullptr)
			return false;

		return net_player->is_host();
	}

	bool network_helper::request_control(Entity entity)
	{
		if (*g_pointers->m_is_session_started == false)
			return true;
		*(PDWORD)g_pointers->m_spectate_control_bypass = 0x90909090;
		NETWORK::NETWORK_REQUEST_CONTROL_OF_ENTITY(entity);
		*(PDWORD)g_pointers->m_spectate_control_bypass = 0x6A75C084;
		if (!NETWORK::NETWORK_HAS_CONTROL_OF_ENTITY(entity))
			script::get_current()->yield();
		return NETWORK::NETWORK_HAS_CONTROL_OF_ENTITY(entity);
	}

	bool network_helper::request_control(Entity entity, std::chrono::high_resolution_clock::duration time, std::optional<std::chrono::high_resolution_clock::duration> iteration_epoch)
	{
		static std::chrono::steady_clock::time_point begin_call_time;
		begin_call_time = std::chrono::high_resolution_clock::now();
		while (!request_control(entity) && ((std::chrono::high_resolution_clock::now() - begin_call_time) < time))
			script::get_current()->yield(iteration_epoch); //std::nullopt for yield();
		return NETWORK::NETWORK_HAS_CONTROL_OF_ENTITY(entity);
	}

	void network_helper::destory_prop(Object object, rage::CObject* pCObject)
	{
		QUEUE_JOB_BEGIN_CLAUSE(object, pCObject)
		{
			if(request_control(object, REQUEST_CONTROL_TIME))
				g_pointers->m_delete_object(pCObject, false);
		} QUEUE_JOB_END_CLAUSE
	}

	void network_helper::destory_ped(Ped ped, rage::CPed* pCPed)
	{
		QUEUE_JOB_BEGIN_CLAUSE(ped, pCPed)
		{
			if (request_control(ped, REQUEST_CONTROL_TIME))
				g_pointers->m_delete_ped(pCPed);
		} QUEUE_JOB_END_CLAUSE
	}

	void network_helper::remove_attachments(Ped player_ped)
	{
		rage::CObjectInterface* object_interface = g_pointers->m_replay_interface->m_object_interface;
		int numObj = object_interface->m_max_objects;
		Object weapon = WEAPON::GET_CURRENT_PED_WEAPON_ENTITY_INDEX(player_ped);
		for (int i = 0; i < numObj; i++)
		{
			rage::CObject* pCObject = object_interface->get_object(i);
			if (pCObject == nullptr)
				continue;
		
			Object object = g_pointers->m_ptr_to_handle(pCObject);
			if (object == 0)
				break;
		
			if (!ENTITY::IS_ENTITY_ATTACHED_TO_ENTITY(player_ped, object) || weapon == object)
				continue;
		
			destory_prop(object, pCObject);
		};
		rage::CPedInterface* ped_interface = g_pointers->m_replay_interface->m_ped_interface;
		int max_peds = ped_interface->m_max_peds;
		for (int i = 0; i < max_peds; i++)
		{
			rage::CPed* pCPed = ped_interface->get_ped(i);
			if (pCPed == nullptr)
				continue;

			Ped ped = g_pointers->m_ptr_to_handle(pCPed);
			if (ped == 0)
				break;

			if (!ENTITY::IS_ENTITY_ATTACHED_TO_ENTITY(player_ped, ped))
				continue;

			destory_ped(ped, pCPed);
		};
	}

	void network_helper::spawn_attached_object(player_attachment::player_attachment attachment, Player player)
	{
		Ped player_ped = PLAYER::GET_PLAYER_PED_SCRIPT_INDEX(player);
		Vector3 player_coords = ENTITY::GET_ENTITY_COORDS(player_ped, false);

		while (!STREAMING::HAS_MODEL_LOADED(attachment.model_hash))
		{
			STREAMING::REQUEST_MODEL(attachment.model_hash);
			script::get_current()->yield();
		}
		*(unsigned short*)g_pointers->m_model_spawn_bypass = 0x9090;
		Object object = OBJECT::CREATE_OBJECT(attachment.model_hash, player_coords.x, player_coords.y, player_coords.z, TRUE, FALSE, FALSE);
		*(unsigned short*)g_pointers->m_model_spawn_bypass = 0x0574;

		ENTITY::ATTACH_ENTITY_TO_ENTITY(object, player_ped, PED::GET_PED_BONE_INDEX(player_ped, attachment.bone), attachment.position.x, attachment.position.y, attachment.position.z, attachment.rotation.x, attachment.rotation.y, attachment.rotation.z, false, false, false, true, 0, true);
		ENTITY::_SET_ENTITY_SOMETHING(object, TRUE);
	}

	void network_helper::clear_entities_from_player(Player player)
	{
		QUEUE_JOB_BEGIN_CLAUSE(player)
		{
			auto player_ped = PLAYER::GET_PLAYER_PED_SCRIPT_INDEX(player);
			rage::CObjectInterface* object_interface = g_pointers->m_replay_interface->m_object_interface;
			for (int i = 0; i < object_interface->m_max_objects; i++)
			{
				rage::CObject* obj = object_interface->get_object(i);
				if (obj && obj->m_net_object && obj->m_net_object->m_owner_id == player)
				{
					auto handle = g_pointers->m_ptr_to_handle(obj);
					if(ENTITY::IS_ENTITY_ATTACHED_TO_ENTITY(handle, player_ped))
						network_helper::destory_prop(handle, obj);
				}
			}
			rage::CPedInterface* ped_interface = g_pointers->m_replay_interface->m_ped_interface;
			for (int i = 0; i < ped_interface->m_max_peds; i++)
			{
				rage::CPed* ped = ped_interface->get_ped(i);
				if (ped && ped->m_net_object && ped->m_net_object->m_owner_id == player)
				{
					auto ped_handle = g_pointers->m_ptr_to_handle(ped);
					if (ENTITY::IS_ENTITY_ATTACHED_TO_ENTITY(ped_handle, player_ped))
						network_helper::destory_ped(g_pointers->m_ptr_to_handle(ped), ped);
				}
			}
		} QUEUE_JOB_END_CLAUSE
	}

	void network_helper::kick_player(Player player, std::string reason)
	{
		if (!NETWORK::NETWORK_IS_PLAYER_CONNECTED(player))
			return;

		if (!reason.empty())
			draw_helper::draw_headshot_notification(reason, PLAYER::GET_PLAYER_NAME(player), player, "Kick", HUD_COLOUR_ORANGE);

		if (NETWORK::NETWORK_IS_HOST())
		{
			NETWORK::NETWORK_SESSION_KICK_PLAYER(player);
		}
		else
		{
			network_helper::take_control_of_script(gta_util::find_script_thread(RAGE_JOAAT("freemode")));
			gta_util::execute_as_script(RAGE_JOAAT("freemode"), [&]
			{
				static script_global info_global = script_global(Globals::PlayerInformation);
				auto player_company = *info_global.at(player, PlayerInformation_Size).at(11).as<int*>();
				if (player_company != -1)
					network_helper::ceo_kick(player);

				script_global player_needs_kick = script_global(Globals::PlayerNeedsKick);
				*player_needs_kick.at(player + 1).as<int*>() = 1;
				NETWORK::NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(player_needs_kick.as<int*>(), 33);
			});

			kick(1 << player);
		}
	}

	void network_helper::kick(uint32_t bitset)
	{
		int64_t args[3] = { Events::Kick_BikerMissionWallSounds, g_local_player.player, 36 };
		g_pointers->m_trigger_script_event(1, &args[0], 3, bitset);
		int64_t arena_args[4] = { Events::Kick_ArenaBanner, g_local_player.player, 11, 26 };
		g_pointers->m_trigger_script_event(1, &arena_args[0], 4, bitset);
		int64_t arena1_args[5] = { Events::Kick_ArenaBanner1, g_local_player.player, 11, 26, 0 };
		g_pointers->m_trigger_script_event(1, &arena1_args[0], 5, bitset);
	}

	void network_helper::ceo_kick(Player player)
	{
		take_control_of_script(gta_util::find_script_thread(RAGE_JOAAT("freemode")));
		static script_global info_global = script_global(Globals::PlayerInformation);
		*info_global.at(player, PlayerInformation_Size).at(11).as<int*>() = -1;
	}

	void network_helper::take_control_of_script(GtaThread* thread)
	{
		if (thread == nullptr || thread->m_net_component == nullptr || 
			thread->m_net_component->m_owner_list == nullptr || 
			thread->m_net_component->m_owner_list->m_owner == (rage::CNetGamePlayer*)g_local_player.net_game_player)
			return;
		LOG(EVENT) << "Taking control of " << thread->m_name;
		auto orig = *(BYTE*)g_pointers->m_force_host_patch;
		*(BYTE*)g_pointers->m_force_host_patch = 1;

		g_pointers->m_set_new_script_host(thread->m_net_component, g_local_player.net_game_player);

		try {
			if (g_pointers && g_pointers->m_force_host_patch)
				*(BYTE*)g_pointers->m_force_host_patch = orig;
		} catch (...) {}
	}

	class menu_class
	{
	public:
		menu_class() = default;
		char pad_0000[120]; //0x0000
		bool do_fail; //0x0076
		char pad_0079[21]; //0x0077
		uint32_t selected_friend; //0x008C
		char pad_0090[40]; //0x0090
		uint32_t N00000046; //0x00B8
		char pad_00BC[28]; //0x00BC
		uint8_t N00000086; //0x00D8
		char pad_00D9[3]; //0x00D9
		uint32_t N00000066; //0x00DC
		class N00000095* N00000085; //0x00E0
		char pad_00E8[80]; //0x00E8
		uint32_t N0000006A; //0x0138
	};

	void network_helper::join_player(uint32_t scid)
	{
		//If the pause menu is open, close it
		if (HUD::GET_CURRENT_FRONTEND_MENU_VERSION() != 0xFFFFFFFF)
		{
			HUD::ACTIVATE_FRONTEND_MENU(0xD528C7E2, false, 2);
			script::get_current()->yield(200ms);
		}

		//open the pause menu
		HUD::ACTIVATE_FRONTEND_MENU(0xD528C7E2, false, 2);
		script::get_current()->yield(200ms);

		//setup fake pointer
		menu_class* ptr = new menu_class();

		int64_t info = g_pointers->m_get_friends_list(0);
		PBYTE data = (PBYTE)(info + 8);

		if (data)
		{
			int index = 0;
			while ((uint8_t)*data <= 3u)
			{
				if (*data == 3)
					break;

				++index;
				data += 0x10;
			}

			if (index < 20)
			{
				int64_t old = *(int64_t*)(info + 16i64 * index);
				*(int64_t*)(info + 16i64 * index) = scid;

				g_pointers->m_join_player(ptr, false);
				script::get_current()->yield(400ms);

				*(int64_t*)(info + 16i64 * index) = old;
			}
		}
	}

	void network_helper::crash_player(Player player)
	{
		static const Hash model = RAGE_JOAAT("slod_human");

		Vector3 local_coords = ENTITY::GET_ENTITY_COORDS(g_local_player.player_ped, false);
		Vector3 player_coords = ENTITY::GET_ENTITY_COORDS(PLAYER::GET_PLAYER_PED_SCRIPT_INDEX(player), false);
		bool needs_teleport = false;

		if (NETWORK::NETWORK_IS_IN_SPECTATOR_MODE())
			NETWORK::NETWORK_SET_IN_SPECTATOR_MODE(false, g_local_player.player_ped);

		if (MISC::GET_DISTANCE_BETWEEN_COORDS(local_coords.x, local_coords.y, local_coords.z, player_coords.x, player_coords.y, player_coords.z, false) < 300.f)
		{
			ENTITY::SET_ENTITY_COORDS_NO_OFFSET(g_local_player.player_ped, local_coords.x + 1000, local_coords.y + 1000, local_coords.z + 1000, TRUE, TRUE, TRUE);
			needs_teleport = true;
		}

		while (!STREAMING::HAS_MODEL_LOADED(model))
		{
			STREAMING::REQUEST_MODEL(model);
			script::get_current()->yield();
		}

		*(unsigned short*)g_pointers->m_model_spawn_bypass = 0x9090;
		Ped crash_ped = PED::CREATE_PED(5, model, player_coords.x, player_coords.y, player_coords.z, 0, true, false);
		*(unsigned short*)g_pointers->m_model_spawn_bypass = 0x0574;

		ENTITY::ATTACH_ENTITY_TO_ENTITY(crash_ped, PLAYER::GET_PLAYER_PED_SCRIPT_INDEX(player), 0, 0, 0, 0, 0, 0, 0, false, false, false, true, 0, true);

		script::get_current()->yield(300ms);

		ENTITY::SET_ENTITY_AS_MISSION_ENTITY(crash_ped, false, true);
		ENTITY::DELETE_ENTITY(&crash_ped);
		if (needs_teleport)
			ENTITY::SET_ENTITY_COORDS_NO_OFFSET(g_local_player.player_ped, local_coords.x, local_coords.y, local_coords.z, TRUE, TRUE, TRUE);
	}

	void network_helper::request_session_status(std::unordered_map<uint64_t, std::string> ids)
	{
		g_pending_gamer_group = ids;

		NETWORK::NETWORK_CLEAR_GET_GAMER_STATUS();

		std::vector<RockstarIdentifier> identifiers;
		for (auto entry : ids)
		{
			RockstarIdentifier identifier = RockstarIdentifier();
			identifier.m_rockstar_id = entry.first;
			identifier.m_type = 3;

			identifiers.push_back(identifier);
		}

		if (!g_pointers->m_send_session_info_request(identifiers.data(), (uint32_t)identifiers.size()))
			LOG(WARNING) << "Failed to call send_session_info_request!";
	}

	void network_helper::validate_scid(rage::netPlayerData* player_data)
	{
		if ((player_data->m_rockstar_id == 0 || player_data->m_rockstar_id != player_data->m_rockstar_id_2) && player_data->m_online_port != 0)
		{
			LOG(EVENT) << "Detected invalid SCID from " << player_data->m_name << " SCID1: " << player_data->m_rockstar_id << " SCID2: " << player_data->m_rockstar_id_2;
			if (player_data->m_rockstar_id == 0 && player_data->m_rockstar_id_2 == 0)
				player_data->m_rockstar_id = player_data->m_rockstar_id_2 = 10101;
			else
				player_data->m_rockstar_id = player_data->m_rockstar_id_2;
			g_persist_modder.add_modder(player_data->m_rockstar_id, -1, player_data->m_name, "Invalid SCID");
		}
	}
}