#include "common.hpp"
#include "RageHelper.h"
#include "script.hpp"
#include "script_global.hpp"
#include "pointers.hpp"
#include "gta\joaat.hpp"
#include "helpers\VehicleHelper.h"
#include "gta_util.hpp"
#include "CustomCode/Settings.h"
#include "CustomCode/PlayerInfo.h"
#include "memory/all.hpp"

namespace big
{
	void rage_helper::blame_explosion_on_ped(Ped victim, Ped target, float x, float y, float z)
	{
		if ((x + y + z) == 0.f)
		{
			Vector3 pos = ENTITY::GET_ENTITY_COORDS(target, FALSE);
			x = pos.x;
			y = pos.y;
			z = pos.z;
		}
		*(PWORD)g_pointers->m_add_owned_explosion_bypass = 0xE990;
		FIRE::ADD_OWNED_EXPLOSION(victim, x, y, z, EXPLOSION_STICKYBOMB, 10.f, FALSE, TRUE, 0.f);
		*(PWORD)g_pointers->m_add_owned_explosion_bypass = 0x850F;
	}

	const char* rage_helper::get_player_headshot(Player playerParam)
	{
		script_global headshot_global = script_global(Globals::ScoreBoardHeadshot);
		script_global current_headshots = script_global(Globals::ScoreboardHeadshotNum);
		int max_headshots = *current_headshots.as<int*>();
		std::string headshot = "CHAR_MULTIPLAYER";
		for (int i = 0; i < max_headshots; i++)
		{
			auto player_headshot_data = headshot_global.at(i, 5);
			auto player_headshot_id = *player_headshot_data.at(1).as<Player*>();
			if (player_headshot_id == playerParam)
			{
				auto headshot_num = *player_headshot_data.at(2).as<int*>();
				if (PED::IS_PEDHEADSHOT_VALID(headshot_num) && PED::IS_PEDHEADSHOT_READY(headshot_num))
					headshot = PED::GET_PEDHEADSHOT_TXD_STRING(headshot_num);
				break;
			}
		}
		return headshot.c_str();
	}

	std::string rage_helper::format_money(std::uint32_t money)
	{
		std::stringstream ss;
		ss.imbue(std::locale(""));
		ss << std::fixed << money;
		return ss.str();
	}

	bool rage_helper::validate_player_id(int64_t player)
	{
		return !(player > -1 && player < MAX_PLAYERS);
	}

	void rage_helper::stop_ped_vehicle(Ped ped)
	{
		if (!PED::IS_PED_IN_ANY_VEHICLE(ped, false))
			return;

		vehicle_helper::stop_vehicle(PED::GET_VEHICLE_PED_IS_USING(ped));
	}

	void rage_helper::change_name(std::string& new_name, rage::netPlayerData* net_data)
	{
		if (new_name.empty() || new_name.length() >= 20)
			return;

		auto cstr_name = new_name.c_str();
		auto new_length = new_name.length() + 1; //We need the \0 character.

		if (auto player_info = g_local_player.player_info)
		{
			if (void* network_name_ptr = g_pointers->m_get_network_name())
			{
				strncpy_s((char*)network_name_ptr + 0x84, new_length, &cstr_name[0], new_length);
				strncpy_s(player_info->m_name, &cstr_name[0], new_length);
			}
		}
		if (net_data)
		{
			strncpy_s(net_data->m_name, &cstr_name[0], new_length);
		}
	}
}