#include "common.hpp"
#include "BlipHelper.h"
#include "pointers.hpp"

namespace big
{
	rage::Blip_t* blip_helper::get_selected_blip()
	{
		for (int i = 0; i < 1500; i++)
		{
			rage::Blip_t* blip = g_pointers->m_blip_list->m_Blips[i].m_pBlip;
			if (blip)
			{
				if ((blip->m_display_bits & BlipDisplayBits::BlipIsSelected))
					return blip;
			}
		}
		return nullptr;
	}

	rage::Blip_t* blip_helper::get_waypoint_blip()
	{
		for (int i = 0; i < 1500; i++)
		{
			rage::Blip_t* blip = g_pointers->m_blip_list->m_Blips[i].m_pBlip;
			if (blip)
			{
				if (blip->m_icon == BlipIcons::Waypoint && blip->m_color == BlipColors::WaypointColor)
					return blip;
			}
		}
		return nullptr;
	}

	rage::Blip_t* blip_helper::get_mission_blip()
	{
		for (rage::BlipEntry blip : g_pointers->m_blip_list->m_Blips) //Actual mission blips.
		{
			if (blip.m_pBlip)
			{
				if ((blip.m_pBlip->m_color == BlipColors::Mission && blip.m_pBlip->m_icon == BlipIcons::Circle) ||
					(blip.m_pBlip->m_color == BlipColors::YellowMission && blip.m_pBlip->m_icon == BlipIcons::Circle) ||
					(blip.m_pBlip->m_color == BlipColors::YellowMission2 && (blip.m_pBlip->m_icon == BlipIcons::Circle || blip.m_pBlip->m_icon == BlipIcons::DollarSignCircled)) ||
					(/*blip.m_pBlip->m_color == BlipColors::None && */blip.m_pBlip->m_icon == BlipIcons::RaceFinish) ||
					(blip.m_pBlip->m_color == BlipColors::Green && blip.m_pBlip->m_icon == BlipIcons::Circle))
				{
					return blip.m_pBlip;
				}
			}
		}
		for (rage::BlipEntry blip : g_pointers->m_blip_list->m_Blips) //Finance and Felony blips.
		{
			if (blip.m_pBlip)
			{
				if ((blip.m_pBlip->m_color == BlipColors::Blue && blip.m_pBlip->m_icon == BlipIcons::Supercar) ||
					(blip.m_pBlip->m_color == BlipColors::Green && blip.m_pBlip->m_icon == BlipIcons::Key) ||
					(blip.m_pBlip->m_color == BlipColors::Green && blip.m_pBlip->m_icon == BlipIcons::Crate))
				{
					return blip.m_pBlip;
				}
			}
		}
		return nullptr;
	}

	rage::Blip_t* blip_helper::get_local_player()
	{
		return g_pointers->m_blip_list->m_Blips[1].m_pBlip;
	}
}