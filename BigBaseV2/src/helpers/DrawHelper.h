#pragma once
#include "natives.hpp"
#include "gta\enums.hpp"

namespace big
{
	class draw_helper
	{
	public:
		static void draw_notification(const std::string& message, const std::string& picture = "", const std::string& sender = "", const std::string& subject = "", HudColor color = HUD_COLOUR_BLACK);
		static void draw_headshot_notification(const std::string& message, const std::string& name, const Player player, const std::string& subject = "", HudColor color = HUD_COLOUR_BLACK);
	};
}