#pragma once

#include "common.hpp"
#include "natives.hpp"
#include "gta\VehicleValues.h"
#include "pointers.hpp"
#include "NetworkHelper.h"
#include "CustomCode/PlayerInfo.h"

namespace big
{
	class vehicle_helper
	{
	public:
		static void add_clan_logo_to_vehicle(Vehicle vehicle, Ped ped);
		static void set_mp_parameters_for_vehicle(Vehicle vehicle);
		static Vehicle create_vehicle(Hash modelHash, float x, float y, float z, float heading);
		static void clear_dirt(Vehicle vehicle);
		static void set_max_health(Vehicle vehicle);
		static void fix_vehicle(Vehicle vehicle);
		static void set_vehicle_strong(Vehicle vehicle);
		static void flip_car(Vehicle vehicle);
		static void set_max_stats(Vehicle vehicle);
		static void stop_vehicle(Vehicle vehicle);
		static void set_vehicle_godmode(Vehicle vehicle, bool status = true);
		static bool vehicle_has_godmode(Vehicle vehicle);
		static rage::CVehicle* get_vehicle(Vehicle vehicle);
		static void boost_spawned_vehicle(Vehicle vehicle);
	};
}