#include "common.hpp"
#include "VehicleHelper.h"
#include "script.hpp"
#include "gta_util.hpp"
#include "CustomCode/PlayerInfo.h"
#include "gta\net_object_mgr.hpp"
#include "CustomCode/Settings.h"

namespace big
{
	void vehicle_helper::add_clan_logo_to_vehicle(Vehicle vehicle, Ped ped)
	{
		rage::vector3 x, y, z;
		float scale;
		Hash modelHash = ENTITY::GET_ENTITY_MODEL(vehicle);
		if (GetVehicleInfoForClanLogo(modelHash, x, y, z, scale))
		{
			int alpha = 200;
			if (modelHash == VEHICLE_WINDSOR || modelHash == VEHICLE_COMET4)
				alpha = 255;

			GRAPHICS::ADD_VEHICLE_CREW_EMBLEM(vehicle, ped, ENTITY::GET_ENTITY_BONE_INDEX_BY_NAME(vehicle, "chassis_dummy"), x.x, x.y, x.z, y.x, y.y, y.z, z.x, z.y, z.z, scale, 0, alpha);
			if (y.z >= 0.0f)
				GRAPHICS::ADD_VEHICLE_CREW_EMBLEM(vehicle, ped, ENTITY::GET_ENTITY_BONE_INDEX_BY_NAME(vehicle, "chassis_dummy"), x.x * -1.0f, x.y, x.z, y.x * -1.0f, y.y, y.z, z.x * -1.0f, z.y * -1.0f, z.z, scale, 1, alpha);
		}
	}

	void vehicle_helper::set_mp_parameters_for_vehicle(Vehicle vehicle)
	{
		if (network_helper::request_control(vehicle))
		{
			DECORATOR::DECOR_SET_INT(vehicle, "MPBitset", 0);
			DECORATOR::DECOR_SET_INT(vehicle, "RandomID", g_local_player.player_hash);
			auto networkId = NETWORK::VEH_TO_NET(vehicle);
			if (NETWORK::NETWORK_GET_ENTITY_IS_NETWORKED(vehicle))
				NETWORK::SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(networkId, true);
			VEHICLE::SET_VEHICLE_IS_STOLEN(vehicle, FALSE);
		}
	}

	Vehicle vehicle_helper::create_vehicle(Hash modelHash, float x, float y, float z, float heading)
	{
		while (!STREAMING::HAS_MODEL_LOADED(modelHash))
		{
			STREAMING::REQUEST_MODEL(modelHash);
			script::get_current()->yield();
		}
		*(unsigned short*)big::g_pointers->m_model_spawn_bypass = 0x9090;
		Vehicle vehicle = VEHICLE::CREATE_VEHICLE(modelHash, x, y, z, heading, TRUE, FALSE, FALSE);
		*(unsigned short*)big::g_pointers->m_model_spawn_bypass = 0x0574;
		script::get_current()->yield(); //This allows the car to initalize so when we write things like radio station, it will overwrite.
		ENTITY::_SET_ENTITY_SOMETHING(vehicle, TRUE); 
		STREAMING::SET_MODEL_AS_NO_LONGER_NEEDED(modelHash);
		if (*big::g_pointers->m_is_session_started)
			set_mp_parameters_for_vehicle(vehicle);
		return vehicle;
	}

	void vehicle_helper::clear_dirt(Vehicle vehicle)
	{
		if (VEHICLE::GET_VEHICLE_DIRT_LEVEL(vehicle) > 0.f)
		{
			if (network_helper::request_control(g_local_player.vehicle, REQUEST_CONTROL_TIME))
			{
				VEHICLE::SET_VEHICLE_DIRT_LEVEL(vehicle, 0.0f);
				if (VEHICLE::IS_VEHICLE_ATTACHED_TO_TRAILER(vehicle))
				{
					Vehicle trailer{};
					VEHICLE::GET_VEHICLE_TRAILER_VEHICLE(vehicle, &trailer);
					if (VEHICLE::GET_VEHICLE_DIRT_LEVEL(trailer) > 0.f && network_helper::request_control(trailer, REQUEST_CONTROL_TIME))
						VEHICLE::SET_VEHICLE_DIRT_LEVEL(trailer, 0.0f);
				}
			}
		}
	}

	void vehicle_helper::set_max_health(Vehicle vehicle)
	{
		ENTITY::SET_ENTITY_HEALTH(vehicle, ENTITY::GET_ENTITY_MAX_HEALTH(vehicle), 0);
		VEHICLE::SET_VEHICLE_ENGINE_HEALTH(vehicle, 1000.f);
		VEHICLE::SET_VEHICLE_PETROL_TANK_HEALTH(vehicle, 1000.f);
		VEHICLE::SET_VEHICLE_BODY_HEALTH(vehicle, 1000.f);
		if (VEHICLE::IS_VEHICLE_ATTACHED_TO_TRAILER(vehicle))
		{
			Vehicle trailer;
			VEHICLE::GET_VEHICLE_TRAILER_VEHICLE(vehicle, &trailer);
			ENTITY::SET_ENTITY_HEALTH(trailer, ENTITY::GET_ENTITY_MAX_HEALTH(trailer), 0);
			VEHICLE::SET_VEHICLE_ENGINE_HEALTH(trailer, 1000.f);
			VEHICLE::SET_VEHICLE_PETROL_TANK_HEALTH(trailer, 1000.f);
			VEHICLE::SET_VEHICLE_BODY_HEALTH(trailer, 1000.f);
		}
	}

	void vehicle_helper::fix_vehicle(Vehicle vehicle)
	{
		if (network_helper::request_control(vehicle, REQUEST_CONTROL_TIME))
		{
			VEHICLE::SET_VEHICLE_FIXED(vehicle);
			set_max_health(vehicle);
		}
	}

	void vehicle_helper::set_vehicle_strong(Vehicle vehicle)
	{
		if (network_helper::request_control(vehicle, REQUEST_CONTROL_TIME))
		{
			VEHICLE::SET_VEHICLE_STRONG(vehicle, TRUE);
			VEHICLE::SET_VEHICLE_HAS_STRONG_AXLES(vehicle, TRUE);
		}
	}

	void vehicle_helper::flip_car(Vehicle vehicle)
	{
		if (ENTITY::IS_ENTITY_UPSIDEDOWN(vehicle) && network_helper::request_control(vehicle, REQUEST_CONTROL_TIME))
		{
			Vector3 rotation = ENTITY::GET_ENTITY_ROTATION(vehicle, 2);
			ENTITY::SET_ENTITY_ROTATION(vehicle, rotation.x, 0, rotation.z, 2, TRUE);
		}
	}

	void vehicle_helper::set_max_stats(Vehicle vehicle)
	{
		Hash vehicleModel = ENTITY::GET_ENTITY_MODEL(vehicle);
		if (VEHICLE::IS_THIS_MODEL_A_CAR(vehicleModel) || VEHICLE::IS_THIS_MODEL_A_BIKE(vehicleModel) && network_helper::request_control(vehicle, REQUEST_CONTROL_TIME))
		{
			VEHICLE::SET_VEHICLE_TYRES_CAN_BURST(vehicle, FALSE); //Bulletproof Tires.
			g_local_player.vehicle_info.can_tires_burst = true;
			VEHICLE::SET_VEHICLE_MOD_KIT(vehicle, 0); //IDK what this does, but I guess it allows individual mods to be added? It's what the game does before calling SET_VEHICLE_MOD.
			VEHICLE::SET_VEHICLE_HAS_STRONG_AXLES(vehicle, TRUE); //6stronk9meme
			int maxengine = VEHICLE::GET_NUM_VEHICLE_MODS(vehicle, MOD_ENGINE) - 1, maxbreaks = VEHICLE::GET_NUM_VEHICLE_MODS(vehicle, MOD_BRAKES) - 1, maxtransmission = VEHICLE::GET_NUM_VEHICLE_MODS(vehicle, MOD_TRANSMISSION) - 1,
				maxsuspension = VEHICLE::GET_NUM_VEHICLE_MODS(vehicle, MOD_SUSPENSION) - 1, maxarmor = VEHICLE::GET_NUM_VEHICLE_MODS(vehicle, MOD_ARMOR) - 1;
			VEHICLE::SET_VEHICLE_MOD(vehicle, MOD_ENGINE, maxengine, FALSE); //6fast9furious
			VEHICLE::SET_VEHICLE_MOD(vehicle, MOD_BRAKES, maxbreaks, FALSE); //GOTTA STOP FAST
			VEHICLE::SET_VEHICLE_MOD(vehicle, MOD_TRANSMISSION, maxtransmission, FALSE); //Not when I shift in to MAXIMUM OVERMEME!
			VEHICLE::SET_VEHICLE_MOD(vehicle, MOD_SUSPENSION, maxsuspension, FALSE); //How low can you go?
			VEHICLE::SET_VEHICLE_MOD(vehicle, MOD_ARMOR, maxarmor, FALSE); //100% armor.
			g_local_player.vehicle_info.owned_mods[MOD_ENGINE] = maxengine;
			g_local_player.vehicle_info.owned_mods[MOD_BRAKES] = maxbreaks;
			g_local_player.vehicle_info.owned_mods[MOD_TRANSMISSION] = maxtransmission;
			g_local_player.vehicle_info.owned_mods[MOD_SUSPENSION] = maxsuspension;
			g_local_player.vehicle_info.owned_mods[MOD_ARMOR] = maxarmor;
			VEHICLE::TOGGLE_VEHICLE_MOD(vehicle, MOD_TURBO, TRUE); //Forced induction huehuehue
			g_local_player.vehicle_info.turbo = true;
		}
	}

	void vehicle_helper::stop_vehicle(Vehicle vehicle)
	{
		if (network_helper::request_control(vehicle, REQUEST_CONTROL_TIME))
		{
			VEHICLE::SET_VEHICLE_FORWARD_SPEED(vehicle, 0);
		}
	}

	void vehicle_helper::set_vehicle_godmode(Vehicle vehicle, bool status)
	{
		if (network_helper::request_control(vehicle, REQUEST_CONTROL_TIME))
		{
			if (rage::CVehicle* vehicle_pointer = get_vehicle(vehicle))
				status ? vehicle_pointer->enable_invincible() : vehicle_pointer->disable_invincible();
		}
	}

	bool vehicle_helper::vehicle_has_godmode(Vehicle vehicle)
	{
		if (rage::CVehicle* vehicle_pointer = get_vehicle(vehicle))
			return vehicle_pointer->is_invincible();
		return false;
	}

	rage::CVehicle* vehicle_helper::get_vehicle(Vehicle vehicle)
	{
		return g_pointers->m_get_vehicle_ptr(vehicle);
	}

	void vehicle_helper::boost_spawned_vehicle(Vehicle vehicle)
	{
		if (g_settings.options["spawn in vehicle"])
			PED::SET_PED_INTO_VEHICLE(g_local_player.player_ped, vehicle, -1);
		if (g_settings.options["spawn vehicle boosted"])
			vehicle_helper::set_max_stats(vehicle);
		if (g_settings.options["spawn vehicle strong"])
			vehicle_helper::set_vehicle_strong(vehicle);
		if (g_settings.options["spawn vehicle godmode"])
			vehicle_helper::set_vehicle_godmode(vehicle);
	}
}