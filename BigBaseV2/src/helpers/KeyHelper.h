#pragma once

#include "common.hpp"

#define IsKeyPressedHelper(key) GetAsyncKeyState(key) & 0x8000

static bool is_key_pressed_once(bool& bIsPressed, DWORD vk)
{
	if (IsKeyPressedHelper(vk))
	{
		if (bIsPressed == false)
		{
			bIsPressed = true;
			return true;
		}
	}
	else if (bIsPressed == true)
	{
		bIsPressed = false;
	}
	return false;
}