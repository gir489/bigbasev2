#pragma once

#include "natives.hpp"
#include "gta\blip.hpp"

namespace big
{
	class blip_helper
	{
	public:
		static rage::Blip_t* get_selected_blip();
		static rage::Blip_t* get_waypoint_blip();
		static rage::Blip_t* get_mission_blip();
		static rage::Blip_t* get_local_player();
	};
}