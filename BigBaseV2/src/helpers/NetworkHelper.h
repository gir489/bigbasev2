#pragma once
#include "natives.hpp"
#include "pointers.hpp"
#include "script_global.hpp"
#include "RageHelper.h"
#include "CustomCode/PlayerAttachments.h"

namespace big
{
	class network_helper
	{
	public:
		static BOOL is_player_friend(Player player);
		static int get_network_handle(Player player);
		static Player get_player_from_scid(uint64_t scid);
		static const char* get_name_from_scid(uint64_t scid);
		static bool is_player_network_host(Player player);
		static bool request_control(Entity entity);
		static bool request_control(Entity entity, std::chrono::high_resolution_clock::duration time, std::optional<std::chrono::high_resolution_clock::duration> iteration_epoch = std::nullopt);
		static void destory_prop(Object object, rage::CObject* pCObject);
		static void destory_ped(Ped ped, rage::CPed* pCPed);
		static void remove_attachments(Ped player_ped);
		static void spawn_attached_object(player_attachment::player_attachment attachment, Player player);
		static void clear_entities_from_player(Player player);
		static void take_control_of_script(GtaThread* thread);

		static void kick_player(Player player, std::string reason = "");
		static void kick(uint32_t bitset);

		static void ceo_kick(Player player);

		static void join_player(uint32_t scid);

		static void crash_player(Player player);

		static void request_session_status(std::unordered_map<uint64_t, std::string> ids);

		static void validate_scid(rage::netPlayerData*);

		template <typename T>
		static T get_player_stat(Player player, PlayerStatType type)
		{
			script_global stats_global = script_global(Globals::PlayerStats);
			return *stats_global.at(player, Globals::PlayerStats_Size).at(PlayerStats_Offset).at(type).as<T*>();
		}

		#define REQUEST_CONTROL_TIME 1s
	};
}