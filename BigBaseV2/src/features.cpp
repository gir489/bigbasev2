#include "common.hpp"
#include "features.hpp"
#include "logger.hpp"
#include "natives.hpp"
#include "script.hpp"
#include "script_global.hpp"
#include "gta_util.hpp"
#include "imgui.h"
#include "gta\Weapons.h"
#include "helpers\KeyHelper.h"
#include "persist\PersistPed.h"
#include "gui.hpp"
#include "helpers\NetworkHelper.h"
#include "helpers\VehicleHelper.h"
#include "helpers\BlipHelper.h"
#include "hooks\NetEventHook.h"
#include "CustomCode\Settings.h"
#include "gui\PlayersTab.h"
#include "CustomCode/PlayerInfo.h"
#include "script_local.hpp"
#include "gui/WeaponsTab.h"
#include "persist\PersistWeapons.h"
#include "gui/OnlineTab.h"
#include "CustomCode/WeaponInfo.h"

namespace big
{
	void features::run_tick()
	{
		g_local_player.is_loading_screen_active = DLC::GET_IS_LOADING_SCREEN_ACTIVE();
		g_local_player.is_player_switch_in_progress = STREAMING::IS_PLAYER_SWITCH_IN_PROGRESS();
		g_local_player.is_cutscene_playing = CUTSCENE::IS_CUTSCENE_PLAYING();
		g_local_player.network_is_activity_session = NETWORK::NETWORK_IS_ACTIVITY_SESSION();

		static script_global focus_global = script_global(Globals::FocusOnPed);
		static script_global balistic_armor_check_global = script_global(Globals::BalisticArmorInFreemodeCheck);
		static script_global personal_vehicle_global = script_global(Globals::PersonalVehicle).at(Globals::PersonalVehicle_Offset);
		static script_global lobby_player_information = script_global(Globals::LobbyPlayerInformation);
		static script_global race_car_global = script_global(Globals::RaceCarInSP);
		static script_global lobby_player_information_global = lobby_player_information;
		if (*focus_global.as<int*>() != 1)
			*focus_global.as<int*>() = 1;
		if (*balistic_armor_check_global.as<int*>() != 0)
			*balistic_armor_check_global.as<int*>() = 0;
		g_local_player.personal_vehicle = *personal_vehicle_global.as<int*>();

		if (g_local_player.player != PLAYER::PLAYER_ID() || g_local_player.player_ped != PLAYER::PLAYER_PED_ID())
		{
			g_local_player.player = g_player_info.script_event_tester_target_player = PLAYER::PLAYER_ID();
			g_local_player.player_hash = NETWORK::NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER::PLAYER_ID());
			g_local_player.player_ped = PLAYER::PLAYER_PED_ID();
			g_local_player.player_info = gta_util::get_local_playerinfo();
			g_player_info.player_name = g_local_player.player_info->m_name;
			lobby_player_information_global = lobby_player_information.at(g_local_player.player, Globals::LobbyPlayerInformationSize);
			g_local_player.ped_ptr = gta_util::get_local_ped();
			g_local_player.net_player_id = (g_local_player.ped_ptr->m_net_object != nullptr) ? g_local_player.ped_ptr->m_net_object->m_object_id : -1;
			g_local_player.net_game_player = g_pointers->m_get_net_player(g_local_player.player);
			STATS::STAT_GET_INT(RAGE_JOAAT("MPPLY_LAST_MP_CHAR"), &g_local_player.character_index, -1);
			g_local_player.is_mp0_char = g_local_player.character_index == 0;
			weapons_tab::handle_vehicle_allow_all_weapons();
			if (!*g_pointers->m_is_session_started)
				*race_car_global.as<bool*>() = true;
			else
				*race_car_global.as<bool*>() = false;
		}
		
		if (g_local_player.is_loading_screen_active || g_local_player.is_player_switch_in_progress || g_local_player.is_cutscene_playing)
			return;
		
		if (ENTITY::DOES_ENTITY_EXIST(g_local_player.player_ped) && !ENTITY::IS_ENTITY_DEAD(g_local_player.player_ped, 0))
		{
			int persist_flags = PERSIST_PED_NONE;
			if (!(g_settings.options["persist outfit only in freemode"] && g_local_player.network_is_activity_session))
			{
				if (g_settings.options["persist outfit all"])
				{
					persist_flags = PERSIST_PED_ALL;
				}
				else
				{
					if (g_settings.options["persist outfit clothes"])
						persist_flags |= PERSIST_PED_OUTFIT;
					if (g_settings.options["persist outfit props"])
						persist_flags |= PERSIST_PED_PED_DECORATION;
					if (g_settings.options["persist outfit model"])
						persist_flags |= PERSIST_PED_MODEL;
					if (g_settings.options["persist outfit face"])
						persist_flags |= PERSIST_PED_FACE;
					if (g_settings.options["persist outfit head"])
						persist_flags |= PERSIST_PED_HEAD;
					if (g_settings.options["persist outfit tattoos"])
						persist_flags |= PERSIST_PED_TATTOOS;
				}
			}
			std::string ped_outfit_file_name = g_settings.options["selected outfit file"];
			if (!ped_outfit_file_name.empty() && persist_flags != PERSIST_PED_NONE)
				persist_ped::ped_outfit_tick(ped_outfit_file_name, persist_flags);

			g_local_player.is_playing_casino_games = (*lobby_player_information_global.at(LobbyPlayerCasino).at(4).as<DWORD*>() & (Blackjack | Poker | Slots | Roulette));
			if (g_local_player.is_playing_casino_games)
			{
				if (auto blackjack_thread = gta_util::find_script_thread(RAGE_JOAAT("blackjack")))
				{
					if (blackjack_thread->m_stack)
					{
						int blackjack_table = *script_local(blackjack_thread, Blackjack_TablePlayers).at(g_local_player.player, 8).at(4).as<int*>();
						if (blackjack_table != -1)
							g_local_player.dealer_blackjack_card = script_local(blackjack_thread, Blackjack_Cards).at(Blackjack_Decks).at(blackjack_table, 13).at(1).as<int*>();
					}
				}
			}
			else if (g_local_player.dealer_blackjack_card != nullptr)
			{
				g_local_player.dealer_blackjack_card = nullptr;
			}

			if (g_settings.options["always win roulette"] && g_local_player.is_playing_casino_games)
			{
				if (auto roulette_thread = gta_util::find_script_thread(RAGE_JOAAT("casinoroulette")))
				{
					auto roulette_table_outcomes_head = script_local(roulette_thread, Roulette_MasterTable).at(Roulette_OutcomesTable).at(Roulette_BallTable);
					for (int i = 0; i < 6; i++)
						*roulette_table_outcomes_head.at(i).as<int*>() = 18;
				}
			}

			WEAPON::GET_CURRENT_PED_WEAPON(g_local_player.player_ped, &g_local_player.weapon, FALSE);
			WEAPON::GET_CURRENT_PED_VEHICLE_WEAPON(g_local_player.player_ped, &g_local_player.vehicle_weapon);
			g_local_player.current_weapon_tint_count = WEAPON::GET_WEAPON_TINT_COUNT(g_local_player.weapon);

			if (PED::IS_PED_IN_ANY_VEHICLE(g_local_player.player_ped, FALSE))
			{
				if (g_local_player.vehicle != PED::GET_VEHICLE_PED_IS_IN(g_local_player.player_ped, FALSE))
				{
					g_local_player.vehicle = PED::GET_VEHICLE_PED_IS_IN(g_local_player.player_ped, FALSE);
					g_local_player.vehicle_ptr = vehicle_helper::get_vehicle(g_local_player.vehicle);
					g_local_player.vehicle_model = g_local_player.vehicle_ptr->m_model_info->m_model;
					g_local_player.is_vehicle_race_car = (g_local_player.vehicle_model == VEHICLE_FORMULA || g_local_player.vehicle_model == VEHICLE_FORMULA2 || g_local_player.vehicle_model == VEHICLE_OPENWHEEL1 || g_local_player.vehicle_model == VEHICLE_OPENWHEEL2);
					g_local_player.net_vehicle_id = (g_local_player.vehicle_ptr->m_net_object != nullptr) ? g_local_player.vehicle_ptr->m_net_object->m_object_id : -1;
					g_local_player.is_in_personal_vehicle = g_local_player.vehicle == g_local_player.personal_vehicle || DECORATOR::DECOR_GET_INT(g_local_player.vehicle, "RandomID") == g_local_player.player_hash;
					weapons_tab::handle_player_get_in_vehicle_weapons_callback();
				}
			}
			else if (g_local_player.is_in_vehicle())
			{
				g_local_player.vehicle = -1;
				g_local_player.vehicle_model = NULL;
				g_local_player.vehicle_ptr = nullptr;
				g_local_player.net_vehicle_id = -1;
			}

			if(g_local_player.last_vehicle != PED::GET_VEHICLE_PED_IS_IN(g_local_player.player_ped, TRUE))
				g_local_player.last_vehicle = PED::GET_VEHICLE_PED_IS_IN(g_local_player.player_ped, TRUE);
		
			if (g_settings.options["unlock bunker research"])
			{
				static script_global bunker_research = script_global(Globals::Tuneables).at(Globals::BunkerResearch);
				if (*bunker_research.as<bool*>() == false)
					*bunker_research.as<bool*>() = true;
			}
		
			if (g_settings.options["disable ragdoll"] == true && PED::CAN_PED_RAGDOLL(g_local_player.player_ped))
			{
				PED::SET_PED_CAN_RAGDOLL(g_local_player.player_ped, FALSE);
				PED::SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(g_local_player.player_ped, FALSE);
			}
		
			if (g_settings.options["semi-godmode"])
			{
				int maxHealth = PED::GET_PED_MAX_HEALTH(g_local_player.player_ped), health = ENTITY::GET_ENTITY_HEALTH(g_local_player.player_ped);
				if (health < maxHealth && health > 1)
					ENTITY::SET_ENTITY_HEALTH(g_local_player.player_ped, maxHealth, 0);
				int armorDelta = PLAYER::GET_PLAYER_MAX_ARMOUR(g_local_player.player) - PED::GET_PED_ARMOUR(g_local_player.player_ped);
				if (armorDelta > 0)
					PED::ADD_ARMOUR_TO_PED(g_local_player.player_ped, armorDelta);
			}
		
			if (g_settings.options["always car godmode"] && g_local_player.personal_vehicle != -1)
			{
				if (auto vehicle = vehicle_helper::get_vehicle(g_local_player.personal_vehicle))
				{
					if (!vehicle->is_invincible())
						vehicle->enable_invincible();
				}
			}
		
			if (g_settings.options["infinite ammo"])
			{
				int max_ammo;
				if (WEAPON::IS_WEAPON_VALID(g_local_player.weapon) && WEAPON::GET_MAX_AMMO(g_local_player.player_ped, g_local_player.weapon, &max_ammo))
					WEAPON::SET_PED_AMMO(g_local_player.player_ped, g_local_player.weapon, max_ammo, 0);
			}

			if (g_settings.options["infinite snacks"])
			{
				int psandqs{}, egochaser{}, meteorite{}, ecola{}, pisswasser{}, cigarettes{}, champagne{};
				STATS::STAT_GET_INT(g_local_player.is_mp0_char ? RAGE_JOAAT("MP0_NO_BOUGHT_YUM_SNACKS") : RAGE_JOAAT("MP1_NO_BOUGHT_YUM_SNACKS"), &psandqs, TRUE);
				if(psandqs != 30)
					STATS::STAT_SET_INT(g_local_player.is_mp0_char ? RAGE_JOAAT("MP0_NO_BOUGHT_YUM_SNACKS") : RAGE_JOAAT("MP1_NO_BOUGHT_YUM_SNACKS"), 30, TRUE); //P's & Q's
				STATS::STAT_GET_INT(g_local_player.is_mp0_char ? RAGE_JOAAT("MP0_NO_BOUGHT_HEALTH_SNACKS") : RAGE_JOAAT("MP1_NO_BOUGHT_HEALTH_SNACKS"), &egochaser, TRUE);
				if(egochaser != 15)
					STATS::STAT_SET_INT(g_local_player.is_mp0_char ? RAGE_JOAAT("MP0_NO_BOUGHT_HEALTH_SNACKS") : RAGE_JOAAT("MP1_NO_BOUGHT_HEALTH_SNACKS"), 15, TRUE); //EgoChaser
				STATS::STAT_GET_INT(g_local_player.is_mp0_char ? RAGE_JOAAT("MP0_NO_BOUGHT_EPIC_SNACKS") : RAGE_JOAAT("MP1_NO_BOUGHT_EPIC_SNACKS"), &meteorite, TRUE);
				if(meteorite != 5)
					STATS::STAT_SET_INT(g_local_player.is_mp0_char ? RAGE_JOAAT("MP0_NO_BOUGHT_EPIC_SNACKS") : RAGE_JOAAT("MP1_NO_BOUGHT_EPIC_SNACKS"), 5, TRUE); //Meteorite
				STATS::STAT_GET_INT(g_local_player.is_mp0_char ? RAGE_JOAAT("MP0_NUMBER_OF_ORANGE_BOUGHT") : RAGE_JOAAT("MP1_NUMBER_OF_ORANGE_BOUGHT"), &ecola, TRUE);
				if(ecola != 10)
					STATS::STAT_SET_INT(g_local_player.is_mp0_char ? RAGE_JOAAT("MP0_NUMBER_OF_ORANGE_BOUGHT") : RAGE_JOAAT("MP1_NUMBER_OF_ORANGE_BOUGHT"), 10, TRUE); //eCola
				STATS::STAT_GET_INT(g_local_player.is_mp0_char ? RAGE_JOAAT("MP0_NUMBER_OF_BOURGE_BOUGHT") : RAGE_JOAAT("MP1_NUMBER_OF_BOURGE_BOUGHT"), &pisswasser, TRUE);
				if(pisswasser != 10)
					STATS::STAT_SET_INT(g_local_player.is_mp0_char ? RAGE_JOAAT("MP0_NUMBER_OF_BOURGE_BOUGHT") : RAGE_JOAAT("MP1_NUMBER_OF_BOURGE_BOUGHT"), 10, TRUE); //Pi�wasser #memewasser
				STATS::STAT_GET_INT(g_local_player.is_mp0_char ? RAGE_JOAAT("MP0_CIGARETTES_BOUGHT") : RAGE_JOAAT("MP1_CIGARETTES_BOUGHT"), &cigarettes, TRUE);
				if(cigarettes != 20)
					STATS::STAT_SET_INT(g_local_player.is_mp0_char ? RAGE_JOAAT("MP0_CIGARETTES_BOUGHT") : RAGE_JOAAT("MP1_CIGARETTES_BOUGHT"), 20, TRUE); //Cigarettes
				STATS::STAT_GET_INT(g_local_player.is_mp0_char ? RAGE_JOAAT("MP0_NUMBER_OF_CHAMP_BOUGHT") : RAGE_JOAAT("MP1_NUMBER_OF_CHAMP_BOUGHT"), &champagne, TRUE);
				if(champagne != 5)
					STATS::STAT_SET_INT(g_local_player.is_mp0_char ? RAGE_JOAAT("MP0_NUMBER_OF_CHAMP_BOUGHT") : RAGE_JOAAT("MP1_NUMBER_OF_CHAMP_BOUGHT"), 5, TRUE); //Champagne
			}
		
			if (g_settings.options["lock wanted"] && !g_local_player.network_is_activity_session)
			{
				if (g_local_player.player_info)
					g_local_player.player_info->m_wanted_level_display = g_local_player.player_info->m_wanted_level = g_settings.options["lock wanted level"];
			}
		
			if (g_settings.options["always off the radar"])
			{
				if (!g_local_player.network_is_activity_session && *g_pointers->m_is_session_started)
				{
					static script_global off_the_radar_time_global = script_global(Globals::OffTheRadarTime).at(OffTheRadarTime_Offset);
					int otr_time = *off_the_radar_time_global.as<int*>();
					if (otr_time == 0 || ((NETWORK::GET_NETWORK_TIME() - otr_time) > 58000))
					{
						*lobby_player_information_global.at(OffTheRadar_IsActive).as<bool*>() = true;
						*off_the_radar_time_global.as<int*>() = NETWORK::GET_NETWORK_TIME();
					}
				}
			}
		
			if (g_settings.options["remove otr from other players"])
			{
				if (!g_local_player.network_is_activity_session && *g_pointers->m_is_session_started)
				{
					for (Player player_iterator = 0; player_iterator < MAX_PLAYERS; player_iterator++)
					{
						if (player_iterator != g_local_player.player)
						{
							auto player_otr_global = lobby_player_information.at(player_iterator, Globals::LobbyPlayerInformationSize);
							auto player_otr_is_active_1 = player_otr_global.at(OffTheRadar_IsActive).as<bool*>();
							if (*player_otr_is_active_1 == true)
								*player_otr_is_active_1 = false;
							auto player_otr_is_active_2 = player_otr_global.at(OffTheRadar_IsActive).at(1).as<bool*>();
							if (*player_otr_is_active_2 == true)
								*player_otr_is_active_2 = false;
						}
					}
				}
			}
		
			if (g_settings.options["always car clean"] && g_local_player.is_in_vehicle())
			{
				if (VEHICLE::GET_PED_IN_VEHICLE_SEAT(g_local_player.vehicle, SEAT_DRIVER, false) == g_local_player.player_ped)
					vehicle_helper::clear_dirt(g_local_player.vehicle);
			}
			
			if (g_settings.options["always car fixed"] && g_local_player.is_in_vehicle())
			{
				static auto previous_call_time = std::chrono::high_resolution_clock::now();
				auto milliseconds_delta = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - previous_call_time);
				if (milliseconds_delta > 500ms && VEHICLE::GET_PED_IN_VEHICLE_SEAT(g_local_player.vehicle, SEAT_DRIVER, false) == g_local_player.player_ped)
				{
					if (VEHICLE::_IS_VEHICLE_DAMAGED(g_local_player.vehicle))
					{
						if (g_local_player.is_vehicle_race_car || VEHICLE::IS_VEHICLE_BUMPER_BOUNCING(g_local_player.vehicle, TRUE))
							VEHICLE::SET_VEHICLE_FIXED(g_local_player.vehicle);
						else
						{
							VEHICLE::SET_VEHICLE_DEFORMATION_FIXED(g_local_player.vehicle);
							int clan_logo_counter = GRAPHICS::DOES_VEHICLE_HAVE_CREW_EMBLEM(g_local_player.vehicle, 0) * 100;
							GRAPHICS::REMOVE_DECALS_FROM_VEHICLE(g_local_player.vehicle);
							vehicle_helper::set_max_health(g_local_player.vehicle);
							while (clan_logo_counter-- > 0 && !GRAPHICS::DOES_VEHICLE_HAVE_CREW_EMBLEM(g_local_player.vehicle, 0))
							{
								vehicle_helper::add_clan_logo_to_vehicle(g_local_player.vehicle, g_local_player.player_ped);
								script::get_current()->yield();
							}
						}
					}
					if (!VEHICLE::IS_VEHICLE_WINDOW_INTACT(g_local_player.vehicle, 6) && !(g_local_player.ped_ptr->m_ped_state & PED_IS_SHOOTING))
						VEHICLE::FIX_VEHICLE_WINDOW(g_local_player.vehicle, 6);
					if (!VEHICLE::IS_VEHICLE_WINDOW_INTACT(g_local_player.vehicle, 7))
						VEHICLE::FIX_VEHICLE_WINDOW(g_local_player.vehicle, 7);
					previous_call_time = std::chrono::high_resolution_clock::now();
				}
			}
		
			if (!g_settings.options["persist weapons"].get<std::string>().empty())
			{
				static auto previous_call_time = std::chrono::high_resolution_clock::now();
				auto milliseconds_delta = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - previous_call_time);
				if (milliseconds_delta > 500ms)
				{
					persist_weapons::check_player_has_weapons(g_settings.options["persist weapons"]);
					previous_call_time = std::chrono::high_resolution_clock::now();
				}
			}

			if (g_settings.options["always win poker"] && g_local_player.is_playing_casino_games)
			{
				if (auto poker_thread = gta_util::find_script_thread(RAGE_JOAAT("three_card_poker")))
				{
					if (g_local_player.maintain_control.find(poker_thread->m_name) == g_local_player.maintain_control.end())
						g_local_player.maintain_control[poker_thread->m_name] = poker_thread;
					online_tab::set_poker_cards(poker_thread);
				}
			}

			if (!g_local_player.maintain_control.empty())
			{
				static auto previous_call_time = std::chrono::high_resolution_clock::now();
				auto milliseconds_delta = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - previous_call_time);
				if (milliseconds_delta > 500ms)
				{
					for (auto script_thread : g_local_player.maintain_control)
					{
						if (gta_util::does_script_thread_exist(script_thread.second) && script_thread.first == script_thread.second->m_name)
							network_helper::take_control_of_script(script_thread.second);
						else
							g_local_player.maintain_control.erase(script_thread.first);
					}
					previous_call_time = std::chrono::high_resolution_clock::now();
				}
			}
		
			if (!g_gui.m_opened)
			{
				if (g_settings.options["weapon hotkeys"][0] && !HUD::_IS_MULTIPLAYER_CHAT_ACTIVE())
				{
					DWORD processIdFocused;
					GetWindowThreadProcessId(GetForegroundWindow(), &processIdFocused);
					if (processIdFocused == GetCurrentProcessId())
					{
						static bool is_one_pressed = false;
						if (is_key_pressed_once(is_one_pressed, '1'))
						{
							PAD::DISABLE_CONTROL_ACTION(0, INPUT_SELECT_WEAPON_UNARMED, true);
							if (g_local_player.weapon == g_settings.options["weapon hotkeys"][1][0])
								resolve_weapon_hotkey(g_settings.options["weapon hotkeys"][1][1]);
							else
								resolve_weapon_hotkey(g_settings.options["weapon hotkeys"][1][0]);
						}
						static bool is_two_pressed = false;
						if (is_key_pressed_once(is_two_pressed, '2'))
						{
							PAD::DISABLE_CONTROL_ACTION(0, INPUT_SELECT_WEAPON_MELEE, true);
							if (!g_local_player.is_in_vehicle())
							{
								if (g_local_player.weapon == g_settings.options["weapon hotkeys"][2][0])
									resolve_weapon_hotkey(g_settings.options["weapon hotkeys"][2][1]);
								else
									resolve_weapon_hotkey(g_settings.options["weapon hotkeys"][2][0]);
							}
							else
							{
								if (g_local_player.weapon == g_settings.options["weapon hotkeys"][2][2])
									resolve_weapon_hotkey(g_settings.options["weapon hotkeys"][2][3]);
								else
									resolve_weapon_hotkey(g_settings.options["weapon hotkeys"][2][2]);
							}
						}
						static bool is_three_pressed = false;
						if (is_key_pressed_once(is_three_pressed, '3'))
						{
							PAD::DISABLE_CONTROL_ACTION(0, INPUT_SELECT_WEAPON_HANDGUN, true);
							if (g_local_player.weapon == g_settings.options["weapon hotkeys"][3][0])
								resolve_weapon_hotkey(g_settings.options["weapon hotkeys"][3][1]);
							else
								resolve_weapon_hotkey(g_settings.options["weapon hotkeys"][3][0]);
						}
						static bool is_four_pressed = false;
						if (is_key_pressed_once(is_four_pressed, '4'))
						{
							PAD::DISABLE_CONTROL_ACTION(0, INPUT_SELECT_WEAPON_SHOTGUN, true);
							if (g_local_player.weapon == g_settings.options["weapon hotkeys"][4][0])
								resolve_weapon_hotkey(g_settings.options["weapon hotkeys"][4][1]);
							else
								resolve_weapon_hotkey(g_settings.options["weapon hotkeys"][4][0]);
						}
						static bool is_five_pressed = false;
						if (is_key_pressed_once(is_five_pressed, '5'))
						{
							PAD::DISABLE_CONTROL_ACTION(0, INPUT_SELECT_WEAPON_SMG, true);
							if (g_local_player.weapon == g_settings.options["weapon hotkeys"][5][0])
								resolve_weapon_hotkey(g_settings.options["weapon hotkeys"][5][1]);
							else
								resolve_weapon_hotkey(g_settings.options["weapon hotkeys"][5][0]);
						}
						static bool is_six_pressed = false;
						if (is_key_pressed_once(is_six_pressed, '6'))
						{
							PAD::DISABLE_CONTROL_ACTION(0, INPUT_SELECT_WEAPON_AUTO_RIFLE, true);
							if (g_local_player.weapon == g_settings.options["weapon hotkeys"][6][0])
								resolve_weapon_hotkey(g_settings.options["weapon hotkeys"][6][1]);
							else
								resolve_weapon_hotkey(g_settings.options["weapon hotkeys"][6][0]);
						}
					}
				}
			}
		
			if (g_settings.options["always clean ped"])
			{
				if (auto decal_controller = *g_pointers->m_decal_controller)
				{
					if (g_local_player.ped_ptr->m_decal_index != -1)
					{
						auto decal_list = decal_controller->ped_decal_list[g_local_player.ped_ptr->m_decal_index];
						decal_list->clear_non_perm_decals_from_decal_array();
						if (!decal_list->decal_array.size())
							decal_list->decal_array.clear();
					}
				}
			}
		
			if (g_settings.options["eat spaghettios"])
			{
				for (int i = 0; i < 1500; i++)
				{
					rage::Blip_t* blip = g_pointers->m_blip_list->m_Blips[i].m_pBlip;
					if (blip != NULL)
					{
						if (blip->m_color != BlipColors::Blue) //Don't hit friendlies.
						{
							if ((blip->m_icon == BlipIcons::Cop) ||
								(blip->m_icon == BlipIcons::Enemy) ||
								(blip->m_icon == BlipIcons::Circle && blip->m_color == BlipColors::Red) ||
								((blip->m_icon == BlipIcons::Crosshair || blip->m_icon == BlipIcons::Crosshair2 || blip->m_icon == BlipIcons::Crosshair3) && (blip->m_color == BlipColors::RedMission || blip->m_color == BlipColors::Red)))
							{
								static bool bShoot = false;
								bShoot = !bShoot;
								if (bShoot)
								{
									if (blip->m_scale_x >= 1.0f)
										FIRE::ADD_OWNED_EXPLOSION(g_local_player.player_ped, blip->m_x, blip->m_y, blip->m_z, EXPLOSION_STICKYBOMB, 1.0f, FALSE, TRUE, 0.0f);
									else
										MISC::SHOOT_SINGLE_BULLET_BETWEEN_COORDS(blip->m_x + 0.1f, blip->m_y, blip->m_z - 0.15f, blip->m_x - 0.1f, blip->m_y, blip->m_z + 1, 1000, TRUE, WEAPON_ASSAULTRIFLE, g_local_player.player_ped, TRUE, TRUE, 1.0f); //FWARRRRRRAING! ~benji Alaska 2277
								}
							}
							if (((blip->m_color <= BlipColors::Red) && (blip->m_icon == BlipIcons::EnemyHelicopter || blip->m_icon == BlipIcons::PoliceHelicopter)) ||
								((blip->m_color == BlipColors::Red || blip->m_color == BlipColors::BlipColorEnemy) && (blip->m_icon == BlipIcons::PlaneDrop || blip->m_icon == BlipIcons::Motorcycle || blip->m_icon == BlipIcons::PersonalVehicleCar || blip->m_icon == BlipIcons::Helicopter2 || blip->m_icon == BlipIcons::PlayerJet || blip->m_icon == BlipIcons::PlayerHelicopter || blip->m_icon == BlipIcons::SAMTurret || blip->m_icon == BlipIcons::Molotok)))
								FIRE::ADD_OWNED_EXPLOSION(g_local_player.player_ped, blip->m_x, blip->m_y, blip->m_z, EXPLOSION_STICKYBOMB, 1.f, FALSE, TRUE, 0.0f);
						}
					}
				}
			}
		
			if (g_settings.options["eat opressormk2"])
			{
				for (int i = 0; i < 1500; i++)
				{
					rage::Blip_t* blip = g_pointers->m_blip_list->m_Blips[i].m_pBlip;
					if (blip != NULL)
					{
						if (blip->m_icon == BlipIcons::OpressorMKII)
						{
							if (blip->m_display_bits & BlipDisplayBits::BlipIsFriend)
								continue;
							static script_global info_global = script_global(Globals::PlayerInformation);
							auto player_company = *info_global.at(g_local_player.player, PlayerInformation_Size).at(11).as<int*>();
							if (player_company != -1)
							{
								if (auto player_blip = blip_helper::get_local_player())
								{
									if(player_blip->m_color == blip->m_color)
										continue;
								}
							}
							FIRE::ADD_OWNED_EXPLOSION(g_local_player.player_ped, blip->m_x, blip->m_y, blip->m_z, EXPLOSION_STICKYBOMB, 1.f, FALSE, TRUE, 0.0f);
						}
					}
				}
			}
		
			if (g_settings.options["super jump"])
			{
				if (g_local_player.player_info)
					g_local_player.player_info->m_frame_flags |= eFrameFlagSuperJump;
			}
		
			if (g_settings.options["super run"])
			{
				bool is_running = (TASK::IS_PED_RUNNING(g_local_player.player_ped) || TASK::IS_PED_SPRINTING(g_local_player.player_ped)) && PAD::IS_CONTROL_PRESSED(0, INPUT_SPRINT);
				if (is_running && !ENTITY::IS_ENTITY_IN_AIR(g_local_player.player_ped) && !PED::IS_PED_RAGDOLL(g_local_player.player_ped))
					ENTITY::APPLY_FORCE_TO_ENTITY(g_local_player.player_ped, 1, 0.f, 1.3f, 0.f, 0.f, 0.f, 0.f, 0, TRUE, FALSE, TRUE, FALSE, TRUE);
			}
		
			if (g_settings.options["godmode"])
			{
				if (g_local_player.ped_ptr && !g_local_player.ped_ptr->is_invincible())
					g_local_player.ped_ptr->enable_invincible();
			}
		
			static bool last_mobile_radio_state;
			bool mobile_radio = g_settings.options["mobile radio"];
			if (mobile_radio != last_mobile_radio_state)
			{
				PLAYER::_0x2F7CEB6520288061(mobile_radio);
				AUDIO::SET_AUDIO_FLAG("AllowRadioDuringSwitch", mobile_radio);
				AUDIO::SET_MOBILE_PHONE_RADIO_STATE(mobile_radio);
				AUDIO::SET_AUDIO_FLAG("MobileRadioInGame", mobile_radio);
				last_mobile_radio_state = mobile_radio;
			}
		
			static script_global tunables_global = script_global(Globals::Tuneables);
			if (g_settings.options["anti-afk"])
			{
				if (*tunables_global.at(Tuneables_AFK).as<int*>() != INT_MAX)
				{
					*tunables_global.at(Tuneables_AFK).as<int*>() = INT_MAX;
					*tunables_global.at(Tuneables_AFK+1).as<int*>() = INT_MAX;
					*tunables_global.at(Tuneables_AFK+2).as<int*>() = INT_MAX;
					*tunables_global.at(Tuneables_AFK+3).as<int*>() = INT_MAX;
				}
			}
		
			if (g_settings.options["bypass casino win limit"])
			{
				int chips_won{};
				STATS::STAT_GET_INT(RAGE_JOAAT("MPPLY_CASINO_CHIPS_WON_GD"), &chips_won, -1);
				if (chips_won >= *tunables_global.at(Tuneables_CasinoLimit).as<int*>())
					STATS::STAT_SET_INT(RAGE_JOAAT("MPPLY_CASINO_CHIPS_WON_GD"), 0, TRUE); //Casino chips the player has won by betting this past gameday
			}
		
			if (g_settings.options["explosive melee"])
			{
				if (auto player_info = g_local_player.player_info)
					player_info->m_frame_flags |= eFrameFlagExplosiveMelee;
			}
		
			if (g_settings.options["explosive ammo"])
			{
				if (auto player_info = g_local_player.player_info)
					player_info->m_frame_flags |= eFrameFlagExplosiveAmmo;
			}
		
			if (g_settings.options["vehicle flymode"] && g_local_player.is_in_vehicle() &&
				!PAD::IS_CONTROL_PRESSED(2, INPUT_VEH_BRAKE) && PED::IS_PED_SITTING_IN_ANY_VEHICLE(g_local_player.player_ped))
			{
				if (VEHICLE::GET_PED_IN_VEHICLE_SEAT(g_local_player.vehicle, SEAT_DRIVER, false) != g_local_player.player_ped)
					network_helper::request_control(g_local_player.vehicle);
				PAD::DISABLE_CONTROL_ACTION(0, INPUT_VEH_MOVE_LEFT_ONLY, true);
				PAD::DISABLE_CONTROL_ACTION(0, INPUT_VEH_MOVE_RIGHT_ONLY, true);
				Vector3 rot = ENTITY::GET_ENTITY_ROTATION(g_local_player.vehicle, 0);
		
				if (PAD::IS_DISABLED_CONTROL_PRESSED(2, INPUT_VEH_MOVE_UP_ONLY))
					rot.x -= 1.3f;
				if (PAD::IS_DISABLED_CONTROL_PRESSED(2, INPUT_VEH_MOVE_DOWN_ONLY))
					rot.x += 1.3f;
		
				if (PAD::IS_DISABLED_CONTROL_PRESSED(2, INPUT_VEH_MOVE_LEFT_ONLY))
					rot.z += 1.3f;
				if (PAD::IS_DISABLED_CONTROL_PRESSED(2, INPUT_VEH_MOVE_RIGHT_ONLY))
					rot.z -= 1.3f;
		
				if (PAD::IS_CONTROL_PRESSED(2, INPUT_VEH_ACCELERATE))
					VEHICLE::SET_VEHICLE_FORWARD_SPEED(g_local_player.vehicle, (PAD::IS_DISABLED_CONTROL_PRESSED(2, INPUT_VEH_PUSHBIKE_SPRINT)) ? 300.f : 80.f);
				if (PAD::IS_DISABLED_CONTROL_PRESSED(2, INPUT_VEH_HANDBRAKE))
					VEHICLE::SET_VEHICLE_FORWARD_SPEED(g_local_player.vehicle, 0);
		
		
				if (ENTITY::IS_ENTITY_IN_AIR(g_local_player.vehicle))
					rot.y = 0;
		
				ENTITY::SET_ENTITY_ROTATION(g_local_player.vehicle, rot.x, rot.y, rot.z, 0, true);
			}
		
			if (g_settings.options["infinite car boost"] && g_local_player.is_in_vehicle() && VEHICLE::GET_PED_IN_VEHICLE_SEAT(g_local_player.vehicle, SEAT_DRIVER, false) == g_local_player.player_ped)
			{
				if (VEHICLE::_GET_HAS_ROCKET_BOOST(g_local_player.vehicle) && (g_local_player.vehicle_ptr->m_boost_bits & ActivelyBoosting))
				{
					if (PAD::IS_CONTROL_PRESSED(0, INPUT_VEH_ROCKET_BOOST) || PAD::IS_CONTROL_PRESSED(0, INPUT_VEH_FLY_BOOST))
					{
						g_local_player.vehicle_ptr->m_rocket_boost = 1.24f;
					}
					else
					{
						g_local_player.vehicle_ptr->m_rocket_boost = 0.f;
						script::get_current()->yield();
						g_local_player.vehicle_ptr->m_rocket_boost = 1.25f;
					}
				}
				else if (VEHICLE::GET_VEHICLE_HAS_KERS(g_local_player.vehicle) && g_local_player.vehicle_ptr->m_boost < g_local_player.vehicle_ptr->m_boost_max)
				{
					g_local_player.vehicle_ptr->m_boost = g_local_player.vehicle_ptr->m_boost_max - 0.01f;
				}
			}
		}
	}

	void features::resolve_weapon_hotkey(Hash weapon)
	{
		if (weapon_info::resolve_vehicle_weapon_name(weapon) != nullptr)
			WEAPON::SET_CURRENT_PED_VEHICLE_WEAPON(g_local_player.player_ped, weapon);
		else
			WEAPON::SET_CURRENT_PED_WEAPON(g_local_player.player_ped, weapon, TRUE);
	}

	void features::script_func()
	{
		while (true)
		{
			TRY_CLAUSE
			{
				run_tick(); 
			}
			EXCEPT_CLAUSE
			script::get_current()->yield();
		}
	}
}
