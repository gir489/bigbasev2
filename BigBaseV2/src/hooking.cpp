#include "common.hpp"
#include "function_types.hpp"
#include "logger.hpp"
#include "gta/player.hpp"
#include "gta/script_thread.hpp"
#include "gui.hpp"
#include "hooking.hpp"
#include "memory/module.hpp"
#include "natives.hpp"
#include "pointers.hpp"
#include "renderer.hpp"
#include "script_mgr.hpp"
#include "hooks\EventHook.h"
#include "hooks\NetEventHook.h"
#include "CustomCode\ChatCommands.h"
#include "gta/CrashHashes.h"
#include "MinHook.h"
#include "CustomCode/PlayerInfo.h"
#include "fiber_pool.hpp"
#include "CustomCode/AntiCheatHandle.h"
#include "gui/GamerMonitor.h"

namespace big
{
	hooking::hooking() :
		m_swapchain_hook(*g_pointers->m_swapchain, hooks::swapchain_num_funcs),
		m_set_cursor_pos_hook("SetCursorPos", memory::module("user32.dll").get_export("SetCursorPos").as<void*>(), &hooks::set_cursor_pos),
		m_metric_script_event_spam_hook("MetricScriptEventSpam", g_pointers->m_metric_script_event_spam, &hooks::metric_script_event_spam_hook),
		m_network_event_hook("NetworkEventFunction", g_pointers->m_network_event_function, &hooks::network_event_hook),
		m_receive_chat_message_hook("Receive Chat Message", g_pointers->m_receive_chat_message, &hooks::receive_chat_message_hook),
		m_received_clone_create_hook("Received Clone Create", g_pointers->m_received_clone_create, &hooks::received_clone_create_hook),
		m_run_script_threads_hook("Script Hook", g_pointers->m_run_script_threads, &hooks::run_script_threads),
		m_censor_chat_text_hook("Censor Chat", g_pointers->m_censor_chat_text, &hooks::censor_chat_text),
		m_send_net_info_to_lobby_hook("Send Net Info To Lobby", g_pointers->m_send_net_info_to_lobby, &hooks::send_net_info_to_lobby_hook),
		m_is_tag_restricted_hook("Is Tag Restricted", g_pointers->m_is_tag_restricted, &hooks::is_tag_restricted_hook),
		m_attach_object_to_entity_hook("Attach Object To Entity", g_pointers->m_attach_object_to_entity, &hooks::attach_object_to_entity_hook),
		m_attach_ped_to_entity_hook("Attach Ped To Entity", g_pointers->m_attach_ped_to_entity, &hooks::attach_ped_to_entity_hook),
		//m_read_session_info_response_hook("Read Session Info Response", g_pointers->m_read_session_info_response, &hooks::read_session_info_response_hook),
		m_convert_thread_to_fiber_hook("ConvertThreadToFiber", memory::module("kernel32.dll").get_export("ConvertThreadToFiber").as<void*>(), &hooks::convert_thread_to_fiber)
	{
		m_swapchain_hook.hook(hooks::swapchain_present_index, &hooks::swapchain_present);
		m_swapchain_hook.hook(hooks::swapchain_resizebuffers_index, &hooks::swapchain_resizebuffers);

		g_hooking = this;
	}

	hooking::~hooking()
	{
		if (m_enabled)
			disable();

		g_hooking = nullptr;
	}

	void hooking::enable()
	{
		m_og_wndproc = reinterpret_cast<WNDPROC>(SetWindowLongPtrW(g_pointers->m_hwnd, GWLP_WNDPROC, reinterpret_cast<LONG_PTR>(&hooks::wndproc)));
		m_convert_thread_to_fiber_hook.enable();
		m_metric_script_event_spam_hook.enable();
		m_network_event_hook.enable();
		m_attach_object_to_entity_hook.enable();
		m_attach_ped_to_entity_hook.enable();
		m_is_tag_restricted_hook.enable();
		m_received_clone_create_hook.enable();
		m_censor_chat_text_hook.enable();
		m_send_net_info_to_lobby_hook.enable();
		m_run_script_threads_hook.enable();
		m_receive_chat_message_hook.enable();
		m_set_cursor_pos_hook.enable();
		m_swapchain_hook.enable();
		//m_read_session_info_response_hook.enable();

		anti_cheat_handle::Initialize();

		m_enabled = true;
	}

	void hooking::disable()
	{
		m_enabled = false;

		SetWindowLongPtrW(g_pointers->m_hwnd, GWLP_WNDPROC, reinterpret_cast<LONG_PTR>(m_og_wndproc));
		m_convert_thread_to_fiber_hook.disable();
		m_metric_script_event_spam_hook.disable();
		m_network_event_hook.disable();
		m_attach_object_to_entity_hook.disable();
		m_attach_ped_to_entity_hook.disable();
		m_is_tag_restricted_hook.disable();
		m_received_clone_create_hook.disable();
		m_censor_chat_text_hook.disable();
		m_send_net_info_to_lobby_hook.disable();
		m_run_script_threads_hook.disable();
		m_receive_chat_message_hook.disable();
		m_set_cursor_pos_hook.disable();
		m_swapchain_hook.disable();
		//m_read_session_info_response_hook.disable();

		anti_cheat_handle::Uninitialize();
	}

	minhook_keepalive::minhook_keepalive()
	{
		MH_Initialize();
	}

	minhook_keepalive::~minhook_keepalive()
	{
		MH_Uninitialize();
	}

	bool hooks::run_script_threads(std::uint32_t ops_to_execute)
	{
		TRY_CLAUSE
		{
			if (g_running)
			{
				g_script_mgr.tick();
			}

			return g_hooking->m_run_script_threads_hook.get_original<functions::run_script_threads_t>()(ops_to_execute);
		} EXCEPT_CLAUSE
		return false;
	}

	void* hooks::convert_thread_to_fiber(void* param)
	{
		TRY_CLAUSE
		{
			if (IsThreadAFiber())
			{
				return GetCurrentFiber();
			}

			return g_hooking->m_convert_thread_to_fiber_hook.get_original<decltype(&convert_thread_to_fiber)>()(param);
		} EXCEPT_CLAUSE
		return nullptr;
	}

	HRESULT hooks::swapchain_present(IDXGISwapChain* this_, UINT sync_interval, UINT flags)
	{
		TRY_CLAUSE
		{
			if (g_running)
			{
				g_renderer->on_present();
			}

			return g_hooking->m_swapchain_hook.get_original<decltype(&swapchain_present)>(swapchain_present_index)(this_, sync_interval, flags);
		} EXCEPT_CLAUSE
		return NULL;
	}

	HRESULT hooks::swapchain_resizebuffers(IDXGISwapChain* this_, UINT buffer_count, UINT width, UINT height, DXGI_FORMAT new_format, UINT swapchain_flags)
	{
		TRY_CLAUSE
		{
			if (g_running)
			{
				g_renderer->pre_reset();

				auto result = g_hooking->m_swapchain_hook.get_original<decltype(&swapchain_resizebuffers)>(swapchain_resizebuffers_index)
					(this_, buffer_count, width, height, new_format, swapchain_flags);

				if (SUCCEEDED(result))
				{
					g_renderer->post_reset();
				}

				return result;
			}

			return g_hooking->m_swapchain_hook.get_original<decltype(&swapchain_resizebuffers)>(swapchain_resizebuffers_index)
				(this_, buffer_count, width, height, new_format, swapchain_flags);
		} EXCEPT_CLAUSE
		return NULL;
	}

	LRESULT hooks::wndproc(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
	{
		TRY_CLAUSE
		{
			if (g_running)
			{
				g_renderer->wndproc(hwnd, msg, wparam, lparam);
			}

			return CallWindowProcW(g_hooking->m_og_wndproc, hwnd, msg, wparam, lparam);
		} EXCEPT_CLAUSE
		return NULL;
	}

	BOOL hooks::set_cursor_pos(int x, int y)
	{
		TRY_CLAUSE
		{
			if (g_gui.m_opened)
				return true;

			return g_hooking->m_set_cursor_pos_hook.get_original<decltype(&set_cursor_pos)>()(x, y);
		} EXCEPT_CLAUSE
		return FALSE;
	}

	BOOL hooks::rec_clone(CNetworkObjectMgr* this_ptr, CNetGamePlayer* sender, CNetGamePlayer* receiver, uint16_t object_type, uint16_t object_id, uint16_t object_flags, rage::datBitBuffer* bitbuffer, int timestamp)
	{
		//Check if the player was marked, or this was an attachment item.
		if (g_persist_modder.is_banned_from_spawning_objects(sender))
			return TRUE;
		if (object_type > NET_OBJ_TYPE_TRAIN)
		{
			NetEventHook::ReportSpawnedObject(sender, "Bad Object", "Crash Model", 50);
			return TRUE;
		}
		if (sender->player_id == -1 || sender->bubble_id == 10 || sender->bubble_id != receiver->bubble_id)
			return g_hooking->m_received_clone_create_hook.get_original<decltype(&received_clone_create_hook)>()(this_ptr, sender, receiver, object_type, object_id, object_flags, bitbuffer, timestamp);
		Hash model{};
		auto name = sender->player_info->m_name;
		//(Most)Hackers only have script level access, they can't make engine requests.
		bool events_need_checking = (object_flags & (NET_OBJ_FLAGS_FROM_SCRIPT | NET_OBJ_FLAGS_SCRIPTED)) || object_type == NET_OBJ_TYPE_PICKUP;
		if (events_need_checking)
		{
			auto sync_tree = g_pointers->m_get_sync_tree(this_ptr, object_type);
			auto old_location = bitbuffer->GetPosition();
			g_pointers->m_populate_sync_tree(sync_tree, SYNC_RECEIVED_CLONE_CREATE, SYNC_RECEIVED_CLONE_CREATE, bitbuffer, nullptr);
			switch (object_type)
			{
				case NET_OBJ_TYPE_PED:
					model = sync_tree->m_sync_tree_node->m_ped_model;
					break;
				case NET_OBJ_TYPE_DOOR:
					model = sync_tree->m_sync_tree_node->m_player_model;
					break;
				case NET_OBJ_TYPE_PLAYER:
					model = sync_tree->m_sync_tree_node->m_player_model;
					switch (model)
					{
						case RAGE_JOAAT("mp_m_freemode_01"):
						case RAGE_JOAAT("mp_f_freemode_01"):
							break;
						default:
							g_persist_modder.add_modder(sender, "Invalid Player Model", 50);
							LOG(HACKER) << "Invalid player model " << HEX_TO_UPPER(model) << " from " << name;
							draw_helper::draw_headshot_notification("Invalid Player Model", sender->get_name(), sender->player_id, "", HUD_COLOUR_RED);
							return TRUE;
					}
					break;
				case NET_OBJ_TYPE_AUTOMOBILE:
				case NET_OBJ_TYPE_BIKE:
				case NET_OBJ_TYPE_BOAT:
				case NET_OBJ_TYPE_HELI:
				case NET_OBJ_TYPE_PLANE:
				case NET_OBJ_TYPE_TRAILER:
				case NET_OBJ_TYPE_TRAIN:
				case NET_OBJ_TYPE_SUBMARINE:
					model = sync_tree->m_sync_tree_node->m_vehicle_model;
					break;
				case NET_OBJ_TYPE_PICKUP:
				case NET_OBJ_TYPE_PICKUP_PLACEMENT:
					model = sync_tree->m_sync_tree_node->m_pickup_model;
					break;
				case NET_OBJ_TYPE_OBJECT:
					model = sync_tree->m_sync_tree_node->m_object_model;
			}
			if (is_crash_model(model))
			{
				NetEventHook::ReportSpawnedObject(sender, name, "Crash Model", 50);
				if (g_settings.options["log object events"])
					LOG(EVENT) << "Blocked Network Object from: player:" << name << " object_id:" << object_id << " object_type:" << object_type << " object_flags:" << object_flags << " model:" << HEX_TO_UPPER(model);
				return TRUE;
			}
			bitbuffer->Seek(old_location);
		}
		//Point of no return.
		auto retn_val = g_hooking->m_received_clone_create_hook.get_original<decltype(&received_clone_create_hook)>()(this_ptr, sender, receiver, object_type, object_id, object_flags, bitbuffer, timestamp);
		//Check if they spawned a ped near us.
		if (events_need_checking && !g_local_player.network_is_activity_session && g_local_player.ped_ptr != nullptr)
		{
			auto net_obj = this_ptr->find_object_by_id(object_id, FALSE);
			if (net_obj != nullptr && object_type == NET_OBJ_TYPE_PED)
			{
				auto ped = (rage::CPed*)net_obj->GetGameObject();
				if (ped != nullptr && ped->m_navigation != nullptr && g_local_player.ped_ptr->m_navigation != nullptr)
				{
					auto x = ped->m_navigation->m_position.x - g_local_player.ped_ptr->m_navigation->m_position.x;
					auto y = ped->m_navigation->m_position.y - g_local_player.ped_ptr->m_navigation->m_position.y;
					auto sqst = x * x + y * y;
					auto distance = sqrt(sqst);
					//This will fail if the player creates a ped, then teleports it to the player. (Most)Hackers just spawn it on the player.
					if (distance < 1.f)
					{
						g_persist_modder.ban_from_objects(sender);
						NetEventHook::ReportSpawnedObject(sender, name, "Ped", 50);
						if (g_settings.options["log object events"])
							LOG(EVENT) << "Blocked Network Object from: player:" << name << " object_id:" << object_id << " object_type:" << object_type << " object_flags:" << object_flags << " model:" << HEX_TO_UPPER(model);
						return TRUE;
					}
				}
			}
			if (g_settings.options["log object events"])
				LOG(EVENT) << "Network Object from: player:" << name << " object_id:" << object_id << " object_type:" << object_type << " object_flags:" << object_flags << " model:" << HEX_TO_UPPER(model);
		}
		return retn_val;
	}

	BOOL hooks::received_clone_create_hook(CNetworkObjectMgr* this_ptr, CNetGamePlayer* sender, CNetGamePlayer* receiver, uint16_t object_type, uint16_t object_id, uint16_t object_flags, rage::datBitBuffer* bitbuffer, int timestamp)
	{
		TRY_CLAUSE
		{
			return rec_clone(this_ptr, sender, receiver, object_type, object_id, object_flags, bitbuffer, timestamp);
		} EXCEPT_CLAUSE
		return FALSE;
	}

	void hooks::network_event_hook(rage::netEventMgr* this_ptr, CNetGamePlayer* source_player, CNetGamePlayer* target_player, uint16_t event_id, int event_index, int event_handled_bitset, uint16_t bitbuffer_size, rage::datBitBuffer* buffer)
	{
		TRY_CLAUSE
		{
			if (NetEventHook::NetworkEventFunction(this_ptr, source_player, target_player, event_id, event_index, event_handled_bitset, buffer))
				g_hooking->m_network_event_hook.get_original<decltype(&network_event_hook)>()(this_ptr, source_player, target_player, event_id, event_index, event_handled_bitset, bitbuffer_size, buffer);
		} EXCEPT_CLAUSE
	}

	void hooks::metric_script_event_spam_hook(int eventId)
	{
		return;
	}

	bool hooks::anti_cheat_function_hook(__int64 a1, DWORD* a2)
	{
		return true;
	}

	void hooks::receive_chat_message_hook(__int64 this_ptr, __int64 a2, std::uint64_t sender_gamer_id, const char* message)
	{
		TRY_CLAUSE
		{
			if (auto sender_ptr = g_pointers->m_get_netplayer_from_gamerhandle(sender_gamer_id))
				chat_commands::handle_chat_command(sender_ptr, message);
			g_hooking->m_receive_chat_message_hook.get_original<decltype(&receive_chat_message_hook)>()(this_ptr, a2, sender_gamer_id, message);
		} EXCEPT_CLAUSE
	}

	int hooks::censor_chat_text_func(__int64 chat_menu, const char* user_text, const char** output_text)
	{
		if (g_settings.options["log chat events"].get<bool*>())
			LOG(EVENT) << "Network Chat Event sent. Message: " << user_text;
		if (g_settings.options["gamer mode"].get<bool>() == false)
			return g_hooking->m_censor_chat_text_hook.get_original<decltype(&censor_chat_text)>()(chat_menu, user_text, output_text);
		return -1;
	}

	int hooks::censor_chat_text(__int64 chat_menu, const char* user_text, const char** output_text)
	{
		TRY_CLAUSE
		{
			return censor_chat_text_func(chat_menu, user_text, output_text);
		} EXCEPT_CLAUSE
		return -1;
	}

	bool hooks::is_tag_restricted_hook(int64_t identifier, Hash component_hash, Hash restriction_tag)
	{
		TRY_CLAUSE
		{
			if (restriction_tag == RAGE_JOAAT("JUGG_SUIT")) //This will allow the player to use the Ballistic suit anywhere in the game.
				return false;
			return g_hooking->m_is_tag_restricted_hook.get_original<decltype(&is_tag_restricted_hook)>()(identifier, component_hash, restriction_tag);
		} EXCEPT_CLAUSE
		return false;
	}

	bool hooks::attach_object_to_entity_hook_func(rage::netObject* object, rage::fwEntity* target)
	{
		if (target == g_local_player.ped_ptr)
		{
			auto owner = object->m_owner_id;
			auto net_player = g_pointers->m_get_net_player(owner);
			g_persist_modder.add_modder(net_player, "Spawned Attached Object", 50);
			LOG(HACKER) << "Object Model " << HEX_TO_UPPER(object->GetGameObject()->m_model_info->m_model) << " removed spawn by " << net_player->get_name();
			draw_helper::draw_headshot_notification("Removed Attachment", net_player->get_name(), owner, "", HUD_COLOUR_RED);
			auto pCObject = object->GetGameObject();
			network_helper::destory_prop(g_pointers->m_ptr_to_handle(pCObject), pCObject);
			return false;
		}
		return g_hooking->m_attach_object_to_entity_hook.get_original<decltype(&attach_object_to_entity_hook)>()(object, target);
	}

	bool hooks::attach_object_to_entity_hook(rage::netObject* object, rage::fwEntity* target)
	{
		TRY_CLAUSE
		{
			//Object unwinding nonsense...
			return attach_object_to_entity_hook_func(object, target);
		} EXCEPT_CLAUSE
		return false;
	}

	bool hooks::attach_ped_to_entity_hook_func(rage::netObject* ped, rage::fwEntity* target)
	{
		if (target == g_local_player.ped_ptr)
		{
			auto owner = ped->m_owner_id;
			auto net_player = g_pointers->m_get_net_player(owner);
			g_persist_modder.add_modder(net_player, "Spawned Attached Object", 50);
			LOG(HACKER) << "Object Model " << HEX_TO_UPPER(ped->GetGameObject()->m_model_info->m_model) << " removed spawn by " << net_player->get_name();
			draw_helper::draw_headshot_notification("Removed Attachment", net_player->get_name(), owner, "", HUD_COLOUR_RED);
			auto pCObject = ped->GetGameObject();
			network_helper::destory_ped(g_pointers->m_ptr_to_handle(pCObject), (rage::CPed*)pCObject);
			return false;
		}
		return g_hooking->m_attach_ped_to_entity_hook.get_original<decltype(&attach_ped_to_entity_hook)>()(ped, target);
	}

	bool hooks::attach_ped_to_entity_hook(rage::netObject* ped, rage::fwEntity* target)
	{
		TRY_CLAUSE
		{
			//Object unwinding nonsense...
			return attach_ped_to_entity_hook_func(ped, target);
		} EXCEPT_CLAUSE
		return false;
	}

	bool hooks::send_net_info_to_lobby_hook_func(int64_t a1, int64_t* a2)
	{
		rage::netPlayerData* local_player = (rage::netPlayerData*)(a2 + 1);

		auto name = std::string(local_player->m_name);
		auto name_real = g_settings.options["spoof name_real"].get<std::string>();
		auto scid_real = g_settings.options["spoof scid_real"].get<uint64_t>();
		if (g_local_player.is_player_switch_in_progress || g_local_player.is_loading_screen_active) //Fix for some reason this function is called when in a local lobby.
		{
			if (name_real.empty() || scid_real != local_player->m_rockstar_id) //Check if new or different account.
			{
				g_settings.options["spoof name_real"] = name;
				g_settings.options["spoof scid_real"] = scid_real = local_player->m_rockstar_id;
				g_settings.save();
			}
		}
		if (scid_real == local_player->m_rockstar_id)
		{
			auto spoof_name = g_settings.options["spoof name"].get<std::string>();
			auto spoof_scid = g_settings.options["spoof scid"].get<uint64_t>();
			bool name_diff = !spoof_name.empty() && spoof_name != name;
			bool scid_diff = spoof_scid != 0 && spoof_scid != local_player->m_rockstar_id;
#ifdef _DEBUG
			LOG(INFO_TO_FILE) << "name_diff: " << name_diff << " spoof_name: " << spoof_name << " name: " << name << " " << HEX_TO_UPPER(local_player);
#endif
			if (name_diff)
				rage_helper::change_name(spoof_name, local_player);
			if (scid_diff)
				local_player->m_rockstar_id = local_player->m_rockstar_id_2 = spoof_scid;
			auto retnvalue = g_hooking->m_send_net_info_to_lobby_hook.get_original<decltype(&send_net_info_to_lobby_hook)>()(a1, a2);
			if (scid_diff)
				local_player->m_rockstar_id = local_player->m_rockstar_id_2 = scid_real;
			return retnvalue;
		}
		return g_hooking->m_send_net_info_to_lobby_hook.get_original<decltype(&send_net_info_to_lobby_hook)>()(a1, a2);
	}

	bool hooks::send_net_info_to_lobby_hook(int64_t a1, int64_t* a2)
	{
		TRY_CLAUSE
		{
			return send_net_info_to_lobby_hook_func(a1, a2);
		} EXCEPT_CLAUSE
		return false;
	}

	bool hooks::read_session_info_response_hook_func(QueryGamerResponse* response)
	{
		if (response && response->m_response_size)
		{
			for (int i = 0; i < (int)response->m_response_size; i++)
			{
				nlohmann::json json = nlohmann::json::parse(response->m_response_data[i]);
				//LOG(INFO) << "Raw Response: " << response->m_response_data[i];

				if (json.find("gsinfo") == json.end())
					return false;

				uint64_t rockstar_id = std::stoul(json["_id"].get<std::string>().substr(3));
				std::string gs_info = json["gsinfo"].get<std::string>();

				bool is_playing = !gs_info.empty();
				bool is_online = json.find("gstype") != json.end();

				auto retn = g_pending_gamer_group.find(rockstar_id);
				if (retn != g_pending_gamer_group.end())
				{
					gamerlist_info::gamerlist_status status = gamerlist_info::gamerlist_status::offline;
					if (is_online)
						status = gamerlist_info::gamerlist_status::online;
					if (is_playing)
						status = gamerlist_info::gamerlist_status::session;

					gamer_monitor::gamer_list[retn->second].status = status;
				}
					
			}

			g_pending_gamer_group.clear();
		}

		//return g_hooking->m_read_session_info_response_hook.get_original<decltype(&read_session_info_response_hook)>()(response);
	}

	bool hooks::read_session_info_response_hook(QueryGamerResponse* response)
	{
		TRY_CLAUSE
		{
			return read_session_info_response_hook_func(response);
		} EXCEPT_CLAUSE
		return false;
	}
}
