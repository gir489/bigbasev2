#pragma once
#include "common.hpp"
#include "gta/fwddec.hpp"
#include "gta/enums.hpp"
#include "gta/blip.hpp"
#include "gta/decal_controller.hpp"
#include "function_types.hpp"
#include "gta\replay.hpp"
#include "helpers\DrawHelper.h"
#include "gta/matrix.hpp"

namespace big
{
	class pointers
	{
	public:
		explicit pointers();
		~pointers();
	public:
		HWND m_hwnd{};

		eGameState *m_game_state{};
		bool *m_is_session_started{};

		rage::CPedFactory **m_ped_factory{};
		CNetworkPlayerMgr **m_network_player_mgr{};

		rage::scrNativeRegistrationTable *m_native_registration_table{};
		functions::get_native_handler_t m_get_native_handler{};
		functions::fix_vectors_t m_fix_vectors{};
		functions::ptr_to_handle_t m_ptr_to_handle{};
		functions::trigger_script_event_t m_trigger_script_event{};
		functions::get_player_ped_address_t m_get_player_ped_address{};
		functions::get_ped_data_t m_get_ped_data{};
		//functions::set_lobby_weather_t m_set_lobby_weather{};

		rage::atArray<GtaThread*> *m_script_threads{};
		rage::scrProgramTable *m_script_program_table{};
		functions::run_script_threads_t m_run_script_threads{};
		std::int64_t **m_script_globals{};

		CGameScriptHandlerMgr **m_script_handler_mgr{};

		IDXGISwapChain **m_swapchain{};
		CNetworkObjectMgr** m_networked_object_manager{};
		PVOID m_received_clone_create{};

		PVOID m_force_host_patch{};

        PVOID m_network_event_function{};
        PVOID m_metric_script_event_spam{};
        PVOID m_model_spawn_bypass{};
        PVOID m_anti_cheat_function{};
		PVOID m_spectate_control_bypass{};
		PVOID m_receive_chat_message{};
		PVOID m_text_message_hook{};
		PVOID m_send_net_info_to_lobby{};
		PVOID m_is_tag_restricted{};
		PVOID m_attach_object_to_entity{};
		PVOID m_attach_ped_to_entity{};
		PVOID m_censor_chat_text{};
		PVOID m_read_session_info_response{};

        rage::decal_controller** m_decal_controller{};

		rage::CReplayInterface* m_replay_interface;

		functions::get_netplayer_from_gamerhandle m_get_netplayer_from_gamerhandle{};
		functions::get_net_player_t m_get_net_player{};
		functions::find_net_object_by_id_t m_find_object_by_id{};
		//functions::set_lobby_time_t m_set_lobby_time{};
		functions::read_bitbuf_dword m_read_bitbuf_dword{};
		functions::read_bitbuf_array m_read_bitbuf_array{};
		functions::send_event_ack m_send_event_ack{};
		functions::get_entity_from_id m_get_entity_from_id{};
		functions::send_give_reward_pickup m_send_give_reward_pickup{};
		functions::get_network_name m_get_network_name{};
		functions::join_player m_join_player{};
		functions::get_friends_list_t m_get_friends_list{};
		functions::delete_object m_delete_object{};
		functions::get_vehicle_ptr m_get_vehicle_ptr{};
		functions::get_ped_ptr m_get_ped_ptr{};
		functions::send_network_event m_send_network_event_function{};
		functions::delete_ped m_delete_ped{};
		functions::get_sync_tree m_get_sync_tree{};
		functions::populate_sync_tree m_populate_sync_tree{};
		functions::world_to_scren m_world_to_scren{};
		functions::set_new_script_host_t m_set_new_script_host{};
		functions::send_session_info_request_t m_send_session_info_request{};

		PVOID m_add_owned_explosion_bypass{};

        rage::BlipList* m_blip_list{};
		rage::ScreenResolution* m_screen_resolution{};
		rage::CVehicleDriveByMetadataMgr* m_driveby_metadata_mgr{};
		rage::CVehicleSeatMetadataMgr* m_vehicle_layout_metadata_mgr{};
	};

	inline pointers *g_pointers{};
}
