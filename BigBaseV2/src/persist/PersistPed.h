#pragma once
#include <string>

namespace big
{
	enum persist_ped_flags
	{
		PERSIST_PED_NONE = 0,
		PERSIST_PED_ALL = (1 << 0),
		PERSIST_PED_OUTFIT = (1 << 1),
		PERSIST_PED_HEAD = (1 << 2),
		PERSIST_PED_FACE = (1 << 3),
		PERSIST_PED_PED_DECORATION = (1 << 4),
		PERSIST_PED_MODEL = (1 << 5),
		PERSIST_PED_TATTOOS = (1 << 6),
	};

	class persist_ped
	{
	public:
		static void save_ped(std::string file_name);
		static void load_ped(std::string file_name);
		static void clone_ped(Ped ped, Player player);
		static void ped_outfit_tick(std::string file_name, int persit_flags);
		static std::vector<std::string> list_files();
	private:
		static bool is_ped_showering();
		static bool check_option_flag(int persist_flags, int flag);
		static void set_ped(nlohmann::json outfit_json, int persist_flags);
		static nlohmann::json get_ped_json(Ped ped, Player player);
		static std::filesystem::path check_ped_folder();
		static inline std::unordered_map<std::string, nlohmann::json> outfits;
	};
}
