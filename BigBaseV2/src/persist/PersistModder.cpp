#include "common.hpp"
#include "PersistModder.h"
#include "helpers\DrawHelper.h"
#include "helpers\RageHelper.h"
#include "gui/PlayersTab.h"
#include "helpers/NetworkHelper.h"
#include "CustomCode/Settings.h"

namespace big
{
	bool persist_modder::is_modder(CNetGamePlayer* player_ptr)
	{
		if (player_ptr == nullptr || player_ptr->get_net_data() == nullptr)
			return false;
		return is_modder(player_ptr->get_net_data()->m_rockstar_id, player_ptr->player_id, player_ptr->get_name());
	}

	bool persist_modder::is_banned_from_spawning_objects(CNetGamePlayer* player_ptr)
	{
		if (player_ptr == nullptr)
			return false;
		if (auto net_data = player_ptr->get_net_data())
			return banned_from_objects.count(player_ptr->get_net_data()->m_rockstar_id) != 0;
		return false;
	}

	bool persist_modder::is_modder(uint64_t scid, Player player, const char* name)
	{
		if (modder_list.count(scid) == 0 && scid != g_settings.options["spoof scid_real"])
			check_for_modder(scid, player, name);
		return modder_list[scid] >= 50;
	}

	void persist_modder::add_modder(uint64_t scid, Player player, const char* name, std::string reason, uint64_t infraction_points)
	{
		uint64_t old_value = 0;
		if (modder_list.count(scid) == 0)
		{
			modder_list[scid] = infraction_points;
		}
		else
		{
			old_value = modder_list[scid];
			modder_list[scid] += infraction_points;
		}
		if (modder_list[scid] >= 50)
		{
			persisted_modders[scid] = modder_list[scid];
			save();
			if (old_value < 50)
			{
				draw_helper::draw_headshot_notification(reason, name, player, "Detected Modder", HUD_COLOUR_RED);
				LOG(HACKER) << "Detected Modder " << name << " Reason: " << reason << " SCID: " << scid;
				std::string lowercase_name = name;
				transform(lowercase_name.begin(), lowercase_name.end(), lowercase_name.begin(), ::tolower);
				if (players_tab::playerlist[lowercase_name].color.w == 0)
					players_tab::playerlist[lowercase_name].color = { 1.f, 0.17f, 0.17f, 1.f };
			}
		}
	}

	void persist_modder::add_modder(CNetGamePlayer* player, std::string reason, uint64_t infraction_points)
	{
		if (player == nullptr || player->get_net_data() == nullptr)
			return;
		add_modder(player->get_net_data()->m_rockstar_id, player->player_id, player->get_name(), reason, infraction_points);
	}

	void persist_modder::clean_list(std::set<uint64_t> connected_players)
	{
		for (auto modder : modder_list)
		{
			if (connected_players.find(modder.first) == connected_players.end())
				modder_list.erase(modder.first);
		}
	}

	void persist_modder::ban_from_objects(CNetGamePlayer* player)
	{
		if (player == nullptr)
			return;
		if (auto net_data = player->get_net_data())
		{
			auto scid = net_data->m_rockstar_id;
			banned_from_objects[scid] = 1;
			LOG(HACKER) << net_data->m_name << " was banned from spawning objects.";
			add_modder(player, "Too many entities", 50);
		}
	}

	void persist_modder::load()
	{
		auto path = get_modder_config();
		if (std::filesystem::exists(path))
		{
			std::ifstream file(path);
			nlohmann::json modder_json;
			file >> modder_json;
			file.close();

			persisted_modders = modder_json["Modders"].get<std::unordered_map<uint64_t, uint64_t>>();
		}
	}

	void persist_modder::save()
	{
		auto path = get_modder_config();
		std::ofstream file(path);
		nlohmann::json modder_json;
		modder_json["Modders"] = persisted_modders;
		file << modder_json.dump(4);
		file.close();
	}

	void persist_modder::check_for_modder(uint64_t scid, Player player, const char* name)
	{
		if (persisted_modders.count(scid) != 0)
		{
			modder_list[scid] = persisted_modders[scid];
			draw_helper::draw_headshot_notification("Previous Modder", name, player, "Detected Modder", HUD_COLOUR_RED);
			LOG(HACKER) << "Detected Modder " << name << " Reason: Previous Modder " << " SCID: " << scid;//big::g_logger->log(big::log_color::green | big::log_color::intensify, "Info", "Detected Modder {} Reason: {} SCID: {}", name, , );
		}
		else
		{
			modder_list[scid] = 0;
		}
	}

	std::filesystem::path persist_modder::get_modder_config()
	{
		auto file_path = std::filesystem::path(std::getenv("appdata"));
		file_path /= "BigBaseV2";

		if (!std::filesystem::exists(file_path))
		{
			std::filesystem::create_directory(file_path);
		}
		else if (!std::filesystem::is_directory(file_path))
		{
			std::filesystem::remove(file_path);
			std::filesystem::create_directory(file_path);
		}

		file_path /= "Modders.json";

		return file_path;
	}
}
