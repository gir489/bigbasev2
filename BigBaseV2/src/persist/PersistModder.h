#pragma once
#include "natives.hpp"
#include "gta\player.hpp"
#include <set>

namespace big
{
	class persist_modder
	{
	public:
		bool is_modder(CNetGamePlayer* player_ptr);
		bool is_banned_from_spawning_objects(CNetGamePlayer* player_ptr);
		bool is_modder(uint64_t scid, Player player, const char* name);
		void add_modder(uint64_t scid, Player player, const char* name, std::string reason, uint64_t infraction_points = 50);
		void add_modder(CNetGamePlayer* player, std::string reason, uint64_t infraction_points = 50);
		void clean_list(std::set<uint64_t> connected_players);
		void ban_from_objects(CNetGamePlayer* player);
		void load();
		void save();
	private:
		void check_for_modder(uint64_t scid, Player player, const char* name);
		std::unordered_map<uint64_t, uint64_t> modder_list;
		std::unordered_map<uint64_t, uint64_t> persisted_modders;
		std::unordered_map<uint64_t, uint64_t> banned_from_objects;
		std::filesystem::path get_modder_config();
	};

	inline persist_modder g_persist_modder;
}