#include "common.hpp"
#include "gta\Weapons.h"
#include "PersistWeapons.h"
#include "imgui.h"
#include "gui.hpp"
#include "fiber_pool.hpp"
#include "natives.hpp"
#include "CustomCode\WeaponInfo.h"
#include "CustomCode/PlayerInfo.h"
#include "script.hpp"
#include "CustomCode/Settings.h"

namespace big
{
	void persist_weapons::do_presentation_layer()
	{
		auto loadout_locations = list_weapon_loadouts();
		static std::string selected_loadout;
		ImGui::PushItemWidth(250);
		ImGui::Text("Saved Loadouts");
		if (ImGui::ListBoxHeader("##empty", ImVec2(200, 200)))
		{
			for (auto pair : loadout_locations)
			{
				if (ImGui::Selectable(pair.c_str(), selected_loadout == pair))
					selected_loadout = pair;
			}
			ImGui::ListBoxFooter();
		}
		ImGui::SameLine();
		ImGui::BeginGroup();
		static char loadout_name[50]{};
		ImGui::PushItemWidth(250);
		ImGui::InputText("Loadout Name", loadout_name, IM_ARRAYSIZE(loadout_name));
		if (ImGui::IsItemActive())
			g_gui.m_using_keyboard = true;
		ImGui::SameLine();
		if (ImGui::Button("Save Loadout"))
		{
			QUEUE_JOB_BEGIN_CLAUSE()
			{
				do_save_loadout_gui(loadout_name);
			}
			QUEUE_JOB_END_CLAUSE
		}
		if (ImGui::Button("Load Loadout"))
		{
			QUEUE_JOB_BEGIN_CLAUSE()
			{
				do_load_loadout_gui(selected_loadout);
			}
			QUEUE_JOB_END_CLAUSE
		}
		ImGui::SameLine();
		if (ImGui::Button("Set Loadout"))
		{
			g_settings.options["persist weapons"] = selected_loadout;
			g_settings.save();
		}
		ImGui::SameLine();
		if (ImGui::Button("Disable Loadout"))
		{
			g_settings.options["persist weapons"] = "";
			g_settings.save();
		}
		ImGui::Text(fmt::format("Current Loadout: {}", g_settings.options["persist weapons"]).c_str());
		ImGui::EndGroup();
		ImGui::Separator();
	}

	void persist_weapons::save_weapons(std::string loadout_name)
	{
		Player player = g_local_player.player;
		Ped player_ped = g_local_player.player_ped;
		std::unordered_map<Hash, weaponloadout_json::weaponloadout_json> weapon_json;
		for (auto weapon_info : weapon_info::get_all_weapons())
		{
			Hash weapon = weapon_info.first;
			if (WEAPON::HAS_PED_GOT_WEAPON(player_ped, weapon, FALSE) && weapon != WEAPON_UNARMED)
			{
				weapon_json[weapon].weapon_tint = WEAPON::GET_PED_WEAPON_TINT_INDEX(player_ped, weapon);
				std::vector<Hash> component_array;
				for (auto component : weapon_info.second.attachments)
				{
					if (WEAPON::HAS_PED_GOT_WEAPON_COMPONENT(player_ped, weapon, component.hash))
						component_array.push_back(component.hash);
				}
				weapon_json[weapon].component_array = component_array;
			}
		}
		if (WEAPON::HAS_PED_GOT_WEAPON(player_ped, GADGET_PARACHUTE, FALSE))
		{
			weapon_json[GADGET_PARACHUTE].parachutes = 1;
			int weapon_tint, parachute_pack_tint, reserve_pack_tint, smoke_trail[3]{};
			bool hasReserve = PLAYER::GET_PLAYER_HAS_RESERVE_PARACHUTE(player);
			PLAYER::GET_PLAYER_PARACHUTE_TINT_INDEX(player, &weapon_tint);
			PLAYER::GET_PLAYER_PARACHUTE_PACK_TINT_INDEX(player, &parachute_pack_tint);
			weapon_json[GADGET_PARACHUTE].parachute_pack_tint = parachute_pack_tint;
			weapon_json[GADGET_PARACHUTE].weapon_tint = weapon_tint;
			if (hasReserve == true)
			{
				weapon_json[GADGET_PARACHUTE].parachutes = 2;
				PLAYER::GET_PLAYER_RESERVE_PARACHUTE_TINT_INDEX(player, &reserve_pack_tint);
				weapon_json[GADGET_PARACHUTE].reserve_pack_tint = reserve_pack_tint;
			}
			PLAYER::GET_PLAYER_PARACHUTE_SMOKE_TRAIL_COLOR(player, &smoke_trail[0], &smoke_trail[1], &smoke_trail[2]);
			if (!(smoke_trail[0] == 255 && smoke_trail[1] == 255 && smoke_trail[2] == 255))
				weapon_json[GADGET_PARACHUTE].smoke_trail = std::vector<int>(std::begin(smoke_trail), std::end(smoke_trail));
		}

		auto file_path = get_weapon_config();
		file_path /= loadout_name + ".json";
		std::ofstream file(file_path, std::ios::out | std::ios::trunc);
		nlohmann::json json;
		json = weapon_json;
		file << json.dump(4);
		file.close();
	}

	void persist_weapons::load_weapons(std::string selected_loadout)
	{
		auto file_path = get_weapon_config();
		file_path /= selected_loadout;
		std::ifstream file(file_path);

		if (!file.fail())
		{
			std::unordered_map<Hash, weaponloadout_json::weaponloadout_json> weapon_json;
			nlohmann::json json;
			file >> json;
			weapon_json = json.get_to(weapon_json);
			WEAPON::REMOVE_ALL_PED_WEAPONS(g_local_player.player_ped, TRUE);
			for (auto weapon_iter : weapon_json)
				give_weapon_from_json(weapon_iter.first, weapon_json);
		}
		else
		{
			LOG(WARNING) << "load_weapons can not open " << selected_loadout;
		}
	}

	std::filesystem::path persist_weapons::get_weapon_config()
	{
		auto file_path = std::filesystem::path(std::getenv("appdata"));
		file_path /= "BigBaseV2";
		file_path /= "WeaponLoadouts";

		if (!std::filesystem::exists(file_path))
		{
			std::filesystem::create_directory(file_path);
		}
		else if (!std::filesystem::is_directory(file_path))
		{
			std::filesystem::remove(file_path);
			std::filesystem::create_directory(file_path);
		}

		return file_path;
	}

	std::vector<std::string> persist_weapons::list_weapon_loadouts()
	{
		auto file_path = get_weapon_config();
		std::vector<std::string> return_value;
		for (const auto& p : std::filesystem::directory_iterator(file_path))
			if (p.path().extension() == ".json")
				return_value.push_back(p.path().filename().generic_string());
		return return_value;
	}

	void persist_weapons::do_save_loadout_gui(char* loadout_name)
	{
		save_weapons(loadout_name);
		ZeroMemory(loadout_name, sizeof(loadout_name));
	}

	void persist_weapons::do_load_loadout_gui(std::string& selected_loadout)
	{
		if (!selected_loadout.empty())
		{
			load_weapons(selected_loadout);
			selected_loadout.clear();
		}
	}

	void persist_weapons::check_player_has_weapons(std::string selected_loadout)
	{
		if (g_local_player.is_cutscene_playing || g_local_player.is_player_switch_in_progress || g_local_player.is_loading_screen_active)
			return;
		if (weapon_jsons.find(selected_loadout) == weapon_jsons.end())
		{
			auto file_path = get_weapon_config();
			file_path /= selected_loadout;
			std::ifstream file(file_path);
			nlohmann::json json_read;
			if (!file.fail())
				file >> json_read;
			else
				LOG(WARNING) << "check_player_has_weapons can not open " << selected_loadout;
			weapon_jsons[selected_loadout] = json_read;
		}
		nlohmann::json json = weapon_jsons[selected_loadout];
		if (json.empty())
			return;
		std::unordered_map<Hash, weaponloadout_json::weaponloadout_json> weapon_json;
		weapon_json = json.get_to(weapon_json);
		for (auto weapon_iter : weapon_json)
		{
			Hash weapon = weapon_iter.first;
			bool is_gun = (GADGET_PARACHUTE != weapon && weapon_info::get_weapon(weapon).group != GROUP_MELEE);
			if (WEAPON::HAS_PED_GOT_WEAPON(g_local_player.player_ped, weapon, FALSE) == false ||
				(is_gun && (WEAPON::GET_PED_WEAPON_TINT_INDEX(g_local_player.player_ped, weapon) != weapon_iter.second.weapon_tint ||
					(WEAPON::GET_AMMO_IN_PED_WEAPON(g_local_player.player_ped, weapon) == 0))))
				give_weapon_from_json(weapon, weapon_json);
		}
	}

	void persist_weapons::give_weapon_from_json(Hash weapon, std::unordered_map<Hash, weaponloadout_json::weaponloadout_json> weapon_json)
	{
		int maxAmmo;
		Player player = g_local_player.player;
		Ped player_ped = g_local_player.player_ped;
		WEAPON::GIVE_WEAPON_TO_PED(player_ped, weapon, (WEAPON::GET_MAX_AMMO(player_ped, weapon, &maxAmmo) == TRUE) ? maxAmmo : 9999, FALSE, FALSE);
		if (weapon != GADGET_PARACHUTE)
		{
			WEAPON::SET_PED_WEAPON_TINT_INDEX(player_ped, weapon, weapon_json[weapon].weapon_tint);
			auto all_attachments = weapon_info::get_weapon_attachments(weapon);
			for (auto attachment : all_attachments)
				WEAPON::REMOVE_WEAPON_COMPONENT_FROM_PED(player_ped, weapon, attachment.hash);
			auto component_array = weapon_json[weapon].component_array;
			for (Hash component : component_array)
				WEAPON::GIVE_WEAPON_COMPONENT_TO_PED(player_ped, weapon, component);
			WEAPON::GIVE_WEAPON_TO_PED(player_ped, weapon, (WEAPON::GET_MAX_AMMO(player_ped, weapon, &maxAmmo) == TRUE) ? maxAmmo : 9999, FALSE, FALSE);
		}
		else
		{
			PLAYER::SET_PLAYER_PARACHUTE_TINT_INDEX(player, weapon_json[GADGET_PARACHUTE].weapon_tint);
			PLAYER::SET_PLAYER_PARACHUTE_PACK_TINT_INDEX(player, weapon_json[GADGET_PARACHUTE].parachute_pack_tint);
			if (weapon_json[GADGET_PARACHUTE].parachutes == 2)
			{
				PLAYER::SET_PLAYER_HAS_RESERVE_PARACHUTE(player);
				PLAYER::SET_PLAYER_RESERVE_PARACHUTE_TINT_INDEX(player, weapon_json[GADGET_PARACHUTE].reserve_pack_tint);
			}
			if (!weapon_json[GADGET_PARACHUTE].smoke_trail.empty())
			{
				PLAYER::SET_PLAYER_CAN_LEAVE_PARACHUTE_SMOKE_TRAIL(player, TRUE);
				auto smoke_trail = weapon_json[GADGET_PARACHUTE].smoke_trail;
				PLAYER::SET_PLAYER_PARACHUTE_SMOKE_TRAIL_COLOR(player, smoke_trail[0], smoke_trail[1], smoke_trail[2]);
			}
		}
	}
}