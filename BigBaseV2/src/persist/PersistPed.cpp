#include "common.hpp"
#include "script.hpp"
#include "pointers.hpp"
#include "gta_util.hpp"
#include "PedData.h"
#include "PersistPed.h"
#include "gta\decal_controller.hpp"
#include "script_global.hpp"
#include "CustomCode/PlayerInfo.h"

namespace big
{
	void persist_ped::save_ped(std::string file_name)
	{
		if (ENTITY::DOES_ENTITY_EXIST(g_local_player.player_ped) == FALSE || ENTITY::IS_ENTITY_DEAD(g_local_player.player_ped, 0))
		{
			LOG(WARNING) << "Player is not available to have their outfit saved.";//LOG_ERROR("Player is not available to have their outfit saved.");
			return;
		}

		auto file_path = check_ped_folder();
		file_path /= file_name;
		std::ofstream file(file_path, std::ios::out | std::ios::trunc);
		auto outfit_json = get_ped_json(g_local_player.player_ped, g_local_player.player);
		file << outfit_json.dump(4);
		outfits[file_name] = outfit_json;
		file.close();
	}

	void persist_ped::load_ped(std::string file_name)
	{
		auto file_path = check_ped_folder();
		file_path /= file_name;
		std::ifstream file(file_path);

		nlohmann::json outfit_json;
		file >> outfit_json;
		file.close();
		outfits[file_name] = outfit_json;

		set_ped(outfit_json, PERSIST_PED_ALL);
	}

	void persist_ped::clone_ped(Ped ped, Player player)
	{
		if (ENTITY::DOES_ENTITY_EXIST(ped) == FALSE || ENTITY::IS_ENTITY_DEAD(ped, 0))
			return;
		set_ped(get_ped_json(ped, player), PERSIST_PED_ALL);
	}

	bool persist_ped::is_ped_showering()
	{
		return PED::GET_PED_DRAWABLE_VARIATION(g_local_player.player_ped, 5) == 0 && 
			   PED::GET_PED_DRAWABLE_VARIATION(g_local_player.player_ped, 4) >= 14 && 
			   PED::GET_PED_DRAWABLE_VARIATION(g_local_player.player_ped, 4) <= 18;
	}

	void persist_ped::ped_outfit_tick(std::string file_name, int persit_flags)
	{
		if (is_ped_showering())
			return;
		auto found_object = outfits.find(file_name);
		nlohmann::json outfit_json;
		if (found_object == outfits.end())
		{
			auto file_path = check_ped_folder();
			file_path /= file_name;
			std::ifstream file(file_path);

			file >> outfit_json;

			outfits[file_name] = outfit_json;
		}
		else
		{
			outfit_json = found_object->second;
		}
		set_ped(outfit_json, persit_flags);
	}

	std::vector<std::string> persist_ped::list_files()
	{
		auto file_path = check_ped_folder();
		std::vector<std::string> return_value;
		for (const auto& p : std::filesystem::directory_iterator(file_path))
			if (p.path().extension() == ".json")
				return_value.push_back(p.path().filename().generic_string());
		return return_value;
	}

	bool persist_ped::check_option_flag(int persist_flags, int flag)
	{
		return (persist_flags & (PERSIST_PED_ALL | flag));
	}

	void persist_ped::set_ped(nlohmann::json outfit_json, int persist_flags)
	{
		Ped ped = g_local_player.player_ped;
		Hash model = outfit_json["Model"];
		if (ENTITY::GET_ENTITY_MODEL(ped) != model)
		{
			if (!check_option_flag(persist_flags, PERSIST_PED_MODEL))
				return;
			while (!STREAMING::HAS_MODEL_LOADED(model))
			{
				STREAMING::REQUEST_MODEL(model);
				script::get_current()->yield();
			}

			PLAYER::SET_PLAYER_MODEL(g_local_player.player, model);

			ped = PLAYER::PLAYER_PED_ID();

			g_local_player.player = -1;
			g_local_player.player_ped = 0;

			PED::SET_PED_DEFAULT_COMPONENT_VARIATION(model);
			script::get_current()->yield();
		}
		else if (persist_flags == PERSIST_PED_ALL)
		{
			PED::CLEAR_ALL_PED_PROPS(ped);
			PED::CLEAR_PED_DECORATIONS(ped);
			//PED::CLEAR_PED_DECORATIONS_LEAVE_SCARS(ped);
		}
		bool was_components_set = true;
		if (check_option_flag(persist_flags, PERSIST_PED_OUTFIT))
		{
			std::vector<int> component_drawable_variation = outfit_json["Component Drawable Variation"];
			std::vector<int> component_texture_variation = outfit_json["Component Texture Variation"];
			for (int i = 0; i < 12; i++)
			{
				if (PED::GET_PED_DRAWABLE_VARIATION(ped, i) != component_drawable_variation[i] || PED::GET_PED_TEXTURE_VARIATION(ped, i) != component_texture_variation[i])
				{
					if (i == 5) //Skip parachute.
						continue;
					PED::SET_PED_COMPONENT_VARIATION(ped, i, component_drawable_variation[i], component_texture_variation[i], 0);
				}
				if (PED::GET_PED_DRAWABLE_VARIATION(ped, i) != component_drawable_variation[i] || PED::GET_PED_TEXTURE_VARIATION(ped, i) != component_texture_variation[i])
					was_components_set = false;
			}
		}
		if (check_option_flag(persist_flags, PERSIST_PED_PED_DECORATION))
		{
			std::vector<int> ped_prop_index = outfit_json["Ped Prop Index"];
			std::vector<int> ped_prop_texture_index = outfit_json["Ped Prop Texture Index"];
			for (int i = 0; i < 12; i++)
			{
				if (ped_prop_index[i] == -1 && PED::GET_PED_PROP_INDEX(ped, i) != -1)
				{
					PED::CLEAR_PED_PROP(ped, i);
				}
				else
				{
					if (PED::GET_PED_PROP_INDEX(ped, i) != ped_prop_index[i])
					{
						PED::SET_PED_PROP_INDEX(ped, i, ped_prop_index[i], ped_prop_texture_index[i], NETWORK::NETWORK_IS_GAME_IN_PROGRESS());
						if (i == 0)
						{
							PED::SET_PED_HELMET_PROP_INDEX(ped, ped_prop_index[i], 0);
							PED::SET_PED_HELMET_TEXTURE_INDEX(ped, ped_prop_texture_index[i]);
							PED::SET_PED_CONFIG_FLAG(ped, 34, true);
							if (!PED::IS_PED_ON_ANY_BIKE(ped))
								PED::SET_PED_CONFIG_FLAG(ped, 36, true);
						}
					}
				}
			}
		}
		if (!outfit_json["Ped Data"].is_null())
		{
			peddata::ped_data ped_data = outfit_json["Ped Data"];
			if (check_option_flag(persist_flags, PERSIST_PED_FACE) && was_components_set)
			{
				PED::SET_PED_HEAD_BLEND_DATA(ped, ped_data.first_face_shape, ped_data.second_face_shape, ped_data.third_face_shape,
					ped_data.first_skin_tone, ped_data.second_skin_tone, ped_data.third_skin_tone,
					ped_data.parent_face_shape_percent, ped_data.parent_skin_tone_percent, ped_data.parent_third_unk_percent,
					ped_data.is_parent_inheritance);
				for (int i = 0; i < 20; i++)
					PED::_SET_PED_FACE_FEATURE(ped, i, ped_data.face_feature_scale[i]);
			}
			if (check_option_flag(persist_flags, PERSIST_PED_HEAD))
			{
				PED::_SET_PED_EYE_COLOR(ped, ped_data.eye_color);
				PED::_SET_PED_HAIR_COLOR(ped, ped_data.hair_color, ped_data.hair_highlight);
				for (int i = 0; i < 13; i++)
				{
					PED::SET_PED_HEAD_OVERLAY(ped, i, ped_data.overlay[i], ped_data.overlay_opacity[i]);
					PED::_SET_PED_HEAD_OVERLAY_COLOR(ped, i, ped_data.overlay_color_type[i], ped_data.overlay_color_id[i], ped_data.overlay_second_color_id[i]);
				}
			}
		}
		if (check_option_flag(persist_flags, PERSIST_PED_TATTOOS))
		{
			std::vector<Hash> collection_vector = outfit_json["Collection Vector"];
			std::vector<Hash> overlay_vector = outfit_json["Overlay Vector"];
			for (int i = 0; i < collection_vector.size(); i++)
				PED::ADD_PED_DECORATION_FROM_HASHES(ped, collection_vector[i], overlay_vector[i]);
		}
	}

	nlohmann::json persist_ped::get_ped_json(Ped ped, Player player)
	{
		std::vector<int> component_drawable_variation;
		std::vector<int> component_texture_variation;
		for (int i = 0; i < 12; i++)
		{
			component_drawable_variation.push_back(PED::GET_PED_DRAWABLE_VARIATION(ped, i));
			component_texture_variation.push_back(PED::GET_PED_TEXTURE_VARIATION(ped, i));
		}
		std::vector<int> ped_prop_index;
		std::vector<int> ped_prop_texture_index;
		for (int i = 0; i < 12; i++)
		{
			ped_prop_index.push_back(PED::GET_PED_PROP_INDEX(ped, i));
			ped_prop_texture_index.push_back(PED::GET_PED_PROP_TEXTURE_INDEX(ped, i));
		}

		nlohmann::json outfit_json;
		outfit_json["Model"] = ENTITY::GET_ENTITY_MODEL(ped);
		outfit_json["Component Drawable Variation"] = component_drawable_variation;
		outfit_json["Component Texture Variation"] = component_texture_variation;
		outfit_json["Ped Prop Index"] = ped_prop_index;
		outfit_json["Ped Prop Texture Index"] = ped_prop_texture_index;

		if (*g_pointers->m_is_session_started)
			outfit_json["Ped Data"] = *g_pointers->m_get_ped_data(player);

		rage::CPed* ped_ptr = gta_util::get_player_ped(player);
		std::vector<Hash> collection_vector;
		std::vector<Hash> overlay_vector;
		if (ped_ptr != nullptr && ped_ptr->m_decal_index != -1)
		{
			if (auto decal_controller = *g_pointers->m_decal_controller)
			{
				if (g_local_player.ped_ptr->m_decal_index != -1)
				{
					auto decal_list = decal_controller->ped_decal_list[g_local_player.ped_ptr->m_decal_index];
					decal_list->clear_non_perm_decals_from_decal_array();
					if (!decal_list->decal_array.size())
						decal_list->refresh_list(0);
					auto decal_array = decal_list->decal_array;
					for (auto decal : decal_array)
					{
						if (decal.overlay != 0x0 && decal.collection != 0x0)
						{
							collection_vector.push_back(decal.collection);
							overlay_vector.push_back(decal.overlay);
						}
					}
				}
			}
		}
		outfit_json["Collection Vector"] = collection_vector;
		outfit_json["Overlay Vector"] = overlay_vector;

		return outfit_json;
	}

	std::filesystem::path persist_ped::check_ped_folder()
	{
		auto file_path = std::filesystem::path(std::getenv("appdata"));
		file_path /= "BigBaseV2";
		file_path /= "Peds";

		if (!std::filesystem::exists(file_path))
		{
			std::filesystem::create_directory(file_path);
		}
		else if (!std::filesystem::is_directory(file_path))
		{
			std::filesystem::remove(file_path);
			std::filesystem::create_directory(file_path);
		}

		return file_path;
	}
}