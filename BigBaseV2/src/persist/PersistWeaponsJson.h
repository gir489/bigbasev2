#pragma once

#include "natives.hpp"

namespace weaponloadout_json
{
	struct weaponloadout_json
	{
		std::vector<Hash> component_array;
		int weapon_tint;
		int parachutes;
		int parachute_pack_tint;
		int reserve_pack_tint;
		std::vector<int> smoke_trail;
	};

	static void to_json(nlohmann::json& j, const weaponloadout_json& weapon) {
		if (weapon.parachutes != 0)
			j = nlohmann::json{ {"Component Array", weapon.component_array}, {"Weapon Tint", weapon.weapon_tint}, {"Parachutes", weapon.parachutes}, {"Smoke Trail", weapon.smoke_trail},
								{"Parachute Pack Tint", weapon.parachute_pack_tint}, {"Reserve Pack Tint", weapon.reserve_pack_tint} };
		else
			j = nlohmann::json{ {"Component Array", weapon.component_array}, {"Weapon Tint", weapon.weapon_tint} };
	}
	static void from_json(const nlohmann::json& j, weaponloadout_json& weapon) {
		j.at("Component Array").get_to(weapon.component_array); j.at("Weapon Tint").get_to(weapon.weapon_tint);
		if (j.contains("Parachutes")) {
			j.at("Parachutes").get_to(weapon.parachutes); j.at("Smoke Trail").get_to(weapon.smoke_trail);
			j.at("Parachute Pack Tint").get_to(weapon.parachute_pack_tint); j.at("Reserve Pack Tint").get_to(weapon.reserve_pack_tint);
		}
	}
}