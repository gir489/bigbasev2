#pragma once

#include "natives.hpp"

namespace peddata
{
	class ped_data
	{
	public:
		char pad_0x0000[0x28]; //0x0000
		float parent_face_shape_percent; //0x0028
		float parent_skin_tone_percent; //0x002C
		float parent_third_unk_percent; //0x0030
		char pad_0x0034[0x4]; //0x0034
		float overlay_opacity[13]; //0x0038
		char pad_0x006C[0x34]; //0x006C
		float face_feature_scale[20]; //0x00A0
		int8_t overlay_color_id[13]; //0x00F0
		int8_t overlay_second_color_id[13]; //0x00FD
		int8_t overlay_color_type[13]; //0x010A
		int8_t first_face_shape; //0x0117
		int8_t second_face_shape; //0x0118
		int8_t third_face_shape; //0x0119
		int8_t first_skin_tone; //0x011A
		int8_t second_skin_tone; //0x011B
		int8_t third_skin_tone; //0x011C
		int8_t overlay[13]; //0x011D
		char pad_0x012A[0xC]; //0x012A
		int8_t eye_color; //0x0136
		char pad_0x0137[0x1]; //0x0137
		int8_t hair_color; //0x0138
		int8_t hair_highlight; //0x0139
		char pad_0x013A[0x3]; //0x013A
		int8_t is_parent_inheritance; //0x013D
	};

	static void to_json(nlohmann::json& j, const ped_data& blend_data) {
		j = nlohmann::json{ {"first_face_shape", blend_data.first_face_shape}, {"second_face_shape", blend_data.second_face_shape}, {"third_face_shape", blend_data.third_face_shape},
							{"first_skin_tone", blend_data.first_skin_tone}, {"second_skin_tone", blend_data.second_skin_tone}, {"third_skin_tone", blend_data.third_skin_tone},
							{"parent_face_shape_percent", blend_data.parent_face_shape_percent}, {"parent_skin_tone_percent", blend_data.parent_skin_tone_percent}, {"parent_third_unk_percent", blend_data.parent_third_unk_percent},
							{"is_parent_inheritance", blend_data.is_parent_inheritance},
							{"overlay", blend_data.overlay}, {"overlay_opacity", blend_data.overlay_opacity},
							{"overlay_color_id", blend_data.overlay_color_id}, {"overlay_second_color_id", blend_data.overlay_second_color_id}, {"overlay_color_type", blend_data.overlay_color_type},
							{"hair_color", blend_data.hair_color}, {"hair_highlight", blend_data.hair_highlight},
							{"eye_color", blend_data.eye_color},
							{"face_feature_scale", blend_data.face_feature_scale} };
	}

	static void from_json(const nlohmann::json& j, ped_data& blend_data) {
		j.at("first_face_shape").get_to(blend_data.first_face_shape); j.at("second_face_shape").get_to(blend_data.second_face_shape); j.at("third_face_shape").get_to(blend_data.third_face_shape);
		j.at("first_skin_tone").get_to(blend_data.first_skin_tone); j.at("second_skin_tone").get_to(blend_data.second_skin_tone); j.at("third_skin_tone").get_to(blend_data.third_skin_tone);
		j.at("parent_face_shape_percent").get_to(blend_data.parent_face_shape_percent); j.at("parent_skin_tone_percent").get_to(blend_data.parent_skin_tone_percent); j.at("parent_third_unk_percent").get_to(blend_data.parent_third_unk_percent);
		j.at("is_parent_inheritance").get_to(blend_data.is_parent_inheritance);
		std::vector<int8_t> overlay_vector = j.at("overlay"); std::copy(overlay_vector.begin(), overlay_vector.end(), blend_data.overlay);
		std::vector<float> overlay_opacity_vector = j.at("overlay_opacity"); std::copy(overlay_opacity_vector.begin(), overlay_opacity_vector.end(), blend_data.overlay_opacity);
		std::vector<float> face_feature_scale_vector = j.at("face_feature_scale"); std::copy(face_feature_scale_vector.begin(), face_feature_scale_vector.end(), blend_data.face_feature_scale);
		std::vector<int8_t> overlay_color_id_vector = j.at("overlay_color_id"); std::copy(overlay_color_id_vector.begin(), overlay_color_id_vector.end(), blend_data.overlay_color_id);
		std::vector<int8_t> overlay_second_color_id_vector = j.at("overlay_second_color_id"); std::copy(overlay_second_color_id_vector.begin(), overlay_second_color_id_vector.end(), blend_data.overlay_second_color_id);
		std::vector<int8_t> overlay_color_type_vector = j.at("overlay_color_type"); std::copy(overlay_color_type_vector.begin(), overlay_color_type_vector.end(), blend_data.overlay_color_type);
		j.at("hair_color").get_to(blend_data.hair_color); j.at("hair_highlight").get_to(blend_data.hair_highlight);
		j.at("eye_color").get_to(blend_data.eye_color);
	}
}