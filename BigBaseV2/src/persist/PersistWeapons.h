#pragma once

#include "PersistWeaponsJson.h"

namespace big
{
    class persist_weapons
    {
    public:
		static void do_presentation_layer();
		static void check_player_has_weapons(std::string selected_loadout);
    private:
		static void save_weapons(std::string loadout_name);
		static void load_weapons(std::string selected_loadout);
		static std::filesystem::path get_weapon_config();
		static std::vector<std::string> list_weapon_loadouts();
		static void do_save_loadout_gui(char* loadout_name);
		static void do_load_loadout_gui(std::string& selected_loadout);
		static void give_weapon_from_json(Hash weapon, std::unordered_map<Hash, weaponloadout_json::weaponloadout_json> weapon_json);

		static inline std::unordered_map<std::string, nlohmann::json> weapon_jsons;
    };
}
