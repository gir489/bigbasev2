#include "common.hpp"
#include "gui.hpp"
#include "imgui.h"
#include "script_global.hpp"
#include "gta\enums.hpp"
#include "script.hpp"
#include "natives.hpp"

#include "gui\SelfTab.h"
#include "gui\VehicleTab.h"
#include "gui\LSCTab.h"
#include "gui\OnlineTab.h"
#include "gui\PlayersTab.h"
#include "gui\WorldTab.h"
#include "gui\WeaponsTab.h"
#include "gui\GamerMonitor.h"
#include "gui\SettingsTab.h"

#include "pointers.hpp"
#include "persist/PersistModder.h"
#include "CustomCode/PlayerInfo.h"
#include "gta_util.hpp"
#include "CustomCode/Settings.h"
#include "helpers/RageHelper.h"

namespace big
{
	void gui::dx_init()
	{
		auto& style = ImGui::GetStyle();
		style.WindowPadding = { 10.f, 10.f };
		style.PopupRounding = 6.f;
		style.FramePadding = { 8.f, 4.f };
		style.ItemSpacing = { 10.f, 8.f };
		style.ItemInnerSpacing = { 6.f, 6.f };
		style.TouchExtraPadding = { 0.f, 0.f };
		style.IndentSpacing = 21.f;
		style.ScrollbarSize = 15.f;
		style.GrabMinSize = 8.f;
		style.WindowBorderSize = 1.f;
		style.ChildBorderSize = 0.f;
		style.PopupBorderSize = 1.f;
		style.FrameBorderSize = 0.f;
		style.TabBorderSize = 0.f;
		style.WindowRounding = 6.f;
		style.ChildRounding = 6.f;
		style.FrameRounding = 6.f;
		style.ScrollbarRounding = 6.f;
		style.GrabRounding = 6.f;
		style.TabRounding = 6.f;
		style.PopupRounding = 6.f;
		style.WindowTitleAlign = { 0.5f, 0.5f };
		style.ButtonTextAlign = { 0.5f, 0.5f };
		style.DisplaySafeAreaPadding = { 3.f, 3.f };

		auto& colors = style.Colors;
		colors[ImGuiCol_Text] = ImVec4(1.00f, 1.00f, 1.00f, 1.00f);
		colors[ImGuiCol_TextDisabled] = ImVec4(0.39f, 0.39f, 0.39f, 1.00f);
		colors[ImGuiCol_WindowBg] = ImVec4(0.07f, 0.04f, 0.10f, 1.00f);
		colors[ImGuiCol_ChildBg] = ImVec4(0.00f, 0.00f, 0.00f, 0.00f);
		colors[ImGuiCol_PopupBg] = ImVec4(0.09f, 0.06f, 0.12f, 1.00f);
		colors[ImGuiCol_Border] = ImVec4(0.30f, 0.30f, 0.30f, 0.50f);
		colors[ImGuiCol_BorderShadow] = ImVec4(0.00f, 0.00f, 0.00f, 0.00f);
		colors[ImGuiCol_FrameBg] = ImVec4(0.16f, 0.18f, 0.48f, 0.8f);
		colors[ImGuiCol_FrameBgHovered] = ImVec4(0.16f, 0.50f, 0.71f, 0.8f);
		colors[ImGuiCol_FrameBgActive] = ImVec4(0.16f, 0.50f, 0.71f, 1.0f);
		colors[ImGuiCol_TitleBg] = ImVec4(0.14f, 0.16f, 0.46f, 1.0f);
		colors[ImGuiCol_TitleBgActive] = ImVec4(0.16f, 0.18f, 0.48f, 1.0f);
		colors[ImGuiCol_TitleBgCollapsed] = ImVec4(0.14f, 0.16f, 0.46f, 0.5f);
		colors[ImGuiCol_MenuBarBg] = ImVec4(0.16f, 0.18f, 0.48f, 0.8f);
		colors[ImGuiCol_ScrollbarBg] = ImVec4(0.02f, 0.02f, 0.02f, 0.53f);
		colors[ImGuiCol_ScrollbarGrab] = ImVec4(0.16f, 0.50f, 0.71f, 0.6f);
		colors[ImGuiCol_ScrollbarGrabHovered] = ImVec4(0.16f, 0.50f, 0.71f, 0.8f);
		colors[ImGuiCol_ScrollbarGrabActive] = ImVec4(0.16f, 0.50f, 0.71f, 1.0f);
		colors[ImGuiCol_CheckMark] = ImVec4(1.00f, 1.00f, 1.00f, 1.00f);
		colors[ImGuiCol_SliderGrab] = ImVec4(0.02f, 0.02f, 0.02f, 0.63f);
		colors[ImGuiCol_SliderGrabActive] = ImVec4(0.02f, 0.02f, 0.02f, 0.73f);
		colors[ImGuiCol_Button] = ImVec4(0.16f, 0.18f, 0.48f, 0.8f);
		colors[ImGuiCol_ButtonHovered] = ImVec4(0.16f, 0.50f, 0.71f, 0.8f);
		colors[ImGuiCol_ButtonActive] = ImVec4(0.16f, 0.50f, 0.71f, 1.0f);
		colors[ImGuiCol_Header] = ImVec4(0.16f, 0.50f, 0.71f, 0.8f);
		colors[ImGuiCol_HeaderHovered] = ImVec4(0.16f, 0.50f, 0.71f, 0.9f);
		colors[ImGuiCol_HeaderActive] = ImVec4(0.16f, 0.50f, 0.71f, 1.0f);
		colors[ImGuiCol_Separator] = ImVec4(0.38f, 0.38f, 0.38f, 0.50f);
		colors[ImGuiCol_SeparatorHovered] = ImVec4(0.46f, 0.46f, 0.46f, 0.50f);
		colors[ImGuiCol_SeparatorActive] = ImVec4(0.46f, 0.46f, 0.46f, 0.64f);
		colors[ImGuiCol_ResizeGrip] = ImVec4(0.16f, 0.18f, 0.48f, 0.8f);
		colors[ImGuiCol_ResizeGripHovered] = ImVec4(0.16f, 0.50f, 0.71f, 0.8f);
		colors[ImGuiCol_ResizeGripActive] = ImVec4(0.16f, 0.50f, 0.71f, 1.0f);
		colors[ImGuiCol_Tab] = ImVec4(0.16f, 0.18f, 0.48f, 0.8f);
		colors[ImGuiCol_TabHovered] = ImVec4(0.16f, 0.50f, 0.71f, 0.8f);
		colors[ImGuiCol_TabActive] = ImVec4(0.16f, 0.50f, 0.71f, 1.0f);
		colors[ImGuiCol_TabUnfocused] = ImVec4(0.10f, 0.10f, 0.10f, 0.97f);
		colors[ImGuiCol_TabUnfocusedActive] = ImVec4(0.15f, 0.15f, 0.15f, 1.00f);
		colors[ImGuiCol_PlotLines] = ImVec4(0.61f, 0.61f, 0.61f, 1.00f);
		colors[ImGuiCol_PlotLinesHovered] = ImVec4(1.00f, 0.43f, 0.35f, 1.00f);
		colors[ImGuiCol_PlotHistogram] = ImVec4(0.90f, 0.70f, 0.00f, 1.00f);
		colors[ImGuiCol_PlotHistogramHovered] = ImVec4(1.00f, 0.60f, 0.00f, 1.00f);
		colors[ImGuiCol_TextSelectedBg] = ImVec4(0.26f, 0.59f, 0.98f, 0.35f);
		colors[ImGuiCol_DragDropTarget] = ImVec4(1.00f, 1.00f, 0.00f, 0.90f);
		colors[ImGuiCol_NavHighlight] = ImVec4(0.26f, 0.59f, 0.98f, 1.00f);
		colors[ImGuiCol_NavWindowingHighlight] = ImVec4(1.00f, 1.00f, 1.00f, 0.70f);
		colors[ImGuiCol_NavWindowingDimBg] = ImVec4(0.80f, 0.80f, 0.80f, 0.20f);
		colors[ImGuiCol_ModalWindowDimBg] = ImVec4(0.80f, 0.80f, 0.80f, 0.35f);
	}

	void gui::dx_on_tick()
	{
		TRY_CLAUSE
		{
			if (ImGui::Begin("BigBaseV2", NULL, ImGuiWindowFlags_AlwaysAutoResize))
			{
				ImGui::BeginTabBar("tabbar");
				self_tab::render_self_tab();
				vehicle_tab::render_vehicle_tab();
				lsc_tab::render_lsc_tab();
				online_tab::render_online_tab();
				players_tab::render_players_tab();
				world_tab::render_world_tab();
				weapons_tab::render_weapons_tab();
				//gamer_monitor::render_gamer_monitor();
				settings_tab::render_settings_tab();
				ImGui::EndTabBar();
			}
			ImGui::End();
		}
		EXCEPT_CLAUSE
	}

	void gui::esp_on_tick()
	{
		if (g_local_player.is_loading_screen_active || g_local_player.is_player_switch_in_progress || g_local_player.is_cutscene_playing || 
			g_pointers->m_get_player_ped_address == nullptr || g_local_player.player == -1)
				return;
		TRY_CLAUSE
		{
			do_esp();
		}
		EXCEPT_CLAUSE
	}

	void gui::do_esp()
	{
		ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
		ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, { 0.0f, 0.0f });
		ImGui::PushStyleColor(ImGuiCol_WindowBg, { 0.0f, 0.0f, 0.0f, 0.0f });
		ImGui::Begin("##esp", nullptr, ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoInputs);

		ImGui::SetWindowPos(ImVec2(0, 0), ImGuiCond_Always);
		ImGui::SetWindowSize(ImVec2((float)g_pointers->m_screen_resolution->w, (float)g_pointers->m_screen_resolution->h), ImGuiCond_Always);
		for (int i = 0; i < MAX_PLAYERS; i++)
		{
			if (auto ped = gta_util::get_player_ped(i))
			{
				if (ped == g_local_player.ped_ptr || ped == nullptr || ped->m_player_info == nullptr)
					continue;

				std::string lowercaseName = ped->m_player_info->m_name;
				transform(lowercaseName.begin(), lowercaseName.end(), lowercaseName.begin(), ::tolower);
				ImColor color = players_tab::playerlist[lowercaseName].color;
				if (color.Value.w == 0)
					color = ImColor(255, 255, 0, 255);

				float x{}, y{};
				if (ped->m_navigation != nullptr &&
					(((ped->m_ped_state & PED_IS_IN_ANY_VEHICLE) && (ped->m_last_vehicle2 != g_local_player.vehicle_ptr)) || !(ped->m_ped_state & PED_IS_IN_ANY_VEHICLE)) &&
					g_pointers->m_world_to_scren(ped->m_navigation->m_position, &x, &y))
				{
					ImVec2 text_size = ImGui::GetFont()->CalcTextSizeA(ImGui::GetFontSize(), FLT_MAX, 0.0f, ped->m_player_info->m_name, 0, NULL);
					ImGui::GetWindowDrawList()->AddText(ImGui::GetFont(), ImGui::GetFontSize(), ImVec2(x * g_pointers->m_screen_resolution->w - (text_size.x / 2), y * g_pointers->m_screen_resolution->h), color, ped->m_player_info->m_name, 0, 0.0f, 0);
				}

			}
		}

		ImGui::GetWindowDrawList()->PushClipRectFullScreen();

		ImGui::End();
		ImGui::PopStyleColor();
		ImGui::PopStyleVar(2);
	}

	void gui::script_on_tick()
	{
		TRY_CLAUSE
		{
			players_tab::run_tick();
			lsc_tab::run_tick();

			if (g_local_player.is_loading_screen_active || g_local_player.is_player_switch_in_progress || g_local_player.is_cutscene_playing)
				return;

			PAD::DISABLE_CONTROL_ACTION(0, INPUT_MAP_POI, true);
			if (g_gui.m_opened)
			{
				if (m_using_keyboard)
				{
					PAD::DISABLE_ALL_CONTROL_ACTIONS(0);
					m_using_keyboard = false;
				}
				else
				{
					PAD::DISABLE_CONTROL_ACTION(0, INPUT_NEXT_CAMERA, true);
					PAD::DISABLE_CONTROL_ACTION(0, INPUT_VEH_CIN_CAM, true);
					PAD::DISABLE_CONTROL_ACTION(0, INPUT_ATTACK, true);
					PAD::DISABLE_CONTROL_ACTION(0, INPUT_ATTACK2, true);
					PAD::DISABLE_CONTROL_ACTION(0, INPUT_LOOK_LR, true);
					PAD::DISABLE_CONTROL_ACTION(0, INPUT_LOOK_UD, true);
					PAD::DISABLE_CONTROL_ACTION(0, INPUT_AIM, true);
					PAD::DISABLE_CONTROL_ACTION(0, INPUT_VEH_RADIO_WHEEL, true);
					PAD::DISABLE_CONTROL_ACTION(0, INPUT_SELECT_NEXT_WEAPON, true);
					PAD::DISABLE_CONTROL_ACTION(0, INPUT_SELECT_PREV_WEAPON, true);
					PAD::DISABLE_CONTROL_ACTION(0, INPUT_VEH_SELECT_NEXT_WEAPON, true);
					PAD::DISABLE_CONTROL_ACTION(0, INPUT_VEH_ATTACK, true);
					PAD::DISABLE_CONTROL_ACTION(0, INPUT_VEH_ATTACK2, true);
					PAD::DISABLE_CONTROL_ACTION(0, INPUT_VEH_FLY_ATTACK, true);
					PAD::DISABLE_CONTROL_ACTION(0, INPUT_VEH_FLY_ATTACK2, true);
					PAD::DISABLE_CONTROL_ACTION(0, INPUT_VEH_PASSENGER_AIM, true);
					PAD::DISABLE_CONTROL_ACTION(0, INPUT_VEH_PASSENGER_ATTACK, true);
					PAD::DISABLE_CONTROL_ACTION(0, INPUT_VEH_AIM, true);
					PAD::DISABLE_CONTROL_ACTION(0, INPUT_VEH_MOUSE_CONTROL_OVERRIDE, true);
					PAD::DISABLE_CONTROL_ACTION(0, INPUT_VEH_FLY_MOUSE_CONTROL_OVERRIDE, true);
					PAD::DISABLE_CONTROL_ACTION(0, INPUT_CURSOR_ACCEPT, true);
				}
			}
		}
		EXCEPT_CLAUSE
	}

	void gui::script_func()
	{
		while (true)
		{
			g_gui.script_on_tick();
			script::get_current()->yield();
		}
	}
}