#pragma once

#include "WeaponJson.h"

namespace big
{
	class weapon_info
	{
	public:
		static void load_weapon_info();
		static const char* get_weapon_name(Hash weapon);
		static std::vector<weapon_attachment::weapon_attachment> get_weapon_attachments(Hash weapon);
		static void add_weapon_info_to_json(Hash weapon_hash, weapon_json::weapon_json weapon);
		static const std::unordered_map<Hash, weapon_json::weapon_json> get_all_weapons();
		static const std::map<std::string, Hash> get_display_weapons();
		static const char* get_attachment_name(std::string name);
		static const std::vector<weapon_json::weapon_json> get_weapons_by_group(const std::initializer_list<Hash>);
		static const weapon_json::weapon_json get_weapon(Hash weapon);
		static const char* resolve_vehicle_weapon_name(Hash);
		static const bool does_weapon_exist(Hash weapon);
	private:
		static void set_weapon_display_list();
		static void set_attachment_display_list();
		static void set_weapon_group_list();
		static std::filesystem::path get_weapon_json_file();
		static const char* resolve_weapon_name(Hash weapon);
	};
}