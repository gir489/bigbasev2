#pragma once
#include "common.hpp"
#include "gta/player.hpp"

namespace big
{
	class settings
	{
	public:
		explicit settings() = default;
		~settings() = default;

		std::unordered_map<std::uint64_t, nlohmann::json> player_options;
		nlohmann::json default_player_options =
			R"({
			"money drop": false,
			"never wanted": false,
			"rp drop": false,
			"sound spam": false,
			"semi-godmode": false,
			"otr": false
		})"_json;

		nlohmann::json options;
		nlohmann::json default_options =
			R"({
			"add aircraft height": false,
			"allow all weapons in drive by": false,
			"allow all weapons in drive by opt": 0,
			"always car clean": false,
			"always car fixed": false,
			"always car godmode": false,
			"always clean ped": false,
			"always off the radar": false,
			"always win poker": false,
			"always win roulette": false,
			"anti-afk": false,
			"auto kick on report": false,
			"auto kick on vote kick": false,
			"bypass casino win limit": false,
			"casino wheel prize": 0,
			"chat command toggle": false,
			"disable ragdoll": false,
			"eat opressormk2": false,
			"eat spaghettios": false,
			"explode chat command": false,
			"explosive ammo": false,
			"explosive melee": false,
			"gamer list": {},
			"gamer mode": false,
			"godmode": false,
			"infinite ammo": false,
			"infinite car boost": false,
			"infinite snacks": false,
			"log chat events": false,
			"log network events": 0,
			"log object events": false,
			"log script events": true,
			"mobile radio": false,
			"money chat command": false,
			"name esp": false,
			"lock wanted": false,
			"lock wanted level": 0,
			"persist outfit all": false,
			"persist outfit clothes": false,
			"persist outfit face": false,
			"persist outfit head": false,
			"persist outfit model": false,
			"persist outfit only in freemode": false,
			"persist outfit props": false,
			"persist outfit tattoos": false,
			"persist weapons": "",
			"remove otr from other players": false,
			"script monitor sorted": false,
			"selected outfit file": "",
			"semi-godmode": false,
			"semi-godmode for everyone": false,
			"semigod chat command": false,
			"spawn chat command": false,
			"spawn in vehicle": false,
			"spawn vehicle boosted": false,
			"spawn vehicle godmode": false,
			"spawn vehicle strong": false,
			"spoof name": "",
			"spoof name_real": "",
			"spoof scid": 0,
			"spoof scid_real": 0,
			"super jump": false,
			"super run": false,
			"unlock bunker research": false,
			"vehicle flymode": false,
			"weapon hotkeys": [false,[0,0],[0,0,0,0],[0,0],[0,0],[0,0],[0,0]],
			"weapons chat command": false
		})"_json;

		bool save()
		{
			std::string settings_file = std::getenv("appdata");
			settings_file += "\\BigBaseV2\\settings.json";

			std::ofstream file(settings_file, std::ios::out | std::ios::trunc);
			file << options.dump(4);
			file.close();
			return true;
		}

		bool load()
		{
			std::string settings_file = std::getenv("appdata");
			settings_file += "\\BigBaseV2\\settings.json";

			std::ifstream file(settings_file);

			if (!file.is_open())
			{
				write_default_config();
				file.open(settings_file);
			}

			file >> options;

			bool should_save = false;
			for (auto& e : default_options.items())
			{
				if (options.count(e.key()) == 0)
				{
					should_save = true;
					options[e.key()] = e.value();
				}
					
			}

			if (should_save)
			{
				LOG(INFO) << "Updating settings.";
				save();
			}

			return true;
		}

	private:
		bool write_default_config()
		{
			std::string settings_file = std::getenv("appdata");
			settings_file += "\\BigBaseV2\\settings.json";

			std::ofstream file(settings_file, std::ios::out);
			file << default_options.dump(4);
			file.close();

			options.clear();
			options = default_options;

			return true;
		}
	};

	inline settings g_settings;
}
