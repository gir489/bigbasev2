#include "AntiCheatHandle.h"
#include "pointers.hpp"
#include "hooks/NetEventHook.h"

namespace big
{
	int anti_cheat_handle::FindFreeRegister(const CONTEXT* ctx, void* function)
	{
		for (int i = 0; i < 4; ++i)
		{
			if ((&ctx->Dr0)[i] == (decltype(ctx->Dr0)(function)))
			{
				return -1; // already hooked
			}

			if ((ctx->Dr7 & (1ULL << (i * 2))) == 0)
			{
				return i;
			}
		}

		return -1;
	}

	bool anti_cheat_handle::SetBreakpointsForThreadInternal(HANDLE thread, DWORD threadId, bool enable)
	{
		CONTEXT ctx = {};
		ctx.ContextFlags = CONTEXT_DEBUG_REGISTERS;

		if (GetThreadContext(thread, &ctx))
		{
			if (enable)
			{
				int regIndex = FindFreeRegister(&ctx, g_pointers->m_send_network_event_function);

				if (regIndex != -1)
				{
					switch (regIndex)
					{
						case 0: ctx.Dr0 = (decltype(ctx.Dr0))(g_pointers->m_send_network_event_function); break;
						case 1: ctx.Dr1 = (decltype(ctx.Dr1))(g_pointers->m_send_network_event_function); break;
						case 2: ctx.Dr2 = (decltype(ctx.Dr2))(g_pointers->m_send_network_event_function); break;
						case 3: ctx.Dr3 = (decltype(ctx.Dr3))(g_pointers->m_send_network_event_function); break;
					}

					ctx.Dr7 &= ~(3ULL << (16 + 4 * regIndex));
					ctx.Dr7 &= ~(3ULL << (18 + 4 * regIndex));
					ctx.Dr7 |= 1ULL << (2 * regIndex);
					g_SendNetworkEventThreadRegisters.emplace(threadId, regIndex);
				}

				regIndex = FindFreeRegister(&ctx, g_pointers->m_anti_cheat_function);

				if (regIndex != -1)
				{
					switch (regIndex)
					{
						case 0: ctx.Dr0 = (decltype(ctx.Dr0))(g_pointers->m_anti_cheat_function); break;
						case 1: ctx.Dr1 = (decltype(ctx.Dr1))(g_pointers->m_anti_cheat_function); break;
						case 2: ctx.Dr2 = (decltype(ctx.Dr2))(g_pointers->m_anti_cheat_function); break;
						case 3: ctx.Dr3 = (decltype(ctx.Dr3))(g_pointers->m_anti_cheat_function); break;
					}

					ctx.Dr7 &= ~(3ULL << (16 + 4 * regIndex));
					ctx.Dr7 &= ~(3ULL << (18 + 4 * regIndex));
					ctx.Dr7 |= 1ULL << (2 * regIndex);

					g_AntiCheatThreadRegisters.emplace(threadId, regIndex);
				}
			}
			else
			{
				if (auto it = g_SendNetworkEventThreadRegisters.find(threadId); it != g_SendNetworkEventThreadRegisters.end())
				{
					int regIndex = it->second;

					switch (regIndex)
					{
						case 0: ctx.Dr0 = 0; break;
						case 1: ctx.Dr1 = 0; break;
						case 2: ctx.Dr2 = 0; break;
						case 3: ctx.Dr3 = 0; break;
					}

					ctx.Dr7 &= ~(1ULL << (2 * regIndex));

					g_SendNetworkEventThreadRegisters.erase(it);
				}
				if (auto it = g_AntiCheatThreadRegisters.find(threadId); it != g_AntiCheatThreadRegisters.end())
				{
					int regIndex = it->second;

					switch (regIndex)
					{
						case 0: ctx.Dr0 = 0; break;
						case 1: ctx.Dr1 = 0; break;
						case 2: ctx.Dr2 = 0; break;
						case 3: ctx.Dr3 = 0; break;
					}

					ctx.Dr7 &= ~(1ULL << (2 * regIndex));

					g_AntiCheatThreadRegisters.erase(it);
				}
			}

			SetThreadContext(thread, &ctx);
		}

		return false;
	}

	bool anti_cheat_handle::SetBreakpointsForThread(HANDLE thread, bool enable)
	{
		DWORD threadId = GetThreadId(thread);
		if (threadId == GetCurrentThreadId())
		{
			SetBreakpointsForThreadInternal(thread, threadId, enable);

			return true;
		}
		else if (SuspendThread(thread) != (DWORD)-1)
		{
			SetBreakpointsForThreadInternal(thread, threadId, enable);
			ResumeThread(thread);

			return true;
		}

		return false;
	}

	bool anti_cheat_handle::SetBreakpointsForAllThreads(bool enable)
	{
		HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, GetCurrentThreadId());
		if (hSnapshot)
		{
			THREADENTRY32 entry;
			entry.dwSize = sizeof(entry);

			if (Thread32First(hSnapshot, &entry))
			{
				do
				{
					HANDLE thread = OpenThread(THREAD_GET_CONTEXT | THREAD_QUERY_LIMITED_INFORMATION | THREAD_SET_CONTEXT | THREAD_SUSPEND_RESUME, false, entry.th32ThreadID);
					if (thread)
					{
						SetBreakpointsForThread(thread, enable);
						CloseHandle(thread);
					}
				} while (Thread32Next(hSnapshot, &entry));

				return true;
			}

			CloseHandle(hSnapshot);
		}

		return true;
	}

	bool anti_cheat_handle::AntiCheatCallback(DetectStruct* thisptr, DetectItem* item)
	{
		return false;
	}

	void anti_cheat_handle::SendNetworkEventCallback(rage::netEventMgr* this_ptr, rage::netGameEvent* game_event)
	{
		if (NetEventHook::NetworkEventSendFunction(this_ptr, game_event))
		{
			OnThreadDetach(GetCurrentThread());
			g_pointers->m_send_network_event_function(this_ptr, game_event);
			OnThreadAttach(GetCurrentThread());
		}
	}

	void anti_cheat_handle::OnThreadAttach(HANDLE thread)
	{
		SetBreakpointsForThread(thread, true);
	}

	void anti_cheat_handle::OnThreadDetach(HANDLE thread)
	{
		SetBreakpointsForThread(thread, false);
	}

	LONG WINAPI anti_cheat_handle::ExceptionHandler(EXCEPTION_POINTERS* exp)
	{
		if (exp->ExceptionRecord->ExceptionCode == EXCEPTION_SINGLE_STEP)
		{
			if ((PVOID)(exp->ContextRecord->Rip) == g_pointers->m_anti_cheat_function)
			{
				exp->ContextRecord->Rip = (decltype(exp->ContextRecord->Rip))(&AntiCheatCallback);

				return EXCEPTION_CONTINUE_EXECUTION;
			}
			if ((PVOID)(exp->ContextRecord->Rip) == g_pointers->m_send_network_event_function)
			{
				exp->ContextRecord->Rip = (decltype(exp->ContextRecord->Rip))(&SendNetworkEventCallback);

				return EXCEPTION_CONTINUE_EXECUTION;
			}
		}

		return EXCEPTION_CONTINUE_SEARCH;
	}

	void anti_cheat_handle::Initialize()
	{
		g_VectoredExceptionHandler = AddVectoredExceptionHandler(true, &ExceptionHandler);

		SetBreakpointsForAllThreads(true);
	}

	void anti_cheat_handle::Uninitialize()
	{
		SetBreakpointsForAllThreads(false);

		RemoveVectoredExceptionHandler(g_VectoredExceptionHandler);
	}
}