#pragma once

#include "ModelAttachment.h"

namespace player_attachment
{
	struct player_attachment : model_attachment::model_attachment
	{
		std::string display_name;
		uint32_t bone;
	};
	static void to_json(nlohmann::json& j, const player_attachment& attachment) {
		model_attachment::to_json(j, attachment);
		j["display_name"] = attachment.display_name;
		j["bone"] = attachment.bone;
	}

	static void from_json(const nlohmann::json& j, player_attachment& attachment) {
		model_attachment::from_json(j, attachment);
		j.at("display_name").get_to(attachment.display_name); j.at("bone").get_to(attachment.bone);
	}
}

namespace big
{
	class player_attachments
	{
	public:

		static void load_attachments();
		static std::vector<player_attachment::player_attachment> get_attachments();
	private:
		static std::filesystem::path get_json_file();
		static inline std::vector<player_attachment::player_attachment> attachments;
	};
}