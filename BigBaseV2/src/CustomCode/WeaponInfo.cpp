#include "WeaponInfo.h"
#include "fiber_pool.hpp"
#include "script.hpp"
#include "gta\Weapons.h"

namespace big
{
	static std::unordered_map<Hash, weapon_json::weapon_json> weapon_list;
	static std::map<std::string, Hash> weapon_display_list;
	static std::map<std::string, std::string> attachment_display_list;
	static std::unordered_map<Hash, std::vector<weapon_json::weapon_json>> weapon_group_list;
	static std::unordered_map<Hash, std::string> vehicle_weapon_list = { {VEHICLE_WEAPON_ROTORS, "ROTORS" }, { VEHICLE_WEAPON_TANK, "TANK" }, { VEHICLE_WEAPON_SEARCHLIGHT, "SEARCHLIGHT" }, { VEHICLE_WEAPON_RADAR, "RADAR" }, { VEHICLE_WEAPON_PLAYER_BULLET, "PLAYER_BULLET" }, { VEHICLE_WEAPON_PLAYER_LAZER, "PLAYER_LAZER" }, { VEHICLE_WEAPON_ENEMY_LASER, "ENEMY_LASER" }, { VEHICLE_WEAPON_PLAYER_BUZZARD, "PLAYER_BUZZARD" }, { VEHICLE_WEAPON_PLAYER_HUNTER, "PLAYER_HUNTER" }, { VEHICLE_WEAPON_PLANE_ROCKET, "PLANE_ROCKET" }, { VEHICLE_WEAPON_SPACE_ROCKET, "SPACE_ROCKET" }, { VEHICLE_WEAPON_TURRET_INSURGENT, "TURRET_INSURGENT" }, { VEHICLE_WEAPON_PLAYER_SAVAGE, "PLAYER_SAVAGE" }, { VEHICLE_WEAPON_TURRET_TECHNICAL, "TURRET_TECHNICAL" }, { VEHICLE_WEAPON_NOSE_TURRET_VALKYRIE, "NOSE_TURRET_VALKYRIE" }, { VEHICLE_WEAPON_TURRET_VALKYRIE, "TURRET_VALKYRIE" }, { VEHICLE_WEAPON_CANNON_BLAZER, "CANNON_BLAZER" }, { VEHICLE_WEAPON_TURRET_BOXVILLE, "TURRET_BOXVILLE" }, { VEHICLE_WEAPON_RUINER_BULLET, "RUINER_BULLET" }, { VEHICLE_WEAPON_RUINER_ROCKET, "RUINER_ROCKET" }, { VEHICLE_WEAPON_HUNTER_MG, "HUNTER_MG" }, { VEHICLE_WEAPON_HUNTER_MISSILE, "HUNTER_MISSILE" }, { VEHICLE_WEAPON_HUNTER_CANNON, "HUNTER_CANNON" }, { VEHICLE_WEAPON_HUNTER_BARRAGE, "HUNTER_BARRAGE" }, { VEHICLE_WEAPON_TULA_NOSEMG, "TULA_NOSEMG" }, { VEHICLE_WEAPON_TULA_MG, "TULA_MG" }, { VEHICLE_WEAPON_TULA_DUALMG, "TULA_DUALMG" }, { VEHICLE_WEAPON_TULA_MINIGUN, "TULA_MINIGUN" }, { VEHICLE_WEAPON_SEABREEZE_MG, "SEABREEZE_MG" }, { VEHICLE_WEAPON_MICROLIGHT_MG, "MICROLIGHT_MG" }, { VEHICLE_WEAPON_DOGFIGHTER_MG, "DOGFIGHTER_MG" }, { VEHICLE_WEAPON_DOGFIGHTER_MISSILE, "DOGFIGHTER_MISSILE" }, { VEHICLE_WEAPON_MOGUL_NOSE, "MOGUL_NOSE" }, { VEHICLE_WEAPON_MOGUL_DUALNOSE, "MOGUL_DUALNOSE" }, { VEHICLE_WEAPON_MOGUL_TURRET, "MOGUL_TURRET" }, { VEHICLE_WEAPON_MOGUL_DUALTURRET, "MOGUL_DUALTURRET" }, { VEHICLE_WEAPON_ROGUE_MG, "ROGUE_MG" }, { VEHICLE_WEAPON_ROGUE_CANNON, "ROGUE_CANNON" }, { VEHICLE_WEAPON_ROGUE_MISSILE, "ROGUE_MISSILE" }, { VEHICLE_WEAPON_BOMBUSHKA_DUALMG, "BOMBUSHKA_DUALMG" }, { VEHICLE_WEAPON_BOMBUSHKA_CANNON, "BOMBUSHKA_CANNON" }, { VEHICLE_WEAPON_HAVOK_MINIGUN, "HAVOK_MINIGUN" }, { VEHICLE_WEAPON_VIGILANTE_MG, "VIGILANTE_MG" }, { VEHICLE_WEAPON_VIGILANTE_MISSILE, "VIGILANTE_MISSILE" }, { VEHICLE_WEAPON_TURRET_LIMO, "TURRET_LIMO" }, { VEHICLE_WEAPON_DUNE_MG, "DUNE_MG" }, { VEHICLE_WEAPON_DUNE_GRENADELAUNCHER, "DUNE_GRENADELAUNCHER" }, { VEHICLE_WEAPON_DUNE_MINIGUN, "DUNE_MINIGUN" }, { VEHICLE_WEAPON_TAMPA_MISSILE, "TAMPA_MISSILE" }, { VEHICLE_WEAPON_TAMPA_MORTAR, "TAMPA_MORTAR" }, { VEHICLE_WEAPON_TAMPA_FIXEDMINIGUN, "TAMPA_FIXEDMINIGUN" }, { VEHICLE_WEAPON_TAMPA_DUALMINIGUN, "TAMPA_DUALMINIGUN" }, { VEHICLE_WEAPON_HALFTRACK_DUALMG, "HALFTRACK_DUALMG" }, { VEHICLE_WEAPON_HALFTRACK_QUADMG, "HALFTRACK_QUADMG" }, { VEHICLE_WEAPON_APC_CANNON, "APC_CANNON" }, { VEHICLE_WEAPON_APC_MISSILE, "APC_MISSILE" }, { VEHICLE_WEAPON_APC_MG, "APC_MG" }, { VEHICLE_WEAPON_ARDENT_MG, "ARDENT_MG" }, { VEHICLE_WEAPON_TECHNICAL_MINIGUN, "TECHNICAL_MINIGUN" }, { VEHICLE_WEAPON_INSURGENT_MINIGUN, "INSURGENT_MINIGUN" }, { VEHICLE_WEAPON_TRAILER_QUADMG, "TRAILER_QUADMG" }, { VEHICLE_WEAPON_TRAILER_MISSILE, "TRAILER_MISSILE" }, { VEHICLE_WEAPON_TRAILER_DUALAA, "TRAILER_DUALAA" }, { VEHICLE_WEAPON_NIGHTSHARK_MG, "NIGHTSHARK_MG" }, { VEHICLE_WEAPON_OPPRESSOR_MG, "OPPRESSOR_MG" }, { VEHICLE_WEAPON_OPPRESSOR_MISSILE, "OPPRESSOR_MISSILE" }, { VEHICLE_WEAPON_MOBILEOPS_CANNON, "MOBILEOPS_CANNON" }, { VEHICLE_WEAPON_AKULA_TURRET_SINGLE, "AKULA_TURRET_SINGLE" }, { VEHICLE_WEAPON_AKULA_MISSILE, "AKULA_MISSILE" }, { VEHICLE_WEAPON_AKULA_TURRET_DUAL, "AKULA_TURRET_DUAL" }, { VEHICLE_WEAPON_AKULA_MINIGUN, "AKULA_MINIGUN" }, { VEHICLE_WEAPON_AKULA_BARRAGE, "AKULA_BARRAGE" }, { VEHICLE_WEAPON_AVENGER_CANNON, "AVENGER_CANNON" }, { VEHICLE_WEAPON_BARRAGE_TOP_MG, "BARRAGE_TOP_MG" }, { VEHICLE_WEAPON_BARRAGE_TOP_MINIGUN, "BARRAGE_TOP_MINIGUN" }, { VEHICLE_WEAPON_BARRAGE_REAR_MG, "BARRAGE_REAR_MG" }, { VEHICLE_WEAPON_BARRAGE_REAR_MINIGUN, "BARRAGE_REAR_MINIGUN" }, { VEHICLE_WEAPON_BARRAGE_REAR_GL, "BARRAGE_REAR_GL" }, { VEHICLE_WEAPON_CHERNO_MISSILE, "CHERNO_MISSILE" }, { VEHICLE_WEAPON_COMET_MG, "COMET_MG" }, { VEHICLE_WEAPON_DELUXO_MG, "DELUXO_MG" }, { VEHICLE_WEAPON_DELUXO_MISSILE, "DELUXO_MISSILE" }, { VEHICLE_WEAPON_KHANJALI_CANNON, "KHANJALI_CANNON" }, { VEHICLE_WEAPON_KHANJALI_CANNON_HEAVY, "KHANJALI_CANNON_HEAVY" }, { VEHICLE_WEAPON_KHANJALI_MG, "KHANJALI_MG" }, { VEHICLE_WEAPON_KHANJALI_GL, "KHANJALI_GL" }, { VEHICLE_WEAPON_REVOLTER_MG, "REVOLTER_MG" }, { VEHICLE_WEAPON_WATER_CANNON, "WATER_CANNON" }, { VEHICLE_WEAPON_SAVESTRA_MG, "SAVESTRA_MG" }, { VEHICLE_WEAPON_THRUSTER_MG, "THRUSTER_MG" }, { VEHICLE_WEAPON_THRUSTER_MISSILE, "THRUSTER_MISSILE" }, { VEHICLE_WEAPON_VISERIS_MG, "VISERIS_MG" }, { VEHICLE_WEAPON_VOLATOL_DUALMG, "VOLATOL_DUALMG"},
																		 {VEHICLE_WEAPON_SUBCAR_MG, "Machine Gun"}, {VEHICLE_WEAPON_SUBCAR_MISSILE, "Missles"}, {VEHICLE_WEAPON_SUBCAR_TORPEDO, "Missles (Homing Off)"} };
	void weapon_info::load_weapon_info()
	{
		std::ifstream file(get_weapon_json_file());
		nlohmann::json weapon_list_json;
		file >> weapon_list_json;
		file.close();
		weapon_list = weapon_list_json["Weapon List"].get<std::unordered_map<Hash, weapon_json::weapon_json>>();
		QUEUE_JOB_BEGIN_CLAUSE()
		{
			set_weapon_display_list();
			set_attachment_display_list();
			set_weapon_group_list();
		} QUEUE_JOB_END_CLAUSE
	}
	const char* weapon_info::get_weapon_name(Hash weapon)
	{
		for (auto const& entry : weapon_display_list)
			if (entry.second == weapon)
				return entry.first.c_str();
		return "NOT_FOUND";
	}
	const char* weapon_info::resolve_weapon_name(Hash weapon)
	{
		return HUD::_GET_LABEL_TEXT(get_weapon(weapon).display_name.c_str());
	}
	const char* weapon_info::resolve_vehicle_weapon_name(Hash weapon)
	{
		if (vehicle_weapon_list.find(weapon) == vehicle_weapon_list.end())
			return nullptr;
		return vehicle_weapon_list[weapon].c_str();
	}
	std::vector<weapon_attachment::weapon_attachment> weapon_info::get_weapon_attachments(Hash weapon)
	{
		return get_weapon(weapon).attachments;
	}
	const char* weapon_info::get_attachment_name(std::string name)
	{
		if (attachment_display_list.find(name) != attachment_display_list.end())
			return attachment_display_list.find(name)->second.c_str();
		return "NOT_FOUND";
	}
	void weapon_info::add_weapon_info_to_json(Hash weapon_hash, weapon_json::weapon_json weapon)
	{
		if (weapon_list.find(weapon_hash) == weapon_list.end())
			weapon_list[weapon_hash] = weapon;
	}
	const std::unordered_map<Hash, weapon_json::weapon_json> weapon_info::get_all_weapons()
	{
		return weapon_list;
	}
	const std::map<std::string, Hash> weapon_info::get_display_weapons()
	{
		return weapon_display_list;
	}
	void weapon_info::set_weapon_display_list()
	{
		for (auto const& entry : weapon_list)
			weapon_display_list[std::string(resolve_weapon_name(entry.first))] = entry.first;
	}
	void weapon_info::set_attachment_display_list()
	{
		for (auto weapon_info : get_all_weapons())
		{
			for (auto component : get_weapon_attachments(weapon_info.first))
			{
				std::string resolve_name = HUD::_GET_LABEL_TEXT(component.loc_name.c_str());
				if (resolve_name == "NULL")
					resolve_name = component.loc_name;
				attachment_display_list[component.loc_name] = resolve_name;
			}
		}
	}
	void weapon_info::set_weapon_group_list()
	{
		for (auto weapon_info : get_all_weapons())
		{
			Hash group = weapon_info.second.group;

			if (weapon_group_list.find(group) == weapon_group_list.end())
				weapon_group_list[group] = {};
			weapon_group_list[group].push_back(weapon_info.second);
		}
	}
	const std::vector<weapon_json::weapon_json> weapon_info::get_weapons_by_group(const std::initializer_list<Hash> groups)
	{
		std::vector<weapon_json::weapon_json> retn_val{};

		for (Hash group : groups)
		{
			auto val = weapon_group_list.find(group);
			if (val != weapon_group_list.end())
				retn_val.insert(retn_val.end(), val->second.begin(), val->second.end());
		}

		return retn_val;
	}
	const weapon_json::weapon_json weapon_info::get_weapon(Hash weapon)
	{
		auto retn_val = weapon_list.find(weapon);
		if (retn_val != weapon_list.end())
			return retn_val->second;
#ifndef _DEBUG
		LOG(FATAL) << "get_weapon was asked about " << HEX_TO_UPPER(weapon) << " but was not in weapons.json";
#else
		LOG(WARNING) << "get_weapon was asked about " << HEX_TO_UPPER(weapon) << " but was not in weapons.json";
#endif
		return weapon_json::weapon_json{};
	}
	std::filesystem::path weapon_info::get_weapon_json_file()
	{
		auto file_path = std::filesystem::path(std::getenv("appdata"));
		file_path /= "BigBaseV2";

		if (!std::filesystem::exists(file_path))
		{
			std::filesystem::create_directory(file_path);
		}
		else if (!std::filesystem::is_directory(file_path))
		{
			std::filesystem::remove(file_path);
			std::filesystem::create_directory(file_path);
		}

		file_path /= "Weapons.json";
		if (!std::filesystem::exists(file_path))
			throw std::runtime_error("Failed to find Weapons.json");

		return file_path;
	}
	const bool weapon_info::does_weapon_exist(Hash weapon)
	{
		return weapon_list.find(weapon) != weapon_list.end();
	}
}