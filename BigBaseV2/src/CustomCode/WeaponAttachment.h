#pragma once

#include "natives.hpp"

namespace weapon_attachment
{
	struct weapon_attachment
	{
		Hash hash;
		std::string name;
		std::string loc_name;
		std::string loc_desc;
	};

	static void to_json(nlohmann::json& j, const weapon_attachment& attachment) {
		j = nlohmann::json{ {"hash", attachment.hash}, {"name", attachment.name}, {"loc_name", attachment.loc_name}, {"loc_desc", attachment.loc_desc} };
	}
	static void from_json(const nlohmann::json& j, weapon_attachment& attachment) {
		j.at("hash").get_to(attachment.hash); j.at("name").get_to(attachment.name); j.at("loc_name").get_to(attachment.loc_name); j.at("loc_desc").get_to(attachment.loc_desc);
	}
}