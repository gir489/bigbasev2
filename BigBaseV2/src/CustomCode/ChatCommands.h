#pragma once
#include "common.hpp"
#include "fiber_pool.hpp"
#include "helpers\VehicleHelper.h"
#include "helpers\RageHelper.h"
#include "Settings.h"

namespace big
{
	class chat_commands
	{
	public:
		static void handle_chat_command(CNetGamePlayer*, const char* message);
	private:
		using command_handler = void(*)(Player sender, std::vector<std::string> args);
		struct command
		{
			std::string toggle_name;
			int arg_count;
			command_handler handler;
		};

		static std::unordered_map<std::string, command> handlers;

		static void spawn_handler(Player sender, std::vector<std::string> args);
		static void weapons_handler(Player sender, std::vector<std::string> args);
		static void explode_handler(Player sender, std::vector<std::string> args);
		static void money_handler(Player sender, std::vector<std::string> args);
		static void semigod_handler(Player sender, std::vector<std::string> args);
	};
}