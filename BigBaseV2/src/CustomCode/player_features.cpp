#include "common.hpp"
#include "player_features.hpp"
#include "script.hpp"
#include "pointers.hpp"
#include "gta\PickupRewards.h"
#include "CustomCode/Settings.h"
#include "script_global.hpp"
#include <queue>
#include "CustomCode/PlayerInfo.h"
#include "gta_util.hpp"

namespace big
{	
	void player_features::run_tick()
	{
		static uint32_t sound_spam_timer{}, semi_godmode_timer{};
		static std::unordered_map<int, int> otr_timer{};

		if (NETWORK::NETWORK_IS_TRANSITION_STARTED() || !*big::g_pointers->m_is_session_started)
		{
			sound_spam_timer = semi_godmode_timer = 0;
			otr_timer.clear();
			return;
		}

		int sound_spam_bitset{}, health_bitset{}, armor_bitset{};

		static int money_drop_timer;
		static std::queue<Player> money_drop_queue;

		static int rp_drop_timer;
		static std::queue<Player> rp_drop_queue;

		for (int i = 0; i < gta::num_players; i++)
		{
			if (NETWORK::NETWORK_IS_PLAYER_CONNECTED(i))
			{
				auto scid = g_pointers->m_get_net_player(i)->get_net_data()->m_rockstar_id;
				if (g_settings.player_options[scid].empty())
				{
					g_settings.player_options[scid] = g_settings.default_player_options;
				}
				if (g_settings.player_options[scid]["sound spam"])
				{
					if ((NETWORK::GET_NETWORK_TIME() - sound_spam_timer) > 100)
					{
						sound_spam_bitset |= (1 << i);
					}
				}

				if (g_settings.player_options[scid]["semi-godmode"] || (g_settings.options["semi-godmode for everyone"] && g_local_player.network_is_activity_session))
				{
					Ped ped = PLAYER::GET_PLAYER_PED_SCRIPT_INDEX(i);
					if (ENTITY::GET_ENTITY_HEALTH(ped) < ENTITY::GET_ENTITY_MAX_HEALTH(ped))
						health_bitset |= (1 << i);
					if (PED::GET_PED_ARMOUR(ped) < PLAYER::GET_PLAYER_MAX_ARMOUR(i))
						armor_bitset |= (1 << i);
				}

				if (g_settings.player_options[scid]["money drop"])
				{
					if ((NETWORK::GET_NETWORK_TIME() - money_drop_timer) > 1000)
					{
						money_drop_queue.push(i);
					}
				}

				if (g_settings.player_options[scid]["rp drop"])
				{
					if ((NETWORK::GET_NETWORK_TIME() - rp_drop_timer) > 500)
					{
						rp_drop_queue.push(i);
					}
				}

				if (g_settings.player_options[scid]["otr"])
				{
					if (!g_local_player.network_is_activity_session)
					{
						if (otr_timer[i] == 0 || (NETWORK::GET_NETWORK_TIME() - otr_timer[i]) > 4000)
						{
							otr_timer[i] = NETWORK::GET_NETWORK_TIME();
							static big::script_global off_the_radar_event_global = big::script_global(Globals::PlayerInformation);
							int64_t args[7] = { Events::OffTheRadar, g_local_player.player, NETWORK::GET_NETWORK_TIME(), NETWORK::GET_NETWORK_TIME(), 1, 1, *off_the_radar_event_global.at(i, Globals::PlayerInformation_Size).at(OtrAndWatedLevel).as<int32_t*>() };
							g_pointers->m_trigger_script_event(1, &args[0], 7, (1 << i));
						}
					}
					else if (otr_timer[i])
					{
						otr_timer[i] = 0;
					}
				}

				if (g_settings.player_options[scid]["never wanted"])
				{
					rage::CPlayerInfo* info = gta_util::get_player_playerinfo(i);
					if (info != nullptr && info->m_wanted_level > 0)
					{
						int64_t args[3] = { Events::ClearWantedLevel, g_local_player.player, *script_global(Globals::PlayerInformation).at(i, Globals::PlayerInformation_Size).at(OtrAndWatedLevel).as<int*>() };
						g_pointers->m_trigger_script_event(1, &args[0], 3, (1 << i));
					}
				}
			}
		}

		if (health_bitset || armor_bitset)
		{
			if ((NETWORK::GET_NETWORK_TIME() - semi_godmode_timer) > 500)
			{
				semi_godmode_timer = NETWORK::GET_NETWORK_TIME();
				if (health_bitset)
					g_pointers->m_send_give_reward_pickup(health_bitset, REWARD_HEALTH);
				if (armor_bitset)
					g_pointers->m_send_give_reward_pickup(armor_bitset, REWARD_ARMOUR);
			}
		}

		if (sound_spam_bitset != 0)
		{
			int64_t args[13] = { Events::SoundSpam, g_local_player.player, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
			g_pointers->m_trigger_script_event(1, &args[0], 13, sound_spam_bitset);
			sound_spam_timer = NETWORK::GET_NETWORK_TIME();
		}

		while (!money_drop_queue.empty())
		{
			const Hash model = RAGE_JOAAT("p_poly_bag_01_s");
			const Hash pickup = RAGE_JOAAT("pickup_money_security_case");
			STREAMING::REQUEST_MODEL(model);

			if (STREAMING::HAS_MODEL_LOADED(model))
			{
				Vector3 coords = ENTITY::GET_ENTITY_COORDS(PLAYER::GET_PLAYER_PED_SCRIPT_INDEX(money_drop_queue.front()), false);
				OBJECT::CREATE_AMBIENT_PICKUP(pickup, coords.x, coords.y, coords.z + 1, 0, 2500, model, false, true);
			}
		
			money_drop_queue.pop();

			if (money_drop_queue.empty())
				money_drop_timer = NETWORK::GET_NETWORK_TIME();
		}

		while (!rp_drop_queue.empty())
		{
			const Hash model = RAGE_JOAAT("vw_prop_vw_colle_rsrgeneric");
			const Hash pickup = RAGE_JOAAT("pickup_portable_crate_unfixed_incar_with_passengers");
			STREAMING::REQUEST_MODEL(model);

			if (STREAMING::HAS_MODEL_LOADED(model))
			{
				Vector3 coords = ENTITY::GET_ENTITY_COORDS(PLAYER::GET_PLAYER_PED_SCRIPT_INDEX(rp_drop_queue.front()), false);
				OBJECT::CREATE_AMBIENT_PICKUP(pickup, coords.x, coords.y, coords.z + 1, 2, 0, model, false, true);
			}

			rp_drop_queue.pop();

			if (rp_drop_queue.empty())
				rp_drop_timer = NETWORK::GET_NETWORK_TIME();
		}
	}

	void player_features::script_func()
	{
		while (true)
		{
			run_tick();
			script::get_current()->yield();
		}
	}
}