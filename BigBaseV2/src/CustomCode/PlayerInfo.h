#pragma once
#include "natives.hpp"
#include "gta\player.hpp"
#include "gta\enums.hpp"
#include "gta\replay.hpp"
#include <unordered_set>

namespace big
{
	static class player_info_class
	{
	public:
		static inline Player player = -1;
		static inline Ped player_ped = -1;
		static inline int money{}, rank{}, kills{}, deaths{}, health{}, max_health{}, armor{}, max_armor{}, port{}, wanted_level{}, vehicle_state{};
		static inline uint64_t scid{};
		static inline std::string street_name{}, zone_name{}, vehicle_name{}, host_name{}, player_name{};
		static inline rage::netAddress ip{};
		static inline bool is_friend{}, is_host{}, is_script_host{}, is_modder{}, tab_flag{};
		static inline float speed, kdr{};
		static inline Vehicle player_vehicle{};
		static inline Player script_event_tester_target_player{};
	} g_player_info;
	static class local_player_info_class
	{
	public:
		static inline Player player = -1;
		static inline Ped player_ped{};
		static inline Hash weapon{}, player_hash{}, vehicle_model{}, vehicle_weapon{};
		static inline Vehicle vehicle = -1, last_vehicle = -1, personal_vehicle = -1;
		static inline uint16_t net_player_id{}, net_vehicle_id{};
		static inline rage::CPlayerInfo* player_info{};
		static inline CNetGamePlayer* net_game_player{};
		static inline rage::CVehicle* vehicle_ptr{};
		static inline rage::CPed* ped_ptr{};
		static inline bool is_loading_screen_active{}, is_player_switch_in_progress{}, is_cutscene_playing{}, is_playing_casino_games{}, needs_controls_reset{}, is_in_personal_vehicle{}, network_is_activity_session{}, is_mp0_char{}, is_driver{}, is_vehicle_race_car{};
		static inline int current_weapon_tint_count{}, character_index{};
		static inline int* dealer_blackjack_card{};
		static inline std::unordered_map<std::string, GtaThread*> maintain_control{};
		static class vehicleinfo
		{
		public:
			static inline bool tab_flag{};
			static inline Vehicle player_vehicle{};
			static inline std::vector<std::string> slot_display_names;
			static inline std::unordered_map<int, int> owned_mods;
			static inline std::unordered_map<int, std::vector<std::string>> mod_display_names;
			static inline int selected_slot = -1;
			static inline bool can_tires_burst{}, tiresmoke{}, turbo{}, xenon{}, extras[21], extras_values[21];
			static inline int primary_color{}, secondary_color{}, pearlescent{}, wheel_color{}, interior_color{}, dashboard_color{}, windowtint{}, licenseplate{};
		} vehicle_info;
		static class diamondheistinfo
		{
		public:
			static inline int approach{}, target{}, last_approach{}, hard_approach{};
			static inline int disrupt{}, keylevels{}, crewweap{}, crewdriver{}, crewhacker{}, vehs{}, weaps{}, masks{}, bodyarmorlvl{}, bitset{}, bitsetone{};
		} diamond_heist_info;
		static bool is_in_vehicle()
		{
			return g_local_player.vehicle != -1;
		}
		static void casino_check_controls_enable()
		{
			if (g_local_player.needs_controls_reset)
				g_local_player.player_info->m_player_controls &= ~DisableAllControls;
		}
		static void casino_check_controls_disable()
		{
			if ((g_local_player.player_info->m_player_controls & 1) == 0)
				g_local_player.needs_controls_reset = g_local_player.player_info->m_player_controls |= DisableAllControls;
		}
	} g_local_player;
}