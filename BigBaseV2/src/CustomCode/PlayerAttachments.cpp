#include "PlayerAttachments.h"

namespace big
{
	void player_attachments::load_attachments()
	{
		nlohmann::json attachments_json;
		std::ifstream file(get_json_file());
		file >> attachments_json;
		file.close();
		attachments = attachments_json["Attachment List"].get<std::vector<player_attachment::player_attachment>>();
	}

	std::vector<player_attachment::player_attachment> player_attachments::get_attachments()
	{
		return attachments;
	}

	std::filesystem::path player_attachments::get_json_file()
	{
		auto file_path = std::filesystem::path(std::getenv("appdata"));
		file_path /= "BigBaseV2";

		if (!std::filesystem::exists(file_path))
		{
			std::filesystem::create_directory(file_path);
		}
		else if (!std::filesystem::is_directory(file_path))
		{
			std::filesystem::remove(file_path);
			std::filesystem::create_directory(file_path);
		}

		file_path /= "Attachments.json";
		if (!std::filesystem::exists(file_path))
			throw std::runtime_error("Failed to find Attachments.json");

		return file_path;
	}
}