#include "DetectModders.h"
#include "script.hpp"
#include "gui/PlayersTab.h"
#include "pointers.hpp"
#include "persist/PersistModder.h"
#include "helpers/NetworkHelper.h"

namespace big
{
	void detect_modders::run_tick()
	{
		for (auto pair : players_tab::playerlist)
		{
			Player player = pair.second.id;
			auto playerptr = g_pointers->m_get_net_player(player);
			if (g_persist_modder.is_modder(playerptr))
				continue;
			
			auto money = network_helper::get_player_stat<int>(player, MoneyAll);
			auto rank = network_helper::get_player_stat<int>(player, Rank);
			auto kdr = network_helper::get_player_stat<float>(player, KDRatio);

			if (money >= 900000000)
			{
				g_persist_modder.add_modder(playerptr, "Too much money");
				continue;
			}
			if (rank >= 2000)
			{
				g_persist_modder.add_modder(playerptr, "Rank too high");
				continue;
			}
			if (kdr >= 25.f)
			{
				g_persist_modder.add_modder(playerptr, "KDR too high");
				continue;
			}
		}
	}

	void detect_modders::script_func()
	{
		while (true)
		{
			run_tick();
			script::get_current()->yield(120s);
		}
	}
}