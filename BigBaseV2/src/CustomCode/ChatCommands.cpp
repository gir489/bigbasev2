#include "common.hpp"
#include "ChatCommands.h"
#include "CustomCode\WeaponInfo.h"
#include "natives.hpp"
#include "script.hpp"
#include "gui\WeaponsTab.h"

namespace big
{
	std::unordered_map<std::string, chat_commands::command> chat_commands::handlers = {
		{ "/spawn", { "spawn chat command", 1, spawn_handler } },
		{ "/weapons", { "weapons chat command", 0, weapons_handler } },
		{ "/explode", { "explode chat command", 1, explode_handler } },
		{ "/money", { "money chat command", 1, money_handler } },
		{ "/semigod", { "semigod chat command", 1, semigod_handler } }
	};

	void chat_commands::handle_chat_command(CNetGamePlayer* player, const char * message)
	{
		if(g_settings.options["log chat events"].get<bool*>())
			LOG(EVENT) << "Network Chat Event from: " << player->get_name() << " ID: " << player->player_info->m_rockstar_id << " Message: " << message;
		if (g_settings.options["chat command toggle"])
		{
			std::vector<std::string> args;
			std::istringstream iss(message);

			for (std::string s; iss >> s;)
				args.push_back(s);

			auto it = handlers.find(args[0]);
			if (args.size() > 0 && it != handlers.end())
			{
				command cmd = it->second;
				if (g_settings.options[cmd.toggle_name] && args.size() - 1 >= cmd.arg_count)
				{
					cmd.handler(player->player_id, args);
				}
			}
		}
	}

	void chat_commands::spawn_handler(Player sender, std::vector<std::string> args)
	{
		Hash model = MISC::GET_HASH_KEY(args[1].c_str());

		if (!STREAMING::IS_MODEL_A_VEHICLE(model) || !STREAMING::IS_MODEL_VALID(model))
			return;

		std::string name = NETWORK::NETWORK_PLAYER_GET_NAME(sender);

		draw_helper::draw_headshot_notification(fmt::format("Command: spawn\nVehicle: {}", args[1]), name, sender, "Chat Command", HUD_COLOUR_HEIST_BACKGROUND);
		LOG(INFO) << "Chat Command: " << args[0] << " From: " << name;

		QUEUE_JOB_BEGIN_CLAUSE(sender, model)
		{
			Vector3 coords = ENTITY::GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER::GET_PLAYER_PED_SCRIPT_INDEX(sender), 0, 5, 0);
			float heading = ENTITY::GET_ENTITY_HEADING(PLAYER::GET_PLAYER_PED_SCRIPT_INDEX(sender));

			Vehicle vehicle = vehicle_helper::create_vehicle(model, coords.x, coords.y, coords.z, heading);
			vehicle_helper::set_max_stats(vehicle);
			vehicle_helper::set_vehicle_strong(vehicle);
		} QUEUE_JOB_END_CLAUSE
	}

	void chat_commands::weapons_handler(Player sender, std::vector<std::string> args)
	{
		std::string name = NETWORK::NETWORK_PLAYER_GET_NAME(sender);

		draw_helper::draw_headshot_notification("Command: weapons", name, sender, "Chat Command", HUD_COLOUR_HEIST_BACKGROUND);
		LOG(INFO) << "Chat Command: " << args[0] << " From: " << name;

		QUEUE_JOB_BEGIN_CLAUSE(sender)
		{
			Ped ped = PLAYER::GET_PLAYER_PED_SCRIPT_INDEX(sender);
			weapons_tab::give_all_weapons(ped);
		} QUEUE_JOB_END_CLAUSE
	}

	void chat_commands::explode_handler(Player sender, std::vector<std::string> args)
	{
		if (args[1].length() < 4)
			return;

		for (int i = 0; i < gta::num_players; i++)
		{
			std::string name(NETWORK::NETWORK_PLAYER_GET_NAME(i));
			if (!NETWORK::NETWORK_IS_PLAYER_CONNECTED(i) || name.find(args[1]) == std::string::npos)
				continue;

			draw_helper::draw_headshot_notification(fmt::format("Command: explode\nTarget: {}", name), name, sender, "Chat Command", HUD_COLOUR_HEIST_BACKGROUND);
			LOG(INFO) << "Chat Command: " << args[0] << " From: " << name;

			QUEUE_JOB_BEGIN_CLAUSE(sender, i)
			{
				rage_helper::blame_explosion_on_ped(PLAYER::GET_PLAYER_PED_SCRIPT_INDEX(sender), PLAYER::GET_PLAYER_PED_SCRIPT_INDEX(i));
			} QUEUE_JOB_END_CLAUSE

			break;
		}
	}

	void chat_commands::money_handler(Player sender, std::vector<std::string> args)
	{
		bool value_changed{};
		auto scid = g_pointers->m_get_net_player(sender)->get_net_data()->m_rockstar_id;
		if (!args[1].compare("on") && !g_settings.player_options[scid]["money drop"])
		{
			g_settings.player_options[scid]["money drop"] = true;
			value_changed = true;

		}
		else if (!args[1].compare("off") && g_settings.player_options[scid]["money drop"])
		{
			g_settings.player_options[scid]["money drop"] = false;
			value_changed = true;
		}

		if (value_changed)
		{
			std::string name = NETWORK::NETWORK_PLAYER_GET_NAME(sender);

			draw_helper::draw_headshot_notification(fmt::format("Command: money\nToggle: {}", g_settings.player_options[scid]["money drop"] ? "On" : "Off"), name, sender, "Chat Command", HUD_COLOUR_HEIST_BACKGROUND);
			LOG(INFO) << "Chat Command: " << args[0] << " From: " << name;
		}
	}

	void chat_commands::semigod_handler(Player sender, std::vector<std::string> args)
	{
		bool value_changed{};
		auto scid = g_pointers->m_get_net_player(sender)->get_net_data()->m_rockstar_id;
		if (!args[1].compare("on") && !g_settings.player_options[scid]["semi-godmode"])
		{
			g_settings.player_options[scid]["semi-godmode"] = true;
			value_changed = true;

		}
		else if (!args[1].compare("off") && g_settings.player_options[scid]["semi-godmode"])
		{
			g_settings.player_options[scid]["semi-godmode"] = false;
			value_changed = true;
		}

		if (value_changed)
		{
			std::string name = NETWORK::NETWORK_PLAYER_GET_NAME(sender);

			draw_helper::draw_headshot_notification(fmt::format("Command: semigod\nToggle: {}", g_settings.player_options[scid]["semi-godmode"] ? "On" : "Off"), name, sender, "Chat Command", HUD_COLOUR_HEIST_BACKGROUND);
			LOG(INFO) << "Chat Command: " << args[0] << " From: " << name;
		}
	}

}