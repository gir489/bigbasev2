#pragma once

#include "natives.hpp"
#include "WeaponAttachment.h"

namespace weapon_json
{
	struct weapon_json
	{
		Hash weapon_hash{};
		std::string weapon_name{};
		std::string display_name{};
		Hash group{};
		std::vector<weapon_attachment::weapon_attachment> attachments{};
	};

	static void to_json(nlohmann::json& j, const weapon_json& weapon) {
		j = nlohmann::json{ {"Weapon Hash", weapon.weapon_hash}, {"Weapon Name", weapon.weapon_name}, {"Display Name", weapon.display_name},
							{"Group", weapon.group},{"Accepted Components", weapon.attachments } };
	}
	static void from_json(const nlohmann::json& j, weapon_json& weapon) {
		j.at("Weapon Hash").get_to(weapon.weapon_hash); j.at("Weapon Name").get_to(weapon.weapon_name); j.at("Display Name").get_to(weapon.display_name);
		j.at("Group").get_to(weapon.group); j.at("Accepted Components").get_to(weapon.attachments);
	}
}