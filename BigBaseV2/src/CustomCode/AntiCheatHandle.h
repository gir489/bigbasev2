#pragma once

#include "common.hpp"
#include "tlhelp32.h"
#include "gta\fwddec.hpp"
#include "gta\player.hpp"

namespace big
{
	struct DetectItem
	{
		DWORD _0x00;
		DWORD _0x04;
		DWORD _0x08;
		DWORD _0x0C;
		DWORD _0x10;
		DWORD _0x14;
		DWORD _0x18;
		DWORD _0x1C;
		DWORD _0x20;
		DWORD _0x24;
		DWORD _0x28;
		DWORD _0x2C;
		DWORD _0x30;
		DWORD _0x34;
	};
	static_assert(sizeof(DetectItem) == 0x38);

	struct DetectStruct
	{
		DetectItem items[1024];
		DWORD lastItemIdx;
		DWORD numDeteccItems1;
		DWORD numDeteccItems2;
	};
	static_assert(sizeof(DetectStruct) == 0xE00C);

	class anti_cheat_handle
	{
	public:
		static void Initialize();
		static void Uninitialize();
		static void OnThreadAttach(HANDLE thread);
		static void OnThreadDetach(HANDLE thread);
	private:
		static inline std::unordered_map<int, int> g_AntiCheatThreadRegisters; // ThreadId -> RegIndex
		static inline std::unordered_map<int, int> g_SendNetworkEventThreadRegisters; // ThreadId -> RegIndex
		static int FindFreeRegister(const CONTEXT* ctx, void* function);
		static bool SetBreakpointsForThreadInternal(HANDLE thread, DWORD threadId, bool enable);
		static bool SetBreakpointsForThread(HANDLE thread, bool enable);
		static bool SetBreakpointsForAllThreads(bool enable);
		static bool AntiCheatCallback(DetectStruct* thisptr, DetectItem* item);
		static void SendNetworkEventCallback(rage::netEventMgr* this_ptr, rage::netGameEvent* game_event);
		static LONG WINAPI ExceptionHandler(EXCEPTION_POINTERS* exp);
		static inline PVOID g_VectoredExceptionHandler;
	};
}