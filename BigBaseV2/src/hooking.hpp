#pragma once
#include "common.hpp"
#include "detour_hook.hpp"
#include "gta/fwddec.hpp"
#include "script_hook.hpp"
#include "vmt_hook.hpp"
#include "gta\net_game_event.hpp"
#include "gta\net_object_mgr.hpp"
#include "gta\player.hpp"
#include "gta/session.hpp"

namespace big
{
	inline std::unordered_map<uint64_t, std::string> g_pending_gamer_group;

	struct hooks
	{
		static bool run_script_threads(std::uint32_t ops_to_execute);
		static void *convert_thread_to_fiber(void *param);

		static constexpr auto swapchain_num_funcs = 19;
		static constexpr auto swapchain_present_index = 8;
		static constexpr auto swapchain_resizebuffers_index = 13;
		static constexpr auto networked_object_index = 19;
		static constexpr auto networked_objects_num_funcs = 26;
		static HRESULT swapchain_present(IDXGISwapChain *this_, UINT sync_interval, UINT flags);
		static HRESULT swapchain_resizebuffers(IDXGISwapChain *this_, UINT buffer_count, UINT width, UINT height, DXGI_FORMAT new_format, UINT swapchain_flags);

		static LRESULT wndproc(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam);
		static BOOL set_cursor_pos(int x, int y);
		static BOOL rec_clone(CNetworkObjectMgr* this_ptr, CNetGamePlayer* sender, CNetGamePlayer* receiver, uint16_t object_type, uint16_t object_id, uint16_t object_flags, rage::datBitBuffer* bitbuffer, int timestamp);
		static BOOL received_clone_create_hook(CNetworkObjectMgr* this_ptr, CNetGamePlayer* sender, CNetGamePlayer* receiver, uint16_t object_type, uint16_t object_id, uint16_t object_flags, rage::datBitBuffer* bitbuffer, int timestamp);
		static void network_event_hook(rage::netEventMgr* this_ptr, CNetGamePlayer *source_player, CNetGamePlayer *target_player, uint16_t event_id, int event_index, int event_handled_bitset, uint16_t event_parameters, rage::datBitBuffer* buffer);
		static void metric_script_event_spam_hook(int eventId);
		static bool anti_cheat_function_hook(__int64 a1, DWORD *a2);
		static void receive_chat_message_hook(__int64 this_ptr, __int64 a2, std::uint64_t sender_gamer_id, const char* message);
		static int censor_chat_text_func(__int64 chat_menu, const char* user_text, const char** output_text);
		static int censor_chat_text(__int64 chat_menu, const char* user_text, const char** output_text);
		static bool send_net_info_to_lobby_hook_func(int64_t a1, int64_t* a2);
		static bool send_net_info_to_lobby_hook(int64_t a1, int64_t* a2);
		static bool is_tag_restricted_hook(int64_t identifier, Hash component_hash, Hash restriction_tag);
		static bool attach_object_to_entity_hook_func(rage::netObject* object, rage::fwEntity* target);
		static bool attach_object_to_entity_hook(rage::netObject* object, rage::fwEntity* target);
		static bool attach_ped_to_entity_hook_func(rage::netObject* ped, rage::fwEntity* target);
		static bool attach_ped_to_entity_hook(rage::netObject* ped, rage::fwEntity* target);
		static bool read_session_info_response_hook(QueryGamerResponse* response);
		static bool read_session_info_response_hook_func(QueryGamerResponse* response);
		static size_t strlen(char* str);
		static char netcat_insert_dedupe(uint64_t catalog, uint64_t * key, uint64_t * item);
	};

	struct minhook_keepalive
	{
		minhook_keepalive();
		~minhook_keepalive();
	};

	class hooking
	{
		friend hooks;
	public:
		explicit hooking();
		~hooking();

		void enable();
		void disable();

	private:
		bool m_enabled{};
		minhook_keepalive m_minhook_keepalive;

		vmt_hook m_swapchain_hook;
		
		WNDPROC m_og_wndproc;
		detour_hook m_set_cursor_pos_hook;
		detour_hook m_metric_script_event_spam_hook;
		detour_hook m_network_event_hook;
		detour_hook m_receive_chat_message_hook;
		detour_hook m_received_clone_create_hook;
		detour_hook m_censor_chat_text_hook;

		detour_hook m_run_script_threads_hook;
		detour_hook m_send_net_info_to_lobby_hook;
		detour_hook m_is_tag_restricted_hook;
		detour_hook m_attach_object_to_entity_hook;
		detour_hook m_attach_ped_to_entity_hook;
		detour_hook m_convert_thread_to_fiber_hook;
	};

	inline hooking *g_hooking{};
}
