#pragma once
#include "common.hpp"
#include "gta/player.hpp"
#include "gta/script_thread.hpp"
#include "gta/tls_context.hpp"
#include "pointers.hpp"

namespace big::gta_util
{
	inline rage::CPed* get_local_ped()
	{
		if (auto ped_factory = *g_pointers->m_ped_factory)
		{
			return ped_factory->m_local_ped;
		}

		return nullptr;
	}

	inline rage::CPlayerInfo* get_local_playerinfo()
	{
		if (auto ped_factory = *g_pointers->m_ped_factory)
		{
			if (auto ped = ped_factory->m_local_ped)
			{
				return ped->m_player_info;
			}
		}

		return nullptr;
	}

	inline rage::CPed* get_player_ped(Player player)
	{
		return g_pointers->m_get_player_ped_address(player);
	}

	inline rage::CPlayerInfo* get_player_playerinfo(Player player)
	{
		if (auto ped = g_pointers->m_get_player_ped_address(player))
		{
			return ped->m_player_info;
		}

		return nullptr;
	}

	inline int get_connected_players()
	{
		int count = NETWORK::NETWORK_GET_NUM_CONNECTED_PLAYERS();
		if (count == 0)
			count = 1;

		return count;
	}

	template <typename F, typename ...Args>
	void execute_as_script(rage::joaat_t script_hash, F&& callback, Args&& ...args)
	{
		auto tls_ctx = rage::tlsContext::get();
		for (auto thread : *g_pointers->m_script_threads)
		{
			if (!thread || !thread->m_context.m_thread_id || thread->m_context.m_script_hash != script_hash)
				continue;

			auto og_thread = tls_ctx->m_script_thread;

			tls_ctx->m_script_thread = thread;
			tls_ctx->m_is_script_thread_active = true;

			std::invoke(std::forward<F>(callback), std::forward<Args>(args)...);

			tls_ctx->m_script_thread = og_thread;
			tls_ctx->m_is_script_thread_active = og_thread != nullptr;

			return;
		}
	}

	static GtaThread* find_script_thread(rage::joaat_t hash, int instance_id = -2)
	{
		for (auto thread : *g_pointers->m_script_threads)
		{
			if (thread
				&& thread->m_context.m_thread_id
				&& thread->m_handler
				&& thread->m_script_hash == hash
				&& (instance_id == -2 || thread->m_instance_id == instance_id))
			{
				return thread;
			}
		}

		return nullptr;
	}

	static bool does_script_thread_exist(GtaThread* thread_parameter)
	{
		for (auto thread : *g_pointers->m_script_threads)
			if (thread == thread_parameter)
				return true;
		return false;
	}
}
