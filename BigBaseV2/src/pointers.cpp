#include "common.hpp"
#include "logger.hpp"
#include "pointers.hpp"
#include "memory/all.hpp"

namespace big
{
	pointers::pointers()
	{
		memory::pattern_batch main_batch;

		main_batch.add("Game state", "83 3D ? ? ? ? ? 75 17 8B 43 20", [this](memory::handle ptr)
		{
			m_game_state = ptr.add(2).rip().as<eGameState*>();
		});

		main_batch.add("Ped factory", "48 8B 05 ? ? ? ? 48 8B 48 08 48 85 C9 74 52 8B 81", [this](memory::handle ptr)
		{
			m_ped_factory = ptr.add(3).rip().as<rage::CPedFactory**>();
		});

		main_batch.add("Network player manager", "48 8B 0D ? ? ? ? 8A D3 48 8B 01 FF 50 ? 4C 8B 07 48 8B CF", [this](memory::handle ptr)
		{
			m_network_player_mgr = ptr.add(3).rip().as<CNetworkPlayerMgr**>();
		});

		main_batch.add("Native handlers", "48 8D 0D ? ? ? ? 48 8B 14 FA", [this](memory::handle ptr)
		{
			m_native_registration_table = ptr.add(3).rip().as<rage::scrNativeRegistrationTable*>();
			m_get_native_handler = ptr.add(0xC).rip().as<functions::get_native_handler_t>();
		});

		main_batch.add("Fix vectors", "83 79 18 00 48 8B D1 74 4A FF 4A 18 48 63 4A 18 48 8D 41 04 48 8B 4C CA", [this](memory::handle ptr)
		{
			m_fix_vectors = ptr.as<functions::fix_vectors_t>();
		});

		main_batch.add("Script threads", "45 33 F6 8B E9 85 C9 B8", [this](memory::handle ptr)
		{
			m_script_threads = ptr.sub(4).rip().sub(8).as<decltype(m_script_threads)>();
			m_run_script_threads = ptr.sub(0x1F).as<functions::run_script_threads_t>();
		});

		main_batch.add("Script programs", "48 03 15 ? ? ? ? 4C 23 C2 49 8B 08", [this](memory::handle ptr)
		{
			m_script_program_table = ptr.add(3).rip().as<decltype(m_script_program_table)>();
		});

		main_batch.add("Script globals", "48 8D 15 ? ? ? ? 4C 8B C0 E8 ? ? ? ? 48 85 FF 48 89 1D", [this](memory::handle ptr)
		{
			m_script_globals = ptr.add(3).rip().as<std::int64_t**>();
		});

		main_batch.add("CGameScriptHandlerMgr", "48 8B 0D ? ? ? ? 4C 8B CE E8 ? ? ? ? 48 85 C0 74 05 40 32 FF", [this](memory::handle ptr)
		{
			m_script_handler_mgr = ptr.add(3).rip().as<CGameScriptHandlerMgr**>();
		});

		main_batch.add("Swapchain", "0F 84 ? ? ? ? 4C 39 3D ? ? ? ? 0F 84", [this](memory::handle ptr)
		{
			m_swapchain = ptr.add(9).rip().as<IDXGISwapChain**>();
		});

		main_batch.add("GetNetPlayer", "74 0A 83 F9 1F 77 05 E8 ? ? ? ? 48", [this](memory::handle ptr)
		{
			m_is_session_started = ptr.sub(4).rip().as<bool*>();
			m_get_net_player = ptr.sub(0xC).as<decltype(m_get_net_player)>();
		});

		main_batch.add("NetworkEventFunction", "66 41 83 F9 ? 0F 83", [this](memory::handle ptr)
		{
			m_network_event_function = ptr.as<PVOID>();
		});

		main_batch.add("Trigger Script Event", "48 8B C4 48 89 58 08 48 89 68 10 48 89 70 18 48 89 78 20 41 56 48 81 EC ? ? ? ? 45 8B F0 41 8B F9", [this](memory::handle ptr)
		{
			m_trigger_script_event = ptr.as<functions::trigger_script_event_t>();
		});

		main_batch.add("Add Owned Explosion Bypass", "88 45 DC 0F 85", [this](memory::handle ptr)
		{
			m_add_owned_explosion_bypass = ptr.add(3).as<PVOID>();
		});

		main_batch.add("Blip List", "4C 8D 05 ? ? ? ? 0F B7 C1", [this](memory::handle ptr)
		{
			m_blip_list = ptr.add(3).rip().as<rage::BlipList*>();
		});

		main_batch.add("Get Player Ped Address", "40 53 48 83 EC 20 33 DB 38 1D ? ? ? ? 74 1C", [this](memory::handle ptr)
		{
			m_get_player_ped_address = ptr.as<functions::get_player_ped_address_t>();
		});

		main_batch.add("MetricScriptEventSpam", "8B F9 33 C9 E8 ? ? ? ? 48 8B D0", [this](memory::handle ptr)
		{
			m_metric_script_event_spam = ptr.sub(0xD).as<PVOID>();
		});

		main_batch.add("ModelSpawnBypass", "48 8B C8 FF 52 30 84 C0 74 05 48", [this](memory::handle ptr)
		{
			m_model_spawn_bypass = ptr.add(8).as<PVOID>();
		});

		main_batch.add("ReplayInterface", "48 8D 3D ? ? ? ? 75 16", [this](memory::handle ptr)
		{
			m_replay_interface = *(rage::CReplayInterface**)ptr.add(3).rip().as<PVOID>();
		});

		main_batch.add("ptr_to_handle", "48 89 5C 24 ? 48 89 74 24 ? 57 48 83 EC 20 8B 15 ? ? ? ? 48 8B F9 48 83 C1 10 33 DB", [this](memory::handle ptr)
		{
			m_ptr_to_handle = ptr.as<functions::ptr_to_handle_t>();
		});

		main_batch.add("Get Ped Data", "40 53 48 83 EC 20 48 8B 05 ? ? ? ? 8A D9 48 8B 90", [this](memory::handle ptr)
		{
			m_get_ped_data = ptr.as<functions::get_ped_data_t>();
		});

		main_batch.add("Anti Cheat Function", "44 8B 99 ? ? ? ? 4C 8B C9 B9 ? ? ? ? 4C 8B D2 44 3B D9 0F 8D", [this](memory::handle ptr)
		{
			m_anti_cheat_function = ptr.as<PVOID>();
		});

		main_batch.add("Decal Controller", "48 8B 0D ? ? ? ? C7 44 24 ? ? ? ? ? 8A 90", [this](memory::handle ptr)
		{
			m_decal_controller = ptr.add(3).rip().as<rage::decal_controller**>();
		});

		/*main_batch.add("Set Lobby Weather", "48 89 5C 24 ? 48 89 6C 24 ? 48 89 74 24 ? 57 48 83 EC 30 40 8A E9", [this](memory::handle ptr)
		{
			m_set_lobby_weather = ptr.as<functions::set_lobby_weather_t>();
		});*/

		main_batch.add("Spectate Control Bypass", "84 C0 75 6A 8B CB", [this](memory::handle ptr)
		{
			m_spectate_control_bypass = ptr.as<PVOID>();
		});

		main_batch.add("Network Object Manager", "48 8B 0D ? ? ? ? 45 33 C0 E8 ? ? ? ? 33 FF 4C 8B F0", [this](memory::handle ptr)
		{
			m_networked_object_manager = ptr.add(3).rip().as<CNetworkObjectMgr**>();
			m_find_object_by_id = ptr.add(0xB).rip().as<functions::find_net_object_by_id_t>();
		});

		main_batch.add("Receive Chat Message", "4D 85 C9 0F 84 ? ? ? ? 48 8B C4 48 89 58 08 48 89 70 10 48 89 78 18 4C 89 48 20", [this](memory::handle ptr)
		{
			m_receive_chat_message = ptr.as<PVOID>();
		});

		main_batch.add("Get Player From Gamer Handle", "48 8B D1 48 8B 0D ? ? ? ? 41 B0 01 E9", [this](memory::handle ptr)
		{
			m_get_netplayer_from_gamerhandle = ptr.as<functions::get_netplayer_from_gamerhandle>();
		});

		/*main_batch.add("Set Lobby Clock", "48 89 5C 24 ? 57 48 83 EC 20 8B F9 48 8B 0D ? ? ? ? 48 8B DA 33 D2", [this](memory::handle ptr)
		{
			m_set_lobby_time = ptr.as<functions::set_lobby_time_t>();
		});*/

		main_batch.add("Read BitBuff", "E8 ? ? ? ? 48 8D 95 ? ? ? ? 48 8B CB E8 ? ? ? ? 44 38 AD ? ? ? ? 74 18 48 8D 54 24 ? 41 B8 ? ? ? ? 48 8B CB E8 ? ? ? ? 0F B7 74 24 ? 8B 7C 24 50 85 FF 74 12 48 8D 55 80 45 33 C9 44 8B C7 48 8B CB E8", [this](memory::handle ptr)
		{
			m_read_bitbuf_dword = ptr.add(1).rip().as<functions::read_bitbuf_dword>();
			m_read_bitbuf_array = ptr.add(0x4B).rip().as<functions::read_bitbuf_array>();
		});

		main_batch.add("Received Clone Create", "E8 ? ? ? ? 4C 8B 8C 24 ? ? ? ? 44 8B C3", [this](memory::handle ptr)
		{
			m_received_clone_create = ptr.sub(0x163).as<PVOID>();
			m_get_sync_tree = ptr.add(1).rip().as<functions::get_sync_tree>();
			m_populate_sync_tree = ptr.add(0x1E).rip().as<functions::populate_sync_tree>();
		});

		main_batch.add("Send Event Ack", "E8 ? ? ? ? 66 83 7B ? ? 4C 8D 0D", [this](memory::handle ptr)
		{
			m_send_event_ack = ptr.add(1).rip().as<functions::send_event_ack>();
		});

		main_batch.add("Get Entity From ID", "83 F9 FF 74 31 4C 8B 0D", [this](memory::handle ptr)
		{
			m_get_entity_from_id = ptr.as<functions::get_entity_from_id>();
		});

		main_batch.add("SendGiveRewardPickup", "E8 ? ? ? ? FF C3 3B DE 72 A5", [this](memory::handle ptr)
		{
			m_send_give_reward_pickup = ptr.add(1).rip().as<functions::send_give_reward_pickup>();
		});

		main_batch.add("Get Network Name", "75 1C E8 ? ? ? ? 48 85 C0", [this](memory::handle ptr)
		{
			m_get_network_name = ptr.add(3).rip().as<functions::get_network_name>();
			m_get_friends_list = ptr.sub(0xB).as<functions::get_friends_list_t>();
		});

		main_batch.add("Join Player", "E8 ? ? ? ? E9 ? ? ? ? 41 81 3F ? ? ? ? 0F 84", [this](memory::handle ptr)
		{
			m_join_player = ptr.add(1).rip().as<functions::join_player>();
		});

		/*main_batch.add("Text Message Hook", "48 89 5C 24 ? 48 89 6C 24 ? 48 89 74 24 ? 57 48 81 EC ? ? ? ? 80 3D", [this](memory::handle ptr)
		{
			m_text_message_hook = ptr.as<PVOID>();
		});*/

		main_batch.add("Send NetInfo To Lobby", "48 89 5C 24 ? 48 89 74 24 ? 57 48 83 EC 20 48 8B F2 48 8B D9 E8 ? ? ? ? 84 C0 74 77", [this](memory::handle ptr)
		{
			m_send_net_info_to_lobby = ptr.as<PVOID>();
		});

		main_batch.add("Is Tag Restricted", "40 53 48 83 EC 20 41 8B D8 E8 ? ? ? ? 33 D2 48 85 C0 74 20", [this](memory::handle ptr)
		{
			m_is_tag_restricted = ptr.as<PVOID>();
		});

		//main_batch.add("Attach Entity To Ped", "E8 ? ? ? ? 80 7B 28 04 75 35", [this](memory::handle ptr)
		main_batch.add("Attach NetObject To fwEntity", "48 89 5C 24 ? 57 48 83 EC 20 48 8B D9 E8 ? ? ? ? 40 8A F8 84 C0 74 14 48 8B 13", [this](memory::handle ptr)
		{
			m_attach_object_to_entity = ptr.as<PVOID>();
		});

		main_batch.add("Attach NetPed To fwEntity", "40 53 48 83 EC 60 44 8B 99", [this](memory::handle ptr)
		{
			m_attach_ped_to_entity = ptr.as<PVOID>();
		});

		main_batch.add("Delete Object", "E8 ? ? ? ? FF CE 85 DB", [this](memory::handle ptr)
		{
			m_delete_object = ptr.add(1).rip().as<functions::delete_object>();
		});
		
		main_batch.add("Censor Chat Text", "E8 ? ? ? ? 83 F8 FF 75 B9", [this](memory::handle ptr)
		{
			m_censor_chat_text = ptr.add(1).rip().as<PVOID>();
		});

		main_batch.add("Get Vehicle Ptr", "74 21 E8 ? ? ? ? 48 85 C0 74 10", [this](memory::handle ptr)
		{
			m_get_vehicle_ptr = ptr.add(3).rip().as<functions::get_vehicle_ptr>();
		});

		main_batch.add("Get Ped Ptr", "E8 ? ? ? ? 45 33 FF 48 85 C0 75 07", [this](memory::handle ptr)
		{
			m_get_ped_ptr = ptr.add(1).rip().as<functions::get_ped_ptr>();
		});

		main_batch.add("SendNetworkEventFunction", "48 89 44 24 ? 89 74 24 20 E8 ? ? ? ? 48 8B D8 48 8B 0D ? ? ? ? 48 8B D3 E8", [this](memory::handle ptr)
		{
			m_send_network_event_function = ptr.add(0x1C).rip().as<functions::send_network_event>();
		});

		main_batch.add("Delete Ped", "48 83 EC 28 48 85 C9 74 12 48 8B D1", [this](memory::handle ptr)
		{
			m_delete_ped = ptr.as<functions::delete_ped>();
		});

		main_batch.add("World to Screen", "E8 ? ? ? ? 84 C0 74 19 F3 0F 10 44 24", [this](memory::handle ptr) 
		{
			m_world_to_scren = ptr.add(1).rip().as<functions::world_to_scren>();
		});

		main_batch.add("Screen Resolution", "8B 0D ? ? ? ? 49 8D 56 28", [this](memory::handle ptr)
		{
			m_screen_resolution = ptr.add(2).rip().as<rage::ScreenResolution*>();
		});

		main_batch.add("CDriveByDefaultPtr", "7C B8 48 8B 0D", [this](memory::handle ptr)
		{
			ptr = ptr.add(5).rip();
			m_driveby_metadata_mgr = ptr.as<rage::CVehicleDriveByMetadataMgr*>();
			m_vehicle_layout_metadata_mgr = ptr.add(0x20).as<rage::CVehicleSeatMetadataMgr*>();
		});

		main_batch.add("Force Host Patch", "C6 05 ? ? ? ? ? 48 8B CB E8 ? ? ? ? 84 C0 75 08", [&](memory::handle ptr)
		{
			m_force_host_patch = ptr.add(3).rip().as<PVOID>();
		});

		main_batch.add("Set New Script Host", "48 89 5C 24 ? 57 48 83 EC 50 48 8B FA 48 8B D9 E8 ? ? ? ? 83 7B 10 08", [&](memory::handle ptr)
		{
			m_set_new_script_host = ptr.as<functions::set_new_script_host_t>();
		});

		main_batch.add("Send Session Info Request", "48 8B C4 48 89 58 08 48 89 68 10 48 89 70 18 48 89 78 20 41 56 48 83 EC 30 48 83 3D ? ? ? ? ? 8B", [this](memory::handle ptr)
		{
			m_send_session_info_request = ptr.as<functions::send_session_info_request_t>();
		});

		/*main_batch.add("Read Session Info Response", "48 8B C4 48 89 58 08 48 89 70 10 48 89 78 18 55 41 54 41 56 48 8D 68 D8 48 81 EC ? ? ? ? 45 33 F6 48 8B F9 45", [this](memory::handle ptr)
		{
			m_read_session_info_response = ptr.as<PVOID>();
		});*/

		main_batch.run(memory::module(nullptr));

		m_hwnd = FindWindowW(L"grcWindow", nullptr);
		if (!m_hwnd)
			throw std::runtime_error("Failed to find the game's window.");

		g_pointers = this;
	}

	pointers::~pointers()
	{
		g_pointers = nullptr;
	}
}
