#pragma once
#include "common.hpp"

namespace big
{
	class gui
	{
	public:
		void dx_init();
		void dx_on_tick();
		void esp_on_tick();
		void do_esp();
		
		void script_on_tick();
		static void script_func();
	public:
		bool m_opened{};
		bool m_using_keyboard{};
	};

	inline gui g_gui;
}
