#pragma once

#include "common.hpp"
#include "gta\net_game_event.hpp"

namespace big
{
	class event_hook
	{
	public:
		static bool validate_game_event(CScriptedGameEvent *game_event, CNetGamePlayer *sender, CNetGamePlayer *target);
	private:
		static bool validate_range(int64_t argument, int end, int start = 0);
		static bool validate_all_zero(int64_t* args, int start, int end);
		static bool validate_all_non_zero(int64_t* args, int start, int end);
		static bool validate_sender_is_host(CNetGamePlayer *sender);
	};
}