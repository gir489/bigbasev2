#include "common.hpp"
#include "NetEventHook.h"
#include "CustomCode/Settings.h"
#include "CustomCode/PlayerInfo.h"
#include "helpers/VehicleHelper.h"
#include "gta/net_object_mgr.hpp"
#include "ReportHook.h"
#include "script.hpp"
#include "CustomCode/WeaponInfo.h"

namespace big
{
	bool NetEventHook::NetworkEventFunction(rage::netEventMgr* event_manager, CNetGamePlayer* source_player, CNetGamePlayer* target_player, uint16_t event_id, int event_index, int event_handled_bitset, rage::datBitBuffer* buffer)
	{
		if (event_id >= 91)
			return false;

		const char* eventName = *(char**)((DWORD64)event_manager + 8i64 * event_id + 243376);
		if (source_player == nullptr || rage_helper::validate_player_id(source_player->player_id))
		{
			g_pointers->m_send_event_ack(event_manager, source_player, target_player, event_index, event_handled_bitset);
			return false;
		}

		switch (event_id)
		{
			case SCRIPTED_GAME_EVENT:
			{
				auto game_event = CScriptedGameEvent();
				buffer->ReadDword(&game_event.m_args_size, 32);
				if (game_event.m_args_size <= 0x1AF)
					buffer->ReadArray(&game_event.m_args, 8 * game_event.m_args_size);
				if (event_hook::validate_game_event(&game_event, source_player, target_player) == false)
				{
					g_pointers->m_send_event_ack(event_manager, source_player, target_player, event_index, event_handled_bitset);
					return false;
				}
				buffer->Seek(0);
				return true;
			}
			case NETWORK_INCREMENT_STAT_EVENT: 
			{
				auto event_obj = CNetworkIncrementStatEvent();
				buffer->ReadDword(&event_obj.m_stat, 32);
				buffer->ReadDword(&event_obj.m_amount, 32);

				if (ReportFunction(&event_obj, source_player))
				{
					g_pointers->m_send_event_ack(event_manager, source_player, target_player, event_index, event_handled_bitset);
					return false;
				}

				buffer->Seek(0);
				return true;
			}
			case EXPLOSION_EVENT:
			{
				uint16_t explosion_owner{};
				uint32_t explosion_flags{};
				int32_t explosion_type{};
				buffer->Seek(29);
				buffer->ReadWord(&explosion_owner, 13);
				buffer->Seek(55);
				buffer->ReadInt32(&explosion_type, 8);
				buffer->Seek(149);
				buffer->ReadDword(&explosion_flags, 32);
				if (explosion_owner != 0)
				{
					auto ped_owner_id = -1;
					if (auto net_obj = (*g_pointers->m_networked_object_manager)->find_object_by_id(explosion_owner, FALSE))
						ped_owner_id = net_obj->m_owner_id;
					if ((explosion_flags & AddOwnedExplosionFlag) && source_player->player_id != ped_owner_id && ped_owner_id != -1)
					{
						BlockedEvent("ADD_OWNED_EXPLOSION", source_player);
						if (g_settings.options["log network events"] > 0)
							LOG(EVENT) << "Blocked Network Event from: " << source_player->player_info->m_name << " Type: " << eventName << " explosion_owner: " << explosion_owner << " explosion_flags: " << HEX_TO_UPPER(explosion_flags) << " explosion_type: " << explosion_type;
						g_pointers->m_send_event_ack(event_manager, source_player, target_player, event_index, event_handled_bitset);
						return false;
					}
				}
				buffer->Seek(0);
				if (g_settings.options["log network events"] > 0)
					LOG(EVENT) << "Network Event from: " << source_player->player_info->m_name << " Type: " << eventName << " explosion_owner: " << explosion_owner << " explosion_flags: " << HEX_TO_UPPER(explosion_flags) << " explosion_type: " << explosion_type;
				return true;
			}
			case REMOVE_WEAPON_EVENT:
			{
				uint16_t network_id{};
				Hash weapon{};
				buffer->ReadWord(&network_id, 0xD);
				buffer->ReadDword(&weapon, 0x20);
				if (network_id == 0 || g_local_player.net_player_id == network_id || !weapon_info::does_weapon_exist(weapon))
				{
					BlockedEvent(eventName, source_player);
					if (g_settings.options["log network events"] > 0)
						LOG(EVENT) << "Blocked Network Event from: " << source_player->player_info->m_name << " Type: " << eventName << " network_id: " << network_id << " weapon: " << HEX_TO_UPPER(weapon);
					g_pointers->m_send_event_ack(event_manager, source_player, target_player, event_index, event_handled_bitset);
					return false;
				}
				if (g_settings.options["log network events"] > 0)
					LOG(EVENT) << "Network Event from: " << source_player->player_info->m_name << " Type: " << eventName << " network_id: " << network_id << " weapon: " << HEX_TO_UPPER(weapon);
				buffer->Seek(0);
				break;
			}
			case NETWORK_CLEAR_PED_TASKS_EVENT:
			{
				if (!g_local_player.is_cutscene_playing && (!(g_local_player.network_is_activity_session && g_local_player.vehicle_model == VEHICLE_PBUS)) )
				{
					uint16_t network_id{};
					buffer->ReadWord(&network_id, 0xD);
					if (g_local_player.net_player_id == network_id)
					{
						BlockedEvent(eventName, source_player);
						g_pointers->m_send_event_ack(event_manager, source_player, target_player, event_index, event_handled_bitset);
						return false;
					}
					buffer->Seek(0);
				}
				break;
			}
			case REQUEST_CONTROL_EVENT:
			{
				if (g_local_player.is_in_vehicle())
				{
					uint16_t network_id{};
					buffer->ReadWord(&network_id, 0xD);
					if (g_local_player.net_vehicle_id == network_id && g_local_player.is_in_personal_vehicle)
					{
						BlockedEvent(eventName, source_player);
						g_pointers->m_send_event_ack(event_manager, source_player, target_player, event_index, event_handled_bitset);
						return false;
					}
					buffer->Seek(0);
				}
				break;
			}
			case KICK_VOTES_EVENT:
			{
				uint32_t bitset{};
				buffer->ReadDword(&bitset, MAX_PLAYERS);
				if (bitset & (1 << target_player->player_id))
				{
					draw_helper::draw_headshot_notification("Voted to Kick", source_player->get_name(), source_player->player_id, "", HUD_COLOUR_RED);
					LOG(INFO) << source_player->player_info->m_name << " voted to kick the player.";
					if (g_settings.options["auto kick on vote kick"])
					{
						QUEUE_JOB_BEGIN_CLAUSE(source_player, eventName)
						{
							do_auto_kick(source_player, eventName);
						} QUEUE_JOB_END_CLAUSE
					}

				}
					
				buffer->Seek(0);
				break;
			}
			case REPORT_CASH_SPAWN_EVENT:
			{
				int32_t money{};
				buffer->Seek(64);
				buffer->ReadInt32(&money, 32);
				buffer->Seek(0);
				if (money == 2500)
				{
					g_persist_modder.add_modder(source_player, eventName, 50);
					LOG(HACKER) << "Invalid Money Drop Amount from " << source_player->player_info->m_name;
					draw_helper::draw_headshot_notification("Invalid Money Drop Amount", source_player->get_name(), source_player->player_id, "", HUD_COLOUR_RED);
				}
				if (g_settings.options["log network events"] > 0)
					LOG(EVENT) << "Network Event from: " << source_player->player_info->m_name << " Type: " << eventName << " Money: " << money;
				return true;
			}
			case NETWORK_CHECK_CODE_CRCS_EVENT: //Modder beacon.
			case REPORT_MYSELF_EVENT: //Modder beacon.
			{
				g_persist_modder.add_modder(source_player, eventName);
			}
		}

		if (g_settings.options["log network events"] == 2)
			LOG(EVENT) << "Network Event from: " << source_player->player_info->m_name << " Type: " << eventName;
		return true;
	}

	bool NetEventHook::NetworkEventSendFunction(rage::netEventMgr* this_ptr, rage::netGameEvent* game_event)
	{
		if (game_event == nullptr)
			return false;
		switch (game_event->m_id)
		{
			case REPORT_MYSELF_EVENT:
			case NETWORK_CHECK_CODE_CRCS_EVENT:
			case REPORT_CASH_SPAWN_EVENT:
			{
				if (g_settings.options["log network events"] > 0)
					LOG(EVENT) << "Blocked Network Event Send Type: " << game_event->get_name();
				delete game_event;
				return false;
			}
		}
		if (g_settings.options["log network events"].get<int64_t>() == 2)
			LOG(EVENT) << "Network Event Sent Type: " << game_event->get_name();
		return true;
	}

	void NetEventHook::BlockedEvent(std::string event, CNetGamePlayer *source_player)
	{
		g_persist_modder.add_modder(source_player, event, 50);
		auto name = source_player->get_net_data()->m_name;
		LOG(HACKER) << event << " Event Blocked from " << name;
		if (g_settings.options["log network events"] > 0)
			LOG(EVENT) << "Blocked Network Event from: " << name << " Type: " << event;
		draw_helper::draw_headshot_notification(event, name, source_player->player_id, "Event Blocked", HUD_COLOUR_RED);
	}

	void NetEventHook::ReportSpawnedObject(CNetGamePlayer* sender, const char* name, std::string net_obj_type, int modder_points)
	{
		std::string spawned = "Spawned ";
		spawned.append(net_obj_type);
		Player player = sender->player_id;
		draw_helper::draw_headshot_notification(spawned.c_str(), sender->get_name(), player, "Blocked Object", HUD_COLOUR_RED);
		LOG(HACKER) << net_obj_type << " spawn blocked from " << name;
		g_persist_modder.add_modder(sender, spawned, modder_points);
	}

	void NetEventHook::do_auto_kick(CNetGamePlayer* source_player, const char* eventName)
	{
		big::network_helper::kick_player(source_player->player_id, eventName);
	}
}