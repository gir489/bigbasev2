#pragma once

#include "natives.hpp"
#include "gta\enums.hpp"
#include "fiber_pool.hpp"
#include "pointers.hpp"
#include "Persist\PersistModder.h"
#include "Helpers\RageHelper.h"
#include "gta\net_game_event.hpp"
#include "gta\net_object_mgr.hpp"
#include "EventHook.h"
#include "gta\CrashHashes.h"

namespace big
{
	class NetEventHook
	{
	public:
		static bool NetworkEventFunction(rage::netEventMgr* event_manager, CNetGamePlayer *source_player, CNetGamePlayer *target_player, uint16_t event_id, int event_index, int event_handled_bitset, rage::datBitBuffer* buffer);
		static bool NetworkEventSendFunction(rage::netEventMgr* this_ptr, rage::netGameEvent* game_event);
		static void ReportSpawnedObject(CNetGamePlayer* sender, const char* name, std::string net_obj_type, int modder_points);
	private:
		static void BlockedEvent(std::string event, CNetGamePlayer *source_player);
		static void do_auto_kick(CNetGamePlayer* source_player, const char* eventName);
	};
}