#pragma once

#include "common.hpp"
#include "helpers\RageHelper.h"
#include "helpers\NetworkHelper.h"
#include "helpers\DrawHelper.h"
#include "Persist\PersistModder.h"
#include "gta\net_game_event.hpp"
#include "gta\joaat.hpp"
#include "CustomCode\Settings.h"
#include "fiber_pool.hpp"

static bool ReportFunction(CNetworkIncrementStatEvent* gameEvent, CNetGamePlayer* sourcePlayer)
{
	Player reporter = sourcePlayer->player_id;
	const char* name = sourcePlayer->get_name();

	std::string reason = "Reported for: ";

	switch (gameEvent->m_stat)
	{
		case RAGE_JOAAT("MPPLY_GRIEFING"):
			reason.append("Griefing");
			break;
		case RAGE_JOAAT("MPPLY_VC_ANNOYINGME"):
			reason.append("Voice chat annoying me");
			break;
		case RAGE_JOAAT("MPPLY_VC_HATE"):
			reason.append("Voice chat hate speech");
			break;
		case RAGE_JOAAT("MPPLY_TC_ANNOYINGME"):
			reason.append("Text chat annoying me");
			break;
		case RAGE_JOAAT("MPPLY_TC_HATE"):
			reason.append("Text chat hate speech");
			break;
		case RAGE_JOAAT("MPPLY_OFFENSIVE_LANGUAGE"):
			reason.append("Offensive language");
			break;
		case RAGE_JOAAT("MPPLY_OFFENSIVE_TAGPLATE"):
			reason.append("Offensive license plate");
			break;
		case RAGE_JOAAT("MPPLY_OFFENSIVE_UGC"):
			reason.append("Offensive UGC");
			break;
		case RAGE_JOAAT("MPPLY_BAD_CREW_NAME"):
			reason.append("Bad crew name");
			break;
		case RAGE_JOAAT("MPPLY_BAD_CREW_MOTTO"):
			reason.append("Bad crew motto");
			break;
		case RAGE_JOAAT("MPPLY_BAD_CREW_STATUS"):
			reason.append("Bad crew status");
			break;
		case RAGE_JOAAT("MPPLY_BAD_CREW_EMBLEM"):
			reason.append("Bad crew emblem");
			break;
		case RAGE_JOAAT("MPPLY_GAME_EXPLOITS"):
			reason.append("Game exploits");
			break;
		case RAGE_JOAAT("MPPLY_EXPLOITS"):
			reason.append("Exploits");
			break;
			//If we care about commends too we also have
		case RAGE_JOAAT("MPPLY_FRIENDLY"):
			big::draw_helper::draw_headshot_notification("Commended for: Friendly", name, reporter, "Commended", HUD_COLOUR_BLUE);
			LOG(INFO) << "Commended for: Friendly by: " << sourcePlayer->get_net_data()->m_name;
			return false;
		case RAGE_JOAAT("MPPLY_HELPFUL"):
			big::draw_helper::draw_headshot_notification("Commended for: Helpful", name, reporter, "Commended", HUD_COLOUR_BLUE);
			LOG(INFO) << "Commended for: Helpful by: " << sourcePlayer->get_net_data()->m_name;
			return false;
		default:
			big::g_persist_modder.add_modder(sourcePlayer, "Invalid report stat");
			return true;
	}

	big::draw_helper::draw_headshot_notification(reason, name, reporter, "Report Blocked", HUD_COLOUR_RED);
	LOG(INFO) << "Blocked Report. " << reason << " Player: " << sourcePlayer->get_net_data()->m_name << " Amount: " << gameEvent->m_amount;

	if (big::g_settings.options["auto kick on report"])
	{
		big::g_fiber_pool->queue_job([reporter, reason]
		{ try {
			big::network_helper::kick_player(reporter, reason);
		} catch (...) { LOG(FATAL) << "Networkhelper::AutoKickOnReport threw an exception!"; } });
	}
	
	return true;
}