#include "common.hpp"
#include "EventHook.h"
#include "helpers\RageHelper.h"
#include "helpers\DrawHelper.h"
#include "Persist\PersistModder.h"
#include "CustomCode/Settings.h"

namespace big
{
	bool event_hook::validate_game_event(CScriptedGameEvent *game_event, CNetGamePlayer *sender, CNetGamePlayer *target)
	{
		int eventId = (int)game_event->m_args[0];
		Player playerWhoCalled = sender->player_id;
		int argument_size = game_event->m_args_size / 8;

		if (playerWhoCalled == -1) //Invalid player. Code does the same check.
			return false;

		std::string reason;

		bool hacker_event = playerWhoCalled != (Player)game_event->m_args[1];

		switch (eventId)
		{
			case Events::SoundSpam:
				if (validate_all_non_zero(game_event->m_args, 3, 10))
					reason.append("Sound Spam");
				break;
			case Events::SoundSpam1:
				reason.append("Sound Spam");
				break;
			case Events::CEOKick:
			case Events::CEOKick1:
				reason.append("CEO Kick");
				break;
			case Events::CEOBan:
				reason.append("CEO Ban");
				break;
			case Events::SendToMission:
				reason.append("Send to Mission");
				break;
			case Events::InviteToApartment:
				if(hacker_event)
					reason.append("Invite to Apartment");
				break;
			case Events::Kick_PlayerEnteredSoundScene:
			case Events::Kick_PlayerLeftSoundScene:
			case Events::Kick_BikerMissionWallSounds:
				if (validate_range(game_event->m_args[2], 35) || argument_size != 3)
					reason.append("Kick");
				break;
			case Events::Kick_NETWORK_BAIL:
			case Events::Kick_RaceCreator1:
			case Events::Kick_RaceCreator2:
			case Events::Kick_RaceCreator3:
			case Events::Kick_RaceCreator4:
			case Events::Kick_RaceCreator5:
			case Events::Kick_LegacyStatEvent:
				reason.append("Kick");
				break;
			case Events::Kick_ArenaBanner:
			case Events::Kick_ArenaBanner1:
				if (validate_range(game_event->m_args[3], 25, -1) || argument_size < 4)
					reason.append("Kick");
				break;
			case Events::Kick_HeistUpdateBoard:
				if (validate_range(game_event->m_args[2], 6) || validate_range(game_event->m_args[3], 3) || argument_size != 20)
					reason.append("Kick");
				break;
			case Events::Kick_FmHoldUpTut:
				if (validate_range(game_event->m_args[2], 17) || validate_range(game_event->m_args[3], 1) || argument_size != 4)
					reason.append("Kick");
				break;
			case Events::Kick_Robbery_CashDrawer:
			case Events::Kick_Robbery_GenerateMoney:
			case Events::Kick_Robbery_ShopClosed:
				if (validate_range(game_event->m_args[2], 19) || argument_size != 4)
					reason.append("Kick");
				break;
			case Events::Kick_Betting:
				if (validate_sender_is_host(sender) || validate_range(game_event->m_args[2], 31) || argument_size != 4)
					reason.append("Kick");
				break;
			case Events::Kick_PickupItem:
				if (validate_range(game_event->m_args[2], 61) || game_event->m_args[1] != game_event->m_args[3] || argument_size != 4)
					reason.append("Kick");
				break;
			case Events::Kick_SetupCompanyTeams:
				if (validate_sender_is_host(sender) || validate_range(game_event->m_args[2], 16) || validate_all_zero(game_event->m_args, 3, 23) || argument_size != 24)
					reason.append("Kick");
				break;
			case Events::Kick_SetupHeistCost:
				if (validate_range(game_event->m_args[2], 4) || validate_range(game_event->m_args[3], 4) || argument_size != 5)
					reason.append("Kick");
				break;
			case Events::Kick_ReportModel:
				if (!STREAMING::IS_MODEL_VALID((Hash)game_event->m_args[2]) || validate_all_zero(game_event->m_args, 3, 4) || argument_size != 4)
					reason.append("Kick");
				break;
			case Events::Kick_FmmcLauncher:
				if ((int)game_event->m_args[2] != 7 || argument_size != 31)
					reason.append("Kick");
				break;
			case Events::Teleport:
				if (validate_range(game_event->m_args[5], 115) || hacker_event)
					reason.append("Infinite Load");
				break;
			case Events::GetOutOfCar:
				if ((int)game_event->m_args[8] == 0 || hacker_event)
					reason.append("Eject from Car");
				break;
			case Events::DrawNotification:
				if ((int)game_event->m_args[2] == -1550586884 || (int)game_event->m_args[2] == 2110027654)
					reason.append("Fake Money");
				break;
			case Events::DisplayError:
				reason.append("Error Spam");
				break;
			case Events::SendToIsland:
				reason.append("Send To Island");
				break;
			//case Events::InvalidRockstarEvent:
			//	hacker_event = false;
		}
		if (reason.empty() && hacker_event)
		{
			reason.append("Hacker Event");
		}
		if (!reason.empty())
		{
			const char* name = sender->get_name();
			draw_helper::draw_headshot_notification(reason, name, playerWhoCalled, "Event Blocked", HUD_COLOUR_RED);
			LOG(HACKER) << reason << " event blocked from " << name << " ID: " << eventId;
			g_persist_modder.add_modder(sender, reason);
			if (g_settings.options["log script events"])
			{
				char charguments[2048] = { 0 };
				for (int i = 2; i <= argument_size; i++)
					sprintf_s(charguments, "%sArg #%i: %i ", charguments, i, (int)game_event->m_args[i]);
				LOG(EVENT) << "Blocked Network Event from: " << name << " ID: " << eventId << " Hacker: " << hacker_event << " " << charguments;
			}
			return false;
		}
		if (g_settings.options["log script events"])
		{
			char charguments[2048] = { 0 };
			for (int i = 2; i <= argument_size; i++)
				sprintf_s(charguments, "%sArg #%i: %i ", charguments, i, (int)game_event->m_args[i]);
			LOG(EVENT) << "Network Event from: " << sender->get_name() << " ID: " << eventId << " " << charguments;
		}
		return true;
	}

	bool event_hook::validate_range(int64_t argument, int end, int start)
	{
		return !((int)argument >= start && (int)argument <= end);
	}

	bool event_hook::validate_all_zero(int64_t* args, int start, int end)
	{
		for (int i = start; i <= end; i++)
			if ((int)args[i] != 0)
				return true;
		return false;
	}

	bool event_hook::validate_all_non_zero(int64_t * args, int start, int end)
	{
		for (int i = start; i <= end; i++)
			if ((int)args[i] == 0)
				return true;
		return false;
	}

	bool event_hook::validate_sender_is_host(CNetGamePlayer *sender)
	{
		return NETWORK::NETWORK_GET_HOST_OF_SCRIPT("freemode", -1, 0) != sender->player_id;
	}
}