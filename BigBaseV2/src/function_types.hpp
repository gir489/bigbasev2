#pragma once
#include "common.hpp"
#include "gta/fwddec.hpp"
#include "gta/natives.hpp"
#include "gta/replay.hpp"
#include "persist\PedData.h"
#include "gta\player.hpp"

namespace big::functions
{
	using run_script_threads_t = bool(*)(uint32_t ops_to_execute);
	using get_native_handler_t = rage::scrNativeHandler(*)(rage::scrNativeRegistrationTable*, rage::scrNativeHash);
	using fix_vectors_t = void(*)(rage::scrNativeCallContext*);
	using trigger_script_event_t = void(*)(int eventGroup, int64_t* args, int argCount, uint32_t bitset);
	using get_net_player_t = CNetGamePlayer*(*)(Player);
	using find_net_object_by_id_t = rage::netObject* (*)(rage::netObjectMgrBase*, uint16_t object_id, bool can_delete_be_pending);
	using get_player_ped_address_t = rage::CPed*(*)(int);
	using ptr_to_handle_t = Entity(*)(PVOID);
	using get_ped_data_t = peddata::ped_data*(*)(Player);
	using set_lobby_weather_t = bool(*)(bool propagate, int weather_id, int, CNetGamePlayer* target);
	using get_netplayer_from_gamerhandle = CNetGamePlayer*(*)(std::uint64_t scid);
	using set_lobby_time_t = bool(*)(int32_t, CNetGamePlayer* target);
	using read_bitbuf_dword = bool(*)(void*, uint32_t*, int);
	using read_bitbuf_array = bool(*)(void*, PVOID, int, int);
	using send_event_ack = void(*)(rage::netEventMgr*, CNetGamePlayer* source_player, CNetGamePlayer* target_player, uint32_t event_index, uint32_t event_handled_bitset);
	using get_entity_from_id = rage::fwEntity* (*)(Entity ent);
	using send_give_reward_pickup = void(*)(uint32_t player_bitset, Hash a2);
	using get_network_name = void*(*)();
	using join_player = void*(*)(void*, bool);
	using delete_object = bool(*)(rage::CObject* object, bool);
	using get_vehicle_ptr = rage::CVehicle*(*)(Vehicle vehicle);
	using get_ped_ptr = rage::CPed*(*)(Ped vehicle);
	using send_network_event = void(*)(rage::netEventMgr* this_ptr, rage::netGameEvent* game_event);
	using delete_ped = bool(*)(rage::CPed* ped);
	using get_sync_tree = rage::netSyncTree*(*)(rage::netObjectMgrBase* this_ptr, uint16_t object_type);
	using populate_sync_tree = bool(*)(rage::netSyncTree* this_ptr, int32_t flags, int32_t flags2, rage::datBitBuffer* buffer, PVOID logger);
	using get_friends_list_t = int64_t(*)(uint32_t);
	using world_to_scren = bool(*)(rage::vector3, float* floatx, float* floaty);
	using set_new_script_host_t = int64_t(*)(rage::scriptHandlerNetComponent* comp, CNetGamePlayer* player);
	using send_session_info_request_t = bool(*)(PVOID targets, uint32_t count);
	using netcat_insert_t = void(*)(uint64_t catalog, uint64_t* key, uint64_t** item);
}
