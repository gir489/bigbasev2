﻿#include "common.hpp"
#include "features.hpp"
#include "fiber_pool.hpp"
#include "gui.hpp"
#include "logger.hpp"
#include "hooking.hpp"
#include "pointers.hpp"
#include "renderer.hpp"
#include "script_mgr.hpp"
#include "CustomCode/Settings.h"
#include "CustomCode\WeaponInfo.h"
#include "persist\PersistModder.h"
#include "CustomCode/player_features.hpp"
#include "CustomCode/PlayerAttachments.h"
#include "CustomCode/AntiCheatHandle.h"
#include "CustomCode/DetectModders.h"
#include "gui/GamerMonitor.h"

BOOL APIENTRY DllMain(HMODULE hmod, DWORD reason, PVOID)
{
	using namespace big;
	switch (reason)
	{
		case DLL_PROCESS_ATTACH:
			g_hmodule = hmod;
			g_main_thread = CreateThread(nullptr, 0, [](PVOID) -> DWORD
			{
				while (!FindWindow(L"grcWindow", L"Grand Theft Auto V"))
					std::this_thread::sleep_for(1s);

				auto logger_instance = std::make_unique<logger>();
				try
				{
					LOG(RAW_GREEN_TO_CONSOLE) << u8R"kek(
 ______  _       ______                        ______  
(____  \(_)     (____  \                      (_____ \ 
 ____)  )_  ____ ____)  ) ____  ___  ____ _   _ ____) )
|  __  (| |/ _  |  __  ( / _  |/___)/ _  ) | | /_____/ 
| |__)  ) ( ( | | |__)  | ( | |___ ( (/ / \ V /_______ 
|______/|_|\_|| |______/ \_||_(___/ \____) \_/(_______)
          (_____|)kek";
					auto pointers_instance = std::make_unique<pointers>();
					LOG(INFO) << "Pointers initialized.";

					auto renderer_instance = std::make_unique<renderer>();
					LOG(INFO) << "Renderer initialized.";

					auto fiber_pool_instance = std::make_unique<fiber_pool>(10);
					LOG(INFO) << "Fiber pool initialized.";

					auto hooking_instance = std::make_unique<hooking>();
					LOG(INFO) << "Hooking initialized.";

					g_settings.load();
					g_persist_modder.load();
					LOG(INFO) << "Settings Loaded.";

					weapon_info::load_weapon_info();
					LOG(INFO) << "Weapon Information Loaded.";

					player_attachments::load_attachments();
					LOG(INFO) << "Player Attachments Loaded.";

					g_script_mgr.add_script(std::make_unique<script>(&features::script_func));
					g_script_mgr.add_script(std::make_unique<script>(&player_features::script_func));
					g_script_mgr.add_script(std::make_unique<script>(&gui::script_func));
					//g_script_mgr.add_script(std::make_unique<script>(&gamer_monitor::script_func));
					g_script_mgr.add_script(std::make_unique<script>(&detect_modders::script_func));
					LOG(INFO) << "Scripts registered.";

					g_hooking->enable();
					LOG(INFO) << "Hooking enabled.";

					while (g_running)
					{
						std::this_thread::sleep_for(500ms);
					}

					g_hooking->disable();
					LOG(INFO) << "Hooking disabled.";

					std::this_thread::sleep_for(1000ms);

					g_script_mgr.remove_all_scripts();
					LOG(INFO) << "Scripts unregistered.";

					hooking_instance.reset();
					LOG(INFO) << "Hooking uninitialized.";

					fiber_pool_instance.reset();
					LOG(INFO) << "Fiber pool uninitialized.";

					renderer_instance.reset();
					LOG(INFO) << "Renderer uninitialized.";

					pointers_instance.reset();
					LOG(INFO) << "Pointers uninitialized.";
				}
				catch (std::exception const& ex)
				{
					LOG(WARNING) << ex.what();
					MessageBoxA(nullptr, ex.what(), nullptr, MB_OK | MB_ICONEXCLAMATION);
				}

				LOG(INFO) << "Farewell!";
				logger_instance.reset();

				CloseHandle(g_main_thread);
				FreeLibraryAndExitThread(g_hmodule, 0);
			}, nullptr, 0, &g_main_thread_id);
			return true;
		case DLL_THREAD_ATTACH:
			if(g_pointers)
				anti_cheat_handle::OnThreadAttach(hmod);
			return true;
		case DLL_THREAD_DETACH:
			anti_cheat_handle::OnThreadDetach(hmod);
			return true;
		default:
			return true;
	}
}
